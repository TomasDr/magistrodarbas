/* =========================================================
 * ====                   WARNING                        ===
 * =========================================================
 * The code in this tab has been generated from the GUI form
 * designer and care should be taken when editing this file.
 * Only add/edit code inside the event handlers i.e. only
 * use lines between the matching comment tags. e.g.

 void myBtnEvents(GButton button) { //_CODE_:button1:12356:
     // It is safe to enter your event code here  
 } //_CODE_:button1:12356:
 
 * Do not rename this tab!
 * =========================================================
 */

synchronized public void Communicationwin_draw1(GWinApplet appc, GWinData data) { //_CODE_:Communicationwindow1:979313:
  appc.background(230);
} //_CODE_:Communicationwindow1:979313:

public void SerialPortdropList1_click1(GDropList source, GEvent event) { //_CODE_:SerialPortdropList1:506420:
//  println("SerialPortdropList1 - GDropList event occured " + System.currentTimeMillis()%10000000 );
//  println(SerialPortdropList1.getSelectedIndex());
  println(SerialPortdropList1.getSelectedText());
} //_CODE_:SerialPortdropList1:506420:

public void BaudRatedropList1_click1(GDropList source, GEvent event) { //_CODE_:BaudRatedropList1:326242:
//  println("BaudRatedropList1 - GDropList event occured " + System.currentTimeMillis()%10000000 );
  BaudrateNumber = int(BaudRatedropList1.getSelectedText());
  println(BaudrateNumber);

} //_CODE_:BaudRatedropList1:326242:

public void OpenPortbutton1_click1(GButton source, GEvent event) { //_CODE_:OpenPortbutton1:675630:
//  println("OpenPortbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
//serialCommunication.openPort();
  openSerialPort();
} //_CODE_:OpenPortbutton1:675630:

public void ClosePortbutton1_click1(GButton source, GEvent event) { //_CODE_:ClosePortbutton1:641413:
//  println("ClosePortbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
//serialCommunication.stopPort();
  myPort.stop(); 
} //_CODE_:ClosePortbutton1:641413:

synchronized public void Controlswin_draw1(GWinApplet appc, GWinData data) { //_CODE_:Controlswindow1:488368:
  appc.background(230);
} //_CODE_:Controlswindow1:488368:

public void Readbutton1_click1(GButton source, GEvent event) { //_CODE_:Readbutton1:366668:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
//command.readIDC();
read();
} //_CODE_:Readbutton1:366668:

public void Startbutton1_click1(GButton source, GEvent event) { //_CODE_:Startbutton1:421312:
//  println("Startbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  jsonOut = new JSONObject();
  jsonOut.setString("Command", "Start");
  myPort.write(jsonOut.toString());
//myPort.write("Start");
analysisStarted = true;
//  makefilename(dataArrayF1);
  createDAQFile();
//startAnalysis();
//read();
} //_CODE_:Startbutton1:421312:

public void Stopbutton2_click1(GButton source, GEvent event) { //_CODE_:Stopbutton2:747848:
//  println("Stopbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
analysisStarted = false;
closeDAQFile();
} //_CODE_:Stopbutton2:747848:

public void Configurebutton1_click1(GButton source, GEvent event) { //_CODE_:Configurebutton1:653112:
//  println("Configurebutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
configure();
} //_CODE_:Configurebutton1:653112:

public void Calibratebutton2_click1(GButton source, GEvent event) { //_CODE_:Calibratebutton2:634349:
//  println("Calibratebutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
//calibrate();
calibrateRelative();
} //_CODE_:Calibratebutton2:634349:

public void LoadCLBRbutton3_click1(GButton source, GEvent event) { //_CODE_:LoadCLBRbutton3:659883:
//  println("LoadCLBRbutton3 - GButton event occured " + System.currentTimeMillis()%10000000 );
loadClbr();
} //_CODE_:LoadCLBRbutton3:659883:

public void IDCparamsbutton1_click1(GButton source, GEvent event) { //_CODE_:IDCparamsbutton1:778306:
//  println("IDCparamsbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
sendCommands();
} //_CODE_:IDCparamsbutton1:778306:

public void SelectFolderbutton1_click1(GButton source, GEvent event) { //_CODE_:SelectFolderbutton1:999450:
//  println("SelectFolderbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
selectFolder("Select folder", "folderSelected");
} //_CODE_:SelectFolderbutton1:999450:

synchronized public void GraphCtrlwin_draw1(GWinApplet appc, GWinData data) { //_CODE_:GraphCtrlwindow1:987381:
  appc.background(230);
} //_CODE_:GraphCtrlwindow1:987381:

public void yUpbutton1_click1(GButton source, GEvent event) { //_CODE_:yUpbutton1:596525:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
yMax = yMax + (yMax-yMin)/4;
graphInit();
} //_CODE_:yUpbutton1:596525:

public void yDownbutton2_click1(GButton source, GEvent event) { //_CODE_:yDownbutton2:273815:
//  println("button2 - GButton event occured " + System.currentTimeMillis()%10000000 );
yMax = yMax - (yMax-yMin)/4;
graphInit();
} //_CODE_:yDownbutton2:273815:

public void ybotDwonbutton1_click1(GButton source, GEvent event) { //_CODE_:ybotUPbutton1:752903:
//  println("ybotUPbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
yMin = yMin + (yMax-yMin)/4;
graphInit();
} //_CODE_:ybotUPbutton1:752903:

public void ybotDownbutton2_click1(GButton source, GEvent event) { //_CODE_:ybotDownbutton2:579429:
//  println("ybotDownbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
yMin = yMin - (yMax-yMin)/4;
graphInit();
} //_CODE_:ybotDownbutton2:579429:

public void Graph1checkbox1_clicked1(GCheckbox source, GEvent event) { //_CODE_:Graph1checkbox1:238630:
  println("Graph1checkbox1 - GCheckbox event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:Graph1checkbox1:238630:

public void Graph2checkbox1_clicked1(GCheckbox source, GEvent event) { //_CODE_:Garph2checkbox1:507021:
  println("Garph2checkbox1 - GCheckbox event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:Garph2checkbox1:507021:

public void Graph3checkbox1_clicked1(GCheckbox source, GEvent event) { //_CODE_:Graph3checkbox1:210501:
  println("Graph3checkbox1 - GCheckbox event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:Graph3checkbox1:210501:

public void G1upbutton1_click1(GButton source, GEvent event) { //_CODE_:G1upbutton1:691806:
//  println("G1upbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
dataArrayNum1++;
graphInit();
//G1indtextfield1.setText(Integer.toString(dataArrayNum1));
//F1textfield1.setText(Float.toString(dataArrayF1));
} //_CODE_:G1upbutton1:691806:

public void G1downbutton1_click1(GButton source, GEvent event) { //_CODE_:G1downbutton1:278715:
//  println("G1downbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
dataArrayNum1--;
graphInit();

//F1textfield1.setText(Float.toString(dataArrayF1));
} //_CODE_:G1downbutton1:278715:

public void G2downbutton1_click1(GButton source, GEvent event) { //_CODE_:G2downbutton1:298883:
//  println("G2downbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
dataArrayNum2--;
graphInit();
//G2indtextfield1.setText(Integer.toString(dataArrayNum2));
//F2textfield1.setText(Float.toString(dataArrayF2));
} //_CODE_:G2downbutton1:298883:

public void G3downbutton1_click1(GButton source, GEvent event) { //_CODE_:G3downbutton1:992916:
//  println("G3downbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
dataArrayNum3--;
graphInit();
//G3indtextfield1.setText(Integer.toString(dataArrayNum3));
//F3textfield1.setText(Float.toString(dataArrayF3));
} //_CODE_:G3downbutton1:992916:

public void G2upbutton1_click1(GButton source, GEvent event) { //_CODE_:G2upbutton1:804935:
//  println("G2upbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
dataArrayNum2++;
graphInit();

//F2textfield1.setText(Float.toString(dataArrayF2));
} //_CODE_:G2upbutton1:804935:

public void G3upbutton1_click1(GButton source, GEvent event) { //_CODE_:G3upbutton1:417497:
//  println("G3upbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
dataArrayNum3++;
graphInit();

//F3textfield1.setText(Float.toString(dataArrayF3));
} //_CODE_:G3upbutton1:417497:

public void G1indtextfield1_change1(GTextField source, GEvent event) { //_CODE_:G1indtextfield1:568099:
//  println("G1indtextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );

} //_CODE_:G1indtextfield1:568099:

public void G2indtextfield1_change1(GTextField source, GEvent event) { //_CODE_:G2indtextfield1:499567:
//  println("G2indtextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );

} //_CODE_:G2indtextfield1:499567:

public void G3indtextfield1_change1(GTextField source, GEvent event) { //_CODE_:G3indtextfield1:831100:
//  println("G3indtextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );

} //_CODE_:G3indtextfield1:831100:

public void F1textfield1_change1(GTextField source, GEvent event) { //_CODE_:F1textfield1:303332:
  println("F1textfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:F1textfield1:303332:

public void F2textfield1_change1(GTextField source, GEvent event) { //_CODE_:F2textfield1:434687:
  println("F2textfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:F2textfield1:434687:

public void F3textfield1_change1(GTextField source, GEvent event) { //_CODE_:F3textfield1:303454:
  println("F3textfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:F3textfield1:303454:

synchronized public void Tempwin_draw1(GWinApplet appc, GWinData data) { //_CODE_:Tempwindow1:704011:
  appc.background(230);
} //_CODE_:Tempwindow1:704011:

synchronized public void win_draw1(GWinApplet appc, GWinData data) { //_CODE_:window1:765719:
  appc.background(230);
} //_CODE_:window1:765719:

public void StartFtextfield1_change1(GTextField source, GEvent event) { //_CODE_:StartFtextfield1:545378:
//  println("StartFtextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
startFreqeuncy = float(StartFtextfield1.getText());
//println(startFreqeuncy);
} //_CODE_:StartFtextfield1:545378:

public void IncFtextfield1_change1(GTextField source, GEvent event) { //_CODE_:IncFtextfield1:320701:
//  println("IncFtextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
incFrequency = float(IncFtextfield1.getText());
//println(incFrequency);
} //_CODE_:IncFtextfield1:320701:

public void NoInctextfield1_change1(GTextField source, GEvent event) { //_CODE_:NoInctextfield1:720673:
//  println("NoInctextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
numberInc = int(NoInctextfield1.getText());
//println(numberInc);
} //_CODE_:NoInctextfield1:720673:

public void SetPbutton1_click1(GButton source, GEvent event) { //_CODE_:SetPbutton1:527342:
//  println("SetPbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
sendCommands();
} //_CODE_:SetPbutton1:527342:



// Create all the GUI controls. 
// autogenerated do not edit
public void createGUI(){
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
  G4P.setCursor(ARROW);
  if(frame != null)
    frame.setTitle("Sketch Window");
  Communicationwindow1 = new GWindow(this, "Communication Settings", 0, 0, 330, 120, false, JAVA2D);
  Communicationwindow1.setActionOnClose(G4P.CLOSE_WINDOW);
  Communicationwindow1.addDrawHandler(this, "Communicationwin_draw1");
  SerialPortdropList1 = new GDropList(Communicationwindow1.papplet, 10, 10, 90, 66, 3);
  SerialPortdropList1.setItems(serialArray, 0);
  SerialPortdropList1.addEventHandler(this, "SerialPortdropList1_click1");
  BaudRatedropList1 = new GDropList(Communicationwindow1.papplet, 110, 10, 90, 176, 8);
  BaudRatedropList1.setItems(loadStrings("list_326242"), 5);
  BaudRatedropList1.addEventHandler(this, "BaudRatedropList1_click1");
  OpenPortbutton1 = new GButton(Communicationwindow1.papplet, 210, 10, 50, 30);
  OpenPortbutton1.setText("Open");
  OpenPortbutton1.addEventHandler(this, "OpenPortbutton1_click1");
  ClosePortbutton1 = new GButton(Communicationwindow1.papplet, 270, 10, 50, 30);
  ClosePortbutton1.setText("Close");
  ClosePortbutton1.addEventHandler(this, "ClosePortbutton1_click1");
  Controlswindow1 = new GWindow(this, "Controls", 330, 0, 280, 130, false, JAVA2D);
  Controlswindow1.addDrawHandler(this, "Controlswin_draw1");
  Readbutton1 = new GButton(Controlswindow1.papplet, 10, 10, 80, 30);
  Readbutton1.setText("Read");
  Readbutton1.addEventHandler(this, "Readbutton1_click1");
  Startbutton1 = new GButton(Controlswindow1.papplet, 10, 50, 80, 30);
  Startbutton1.setText("Start");
  Startbutton1.addEventHandler(this, "Startbutton1_click1");
  Stopbutton2 = new GButton(Controlswindow1.papplet, 10, 90, 80, 30);
  Stopbutton2.setText("Stop");
  Stopbutton2.addEventHandler(this, "Stopbutton2_click1");
  Configurebutton1 = new GButton(Controlswindow1.papplet, 100, 10, 80, 30);
  Configurebutton1.setText("Configure");
  Configurebutton1.addEventHandler(this, "Configurebutton1_click1");
  Calibratebutton2 = new GButton(Controlswindow1.papplet, 100, 50, 80, 30);
  Calibratebutton2.setText("Calibrate");
  Calibratebutton2.addEventHandler(this, "Calibratebutton2_click1");
  LoadCLBRbutton3 = new GButton(Controlswindow1.papplet, 100, 90, 80, 30);
  LoadCLBRbutton3.setText("Load Calibration");
  LoadCLBRbutton3.addEventHandler(this, "LoadCLBRbutton3_click1");
  IDCparamsbutton1 = new GButton(Controlswindow1.papplet, 190, 10, 80, 30);
  IDCparamsbutton1.setText("Sweep Parameters");
  IDCparamsbutton1.addEventHandler(this, "IDCparamsbutton1_click1");
  SelectFolderbutton1 = new GButton(Controlswindow1.papplet, 190, 90, 80, 30);
  SelectFolderbutton1.setText("Select Folder");
  SelectFolderbutton1.addEventHandler(this, "SelectFolderbutton1_click1");
  GraphCtrlwindow1 = new GWindow(this, "GraphControl", 0, 140, 400, 120, false, JAVA2D);
  GraphCtrlwindow1.addDrawHandler(this, "GraphCtrlwin_draw1");
  yUpbutton1 = new GButton(GraphCtrlwindow1.papplet, 10, 10, 40, 30);
  yUpbutton1.setText("↑Ytop");
  yUpbutton1.addEventHandler(this, "yUpbutton1_click1");
  yDownbutton2 = new GButton(GraphCtrlwindow1.papplet, 10, 50, 40, 30);
  yDownbutton2.setText("↓Ytop");
  yDownbutton2.addEventHandler(this, "yDownbutton2_click1");
  ybotUPbutton1 = new GButton(GraphCtrlwindow1.papplet, 60, 10, 60, 30);
  ybotUPbutton1.setText("↑Ybottom");
  ybotUPbutton1.addEventHandler(this, "ybotDwonbutton1_click1");
  ybotDownbutton2 = new GButton(GraphCtrlwindow1.papplet, 60, 50, 60, 30);
  ybotDownbutton2.setText("↓Ybottom");
  ybotDownbutton2.addEventHandler(this, "ybotDownbutton2_click1");
  Graph1checkbox1 = new GCheckbox(GraphCtrlwindow1.papplet, 130, 10, 70, 20);
  Graph1checkbox1.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  Graph1checkbox1.setText("Graph 1");
  Graph1checkbox1.setOpaque(false);
  Graph1checkbox1.addEventHandler(this, "Graph1checkbox1_clicked1");
  Graph1checkbox1.setSelected(true);
  Garph2checkbox1 = new GCheckbox(GraphCtrlwindow1.papplet, 130, 40, 70, 20);
  Garph2checkbox1.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  Garph2checkbox1.setText("Graph2");
  Garph2checkbox1.setLocalColorScheme(GCScheme.RED_SCHEME);
  Garph2checkbox1.setOpaque(false);
  Garph2checkbox1.addEventHandler(this, "Graph2checkbox1_clicked1");
  Graph3checkbox1 = new GCheckbox(GraphCtrlwindow1.papplet, 130, 70, 70, 20);
  Graph3checkbox1.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  Graph3checkbox1.setText("Graph 3");
  Graph3checkbox1.setOpaque(false);
  Graph3checkbox1.addEventHandler(this, "Graph3checkbox1_clicked1");
  G1upbutton1 = new GButton(GraphCtrlwindow1.papplet, 200, 10, 20, 20);
  G1upbutton1.setText("↑");
  G1upbutton1.addEventHandler(this, "G1upbutton1_click1");
  G1downbutton1 = new GButton(GraphCtrlwindow1.papplet, 220, 10, 20, 20);
  G1downbutton1.setText("↓");
  G1downbutton1.addEventHandler(this, "G1downbutton1_click1");
  G2downbutton1 = new GButton(GraphCtrlwindow1.papplet, 220, 40, 20, 20);
  G2downbutton1.setText("↓");
  G2downbutton1.addEventHandler(this, "G2downbutton1_click1");
  G3downbutton1 = new GButton(GraphCtrlwindow1.papplet, 220, 70, 20, 20);
  G3downbutton1.setText("↓");
  G3downbutton1.addEventHandler(this, "G3downbutton1_click1");
  G2upbutton1 = new GButton(GraphCtrlwindow1.papplet, 200, 40, 20, 20);
  G2upbutton1.setText("↑");
  G2upbutton1.addEventHandler(this, "G2upbutton1_click1");
  G3upbutton1 = new GButton(GraphCtrlwindow1.papplet, 200, 70, 20, 20);
  G3upbutton1.setText("↑");
  G3upbutton1.addEventHandler(this, "G3upbutton1_click1");
  G1indtextfield1 = new GTextField(GraphCtrlwindow1.papplet, 240, 10, 30, 20, G4P.SCROLLBARS_NONE);
  G1indtextfield1.setOpaque(true);
  G1indtextfield1.addEventHandler(this, "G1indtextfield1_change1");
  G2indtextfield1 = new GTextField(GraphCtrlwindow1.papplet, 240, 40, 30, 20, G4P.SCROLLBARS_NONE);
  G2indtextfield1.setOpaque(true);
  G2indtextfield1.addEventHandler(this, "G2indtextfield1_change1");
  G3indtextfield1 = new GTextField(GraphCtrlwindow1.papplet, 240, 70, 30, 20, G4P.SCROLLBARS_NONE);
  G3indtextfield1.setOpaque(true);
  G3indtextfield1.addEventHandler(this, "G3indtextfield1_change1");
  F1label1 = new GLabel(GraphCtrlwindow1.papplet, 270, 10, 40, 20);
  F1label1.setText("F1,Hz");
  F1label1.setOpaque(false);
  F2label1 = new GLabel(GraphCtrlwindow1.papplet, 270, 40, 40, 20);
  F2label1.setText("F2,Hz");
  F2label1.setOpaque(false);
  F3label1 = new GLabel(GraphCtrlwindow1.papplet, 270, 70, 40, 20);
  F3label1.setText("F3,Hz");
  F3label1.setOpaque(false);
  F1textfield1 = new GTextField(GraphCtrlwindow1.papplet, 310, 10, 80, 20, G4P.SCROLLBARS_NONE);
  F1textfield1.setOpaque(true);
  F1textfield1.addEventHandler(this, "F1textfield1_change1");
  F2textfield1 = new GTextField(GraphCtrlwindow1.papplet, 310, 40, 80, 20, G4P.SCROLLBARS_NONE);
  F2textfield1.setOpaque(true);
  F2textfield1.addEventHandler(this, "F2textfield1_change1");
  F3textfield1 = new GTextField(GraphCtrlwindow1.papplet, 310, 70, 80, 20, G4P.SCROLLBARS_NONE);
  F3textfield1.setOpaque(true);
  F3textfield1.addEventHandler(this, "F3textfield1_change1");
  Tempwindow1 = new GWindow(this, "Temperature", 500, 0, 320, 200, false, JAVA2D);
  Tempwindow1.setActionOnClose(G4P.CLOSE_WINDOW);
  Tempwindow1.addDrawHandler(this, "Tempwin_draw1");
  window1 = new GWindow(this, "Window title", 0, 400, 240, 140, false, JAVA2D);
  window1.addDrawHandler(this, "win_draw1");
  StartFlabel1 = new GLabel(window1.papplet, 10, 10, 110, 20);
  StartFlabel1.setText("Start Frequency, Hz");
  StartFlabel1.setOpaque(false);
  StartFtextfield1 = new GTextField(window1.papplet, 130, 10, 100, 30, G4P.SCROLLBARS_NONE);
  StartFtextfield1.setText("1000.0");
  StartFtextfield1.setOpaque(true);
  StartFtextfield1.addEventHandler(this, "StartFtextfield1_change1");
  IncFlabel1 = new GLabel(window1.papplet, 10, 40, 110, 20);
  IncFlabel1.setText("Increment, Hz");
  IncFlabel1.setOpaque(false);
  NoInclabel1 = new GLabel(window1.papplet, 10, 70, 110, 20);
  NoInclabel1.setText("# of Increments");
  NoInclabel1.setOpaque(false);
  IncFtextfield1 = new GTextField(window1.papplet, 130, 40, 100, 30, G4P.SCROLLBARS_NONE);
  IncFtextfield1.setText("1000.0");
  IncFtextfield1.setOpaque(true);
  IncFtextfield1.addEventHandler(this, "IncFtextfield1_change1");
  NoInctextfield1 = new GTextField(window1.papplet, 130, 70, 100, 30, G4P.SCROLLBARS_NONE);
  NoInctextfield1.setText("20");
  NoInctextfield1.setOpaque(true);
  NoInctextfield1.addEventHandler(this, "NoInctextfield1_change1");
  SetPbutton1 = new GButton(window1.papplet, 10, 100, 110, 30);
  SetPbutton1.setText("Set Sweep Parameters");
  SetPbutton1.addEventHandler(this, "SetPbutton1_click1");
}

// Variable declarations 
// autogenerated do not edit
GWindow Communicationwindow1;
GDropList SerialPortdropList1; 
GDropList BaudRatedropList1; 
GButton OpenPortbutton1; 
GButton ClosePortbutton1; 
GWindow Controlswindow1;
GButton Readbutton1; 
GButton Startbutton1; 
GButton Stopbutton2; 
GButton Configurebutton1; 
GButton Calibratebutton2; 
GButton LoadCLBRbutton3; 
GButton IDCparamsbutton1; 
GButton SelectFolderbutton1; 
GWindow GraphCtrlwindow1;
GButton yUpbutton1; 
GButton yDownbutton2; 
GButton ybotUPbutton1; 
GButton ybotDownbutton2; 
GCheckbox Graph1checkbox1; 
GCheckbox Garph2checkbox1; 
GCheckbox Graph3checkbox1; 
GButton G1upbutton1; 
GButton G1downbutton1; 
GButton G2downbutton1; 
GButton G3downbutton1; 
GButton G2upbutton1; 
GButton G3upbutton1; 
GTextField G1indtextfield1; 
GTextField G2indtextfield1; 
GTextField G3indtextfield1; 
GLabel F1label1; 
GLabel F2label1; 
GLabel F3label1; 
GTextField F1textfield1; 
GTextField F2textfield1; 
GTextField F3textfield1; 
GWindow Tempwindow1;
GWindow window1;
GLabel StartFlabel1; 
GTextField StartFtextfield1; 
GLabel IncFlabel1; 
GLabel NoInclabel1; 
GTextField IncFtextfield1; 
GTextField NoInctextfield1; 
GButton SetPbutton1; 

