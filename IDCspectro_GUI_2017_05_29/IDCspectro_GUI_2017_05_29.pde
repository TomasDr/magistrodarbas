import org.gwoptics.graphics.graph2D.Graph2D;
import org.gwoptics.graphics.graph2D.traces.ILine2DEquation;
import org.gwoptics.graphics.graph2D.traces.RollingLine2DTrace;

import processing.serial.*;
import java.awt.event.*;
//import java.lang.*;
import g4p_controls.*;
import java.awt.event.*;
import cc.arduino.*;

public void setup(){
  size(1000, 480, JAVA2D);
//  frameRate(24);
  classesInit();
  createGUI();
//  customGUI();
  
  frame.setResizable(true);
  frame.addComponentListener(new ComponentAdapter() {
  public void componentResized(ComponentEvent e) {
    if(e.getSource()==frame) {
    redraw();
    redrawGraph = true;
    }
  }
  });
  
graphInit();
}

public void draw(){  
  if (analysisStarted == true){
  background(200);
  g.draw();
  
  if (redrawGraph == true){
   graphInit();
   redrawGraph = false; 
  }
  }
  background(200);
  indicators();
}

public void classesInit(){
  SerialComm serialCommunication = new SerialComm();
  serialArray = new String[serialCommunication.getPorts().length];
  serialArray = serialCommunication.getPorts();
  
  Commands command = new Commands(myPort);
  
  procJSON processJSON = new procJSON(json);
  
  gainFactorArray = new float[511];
  systemPhaseArray = new float[511];
  impedanceDraw = new float[50];
}


