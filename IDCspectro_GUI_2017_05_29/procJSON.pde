public class procJSON{
 JSONObject json;
 public procJSON(JSONObject json){
  this.json = json; 
 }
 
 public JSONObject readJSON(String inString){
   json = JSONObject.parse(inString);
   return json;
 }
 
 public JSONArray getFrequency(JSONObject json){
   JSONArray frequencyJSON = json.getJSONArray("frequency");
   return frequencyJSON;
 }
 
 public JSONArray getRealVal(JSONObject json){
   JSONArray realPartJSON = json.getJSONArray("realPart");
   return realPartJSON;
 }
 
  public JSONArray getImgVal(JSONObject json){
   JSONArray imagPartJSON = json.getJSONArray("imagPart"); // imaginary part to json array conversion
   return imagPartJSON;
 }
 
}

 public JSONObject readJSONstring(String inString){
   json = JSONObject.parse(inString);
   return json;
 }
 
  public JSONArray getJSONFrequency(JSONObject json){
   JSONArray frequencyJSON = json.getJSONArray("frequency");
   return frequencyJSON;
 }
 
  public float[] getFrequencyArray(JSONObject json){
    return json.getJSONArray("frequency").getFloatArray();
  }
  
  public int[] getRealValArray(JSONObject json){
    return json.getJSONArray("realPart").getIntArray();
  }
  
  public int[] getImgValArray(JSONObject json){
    return json.getJSONArray("imagPart").getIntArray();
  }
  
  public float getTemp(JSONObject json){
   return json.getFloat("temperature");
  }
  
  public float getTime(JSONObject json){
   return json.getFloat("time");
  }
  
  public String getJsonStatus(JSONObject json){
    return json.getString("status");
  }
  
  public float[] getPhase(int[] realValArray, int[] imgValArray){
    float[] phaseArrayData = new float[realValArray.length];
      for (int i = 0; i < realValArray.length; i++){
    //phase = atan2(imgVal, realVal);   // theta = arctan (imaginary part/real part)
      phaseArrayData[i] = atan2(realValArray[i], imgValArray[i]) - systemPhaseArray[i];
      }
      return phaseArrayData;
  }
  
  public float[] getMagnitude(int[] realValArray, int[] imgValArray){
    float[] magnitudeArray = new float[realValArray.length];
      for (int i = 0; i < realValArray.length; i++){
    //phase = atan2(imgVal, realVal);   // theta = arctan (imaginary part/real part)
      magnitudeArray[i] = sqrt(pow(realValArray[i], 2) + pow(imgValArray[i], 2));
      }
      return magnitudeArray;
  }
  
  public float[] getImpedance(float[] magnitudeArray, float[] gainFactorArray){
    float[] impdeanceArray = new float[magnitudeArray.length];
      for (int i = 0; i < magnitudeArray.length; i++){
    //phase = atan2(imgVal, realVal);   // theta = arctan (imaginary part/real part)
      impdeanceArray[i] = 1.0/(gainFactorArray[i] * magnitudeArray[i]);
      }
      return impdeanceArray;
  }
  
  public float[] getRealPart(float[] impdeanceArray, float[] phaseArray){
    float[] getRealPartArray = new float[impdeanceArray.length];
      for (int i = 0; i < impdeanceArray.length; i++){
    //phase = atan2(imgVal, realVal);   // theta = arctan (imaginary part/real part)
      getRealPartArray[i] = impdeanceArray[i] * cos(phaseArray[i]);
      }
      return getRealPartArray;
  }
  
  public float[] getImgPart(float[] impdeanceArray, float[] phaseArray){
    float[] getImgPartArray = new float[impdeanceArray.length];
      for (int i = 0; i < impdeanceArray.length; i++){
    //phase = atan2(imgVal, realVal);   // theta = arctan (imaginary part/real part)
      getImgPartArray[i] = impdeanceArray[i] * sin (phaseArray[i]);
      //getImgPartArray[i] = -1.0 * impdeanceArray[i] * sin (phaseArray[i]);
      }
      return getImgPartArray;
  }
  
  public float[] setSysPhase(int[] realValArray, int[] imgValArray)
  {
    float[] sysPhaseData = new float[realValArray.length];
      for (int i = 0; i < realValArray.length; i++){
    //phase = atan2(imgVal, realVal);   // theta = arctan (imaginary part/real part)
      sysPhaseData[i] = atan2(realValArray[i], imgValArray[i]);
      }
      return sysPhaseData;
  }
    
  public void setGainFactor(float[] magnitudeArray, float calImpedance){
    gainFactorArray = new float[magnitudeArray.length];
    for (int i = 0; i < magnitudeArray.length; i++){
     gainFactorArray[i] = (1.0/calImpedance)/magnitudeArray[i];
    }
  }
  
