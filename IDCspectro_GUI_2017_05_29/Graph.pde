int yMax = 2000000, yMin = 0, ySpace, xMax = 360, xSpace, lineWidth = 2;


void graphInit(){
  r1 = new RollingLine2DTrace(new eq1(),100,0.1f);
  r2 = new RollingLine2DTrace(new eq2(),100,0.1f);
  r3 = new RollingLine2DTrace(new eq3(),100,0.1f);
  t = new RollingLine2DTrace(new eq1(),100,0.1f);
  r2.setTraceColour(255, 0, 0);
  r3.setTraceColour(0, 0, 255);
  r1.setLineWidth(lineWidth);
  r2.setLineWidth(lineWidth);
  r3.setLineWidth(lineWidth);
  
  ySpace = (yMax - yMin)/4;
  xSpace = xMax/6;
  
  g = new Graph2D(this, width-width/5, height-height/5, false);
  g.setYAxisMax(yMax);
  g.setYAxisMin(yMin);
  g.addTrace(r1);
  g.addTrace(r2);
  g.addTrace(r3);
  g.position.y = 50;
  g.position.x = 100;
  g.setYAxisTickSpacing(ySpace);
  g.setXAxisTickSpacing(xSpace);
  g.setXAxisMax(xMax);
}

class eq1 implements ILine2DEquation{
  public double computePoint(double x,int pos) {
    return impedanceDraw[dataArrayNum1];
  }    
}

class eq2 implements ILine2DEquation{
  public double computePoint(double x,int pos) {
    return impedanceDraw[dataArrayNum2];
  }    
}

class eq3 implements ILine2DEquation{
  public double computePoint(double x,int pos) {
    return impedanceDraw[dataArrayNum3];
  }    
}

