public class Commands{
  Serial myPort;
  
  public Commands(Serial myPort){
    this.myPort = myPort;
  }
  
 public void readIDC(){
   myPort.write("Meas");
 } 
}

 public void read(){
//   myPort.write("Meas");
  jsonOut = new JSONObject();
  jsonOut.setString("Command", "Meas");
  myPort.write(jsonOut.toString()); //{"Command": "Meas"}

 // println(jsonOut.toString());
 } 
 
  public void calibrate(){
//   myPort.write("Clbr");
  jsonOut = new JSONObject();
  jsonOut.setString("Command", "Clbr");
  myPort.write(jsonOut.toString());
 } 
 
  public void loadClbr(){

 } 
 
  public void configure(){
//   myPort.write("Conf");
  jsonOut = new JSONObject();
  jsonOut.setString("Command", "Conf");
  myPort.write(jsonOut.toString());
 } 
 
   public void temperature(){
   myPort.write("Temp");
 } 
 
 public void startAnalysis(){
  jsonOut = new JSONObject();
  jsonOut.setString("Command", "Meas");
  myPort.write(jsonOut.toString());
  

//   myPort.write("Meas");
//   analysisStarted = true;
//   readProcess = true;
 }
 
 public void sendCommands(){
  jsonOut = new JSONObject();
  jsonOut.setString("Command", "Params");
  jsonOut.setFloat("StrtFr", startFreqeuncy);
  jsonOut.setFloat("Incr", incFrequency);
  jsonOut.setInt("NIncrs", numberInc);
  
//  println(jsonOut.toString());
  myPort.write(jsonOut.toString());
 }
