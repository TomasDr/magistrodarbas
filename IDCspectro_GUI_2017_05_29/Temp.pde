public void Tempwin_draw1(PApplet app, GWinData data){
    app.background(255);
    app.strokeWeight(2);
    // draw black line to current mouse position
    app.stroke(0);
    app.line(app.width / 2, app.height/2, app.mouseX, app.mouseY);
  }
