String fullPathName, joinedDate, DAQfileName;

PrintWriter outputFile1, outputFile2, outputFile3, specFile;

void folderSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    fullPathName = selection.getAbsolutePath();
  }
}

String makeSpecFile(float time)
{
  String[] fileParts = new String[6];
  fileParts[0] = fullPathName;
  fileParts[1] = "/";
  fileParts[2] = joinedDate;
  fileParts[3] = "/";
  fileParts[4] = Float.toString(time);
  fileParts[5] = ".txt";
  return join(fileParts,"");
}

 String makefilename(float dataArrayF) //generates filename
{
  int d = day();    // Values from 1 - 31
  String sd = nfs(d, 2);
  int mo = month();  // Values from 1 - 12
  String smo = nfs(mo, 2);
  int y = year();   // 2003, 2004, 2005, etc. 
  String sy = nfs(y, 4);
  int mi = minute();
  String smi = nfs(mi, 2);
  int se = second();
  String sse = nfs(se, 2);
  int h = hour();
  String sh = nfs(h, 2);
//  String ending = ".txt";

  String[] date = new String[6];
  date[0] = sy; 
  date[1] = smo; 
  date[2] = sd; 
  date[3] = sh; 
  date[4] = smi; 
  date[5] = sse; 

//  date[9] = ending;
  joinedDate = join(date, "");

  String[] fileParts = new String[7];
  fileParts[0] = fullPathName;
  fileParts[1] = "/";
  fileParts[2] = joinedDate;
  fileParts[3] = "_";
  fileParts[4] = Float.toString(dataArrayF);
  fileParts[5] = "_Hz";
  fileParts[6] = ".txt";
  DAQfileName = join(fileParts,"");
  println(DAQfileName);
  return DAQfileName;
}

void createSpecFile()
{
  specFile = createWriter(makeSpecFile(currentTime));
}

void createDAQFile(){
  outputFile1 = createWriter(makefilename(dataArrayF1));
  outputFile2 = createWriter(makefilename(dataArrayF2)); 
  outputFile3 = createWriter(makefilename(dataArrayF3));  
}

void collectSpectra()
{
  for (int i = 0; i < frequencyWrite.length; i++)
  {
  specFile.println(frequencyWrite[i] + " " + impedanceDraw[i] + " " + phaseWrite[i] + " " + realWrite[i] + " " + imgWrite[i]);
  }
}

void collectData() {
  if (analysisStarted == true){
  outputFile1.println(currentTime + " " + impedanceDraw[dataArrayNum1]);
  outputFile2.println(currentTime + " " + impedanceDraw[dataArrayNum2]);
  outputFile3.println(currentTime + " " + impedanceDraw[dataArrayNum3]);
  }
}

void closeSpecFile()
{
  specFile.flush(); // Writes the remaining data to the file
  specFile.close(); // Finishes the file
}

void closeDAQFile() {
  outputFile1.flush(); // Writes the remaining data to the file
  outputFile1.close(); // Finishes the file
  outputFile2.flush(); // Writes the remaining data to the file
  outputFile2.close(); // Finishes the file
  outputFile3.flush(); // Writes the remaining data to the file
  outputFile3.close(); // Finishes the file
}

void getSpectra()
{
  createSpecFile();
  collectSpectra();
  closeSpecFile();
}
