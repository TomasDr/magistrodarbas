
public void customGUI(){
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
  G4P.setCursor(ARROW);
  if(frame != null)
    frame.setTitle("Sketch Window");
  Communicationwindow1 = new GWindow(this, "Communication Settings", 0, 0, 330, 120, false, JAVA2D);
  Communicationwindow1.setActionOnClose(G4P.CLOSE_WINDOW);
  Communicationwindow1.addDrawHandler(this, "Communicationwin_draw1");
  SerialPortdropList1 = new GDropList(Communicationwindow1.papplet, 10, 10, 90, 66, 3);
  SerialPortdropList1.setItems(serialArray, 0);
  SerialPortdropList1.addEventHandler(this, "SerialPortdropList1_click1");
  BaudRatedropList1 = new GDropList(Communicationwindow1.papplet, 110, 10, 90, 176, 8);
  BaudRatedropList1.setItems(loadStrings("list_326242"), 5);
  BaudRatedropList1.addEventHandler(this, "BaudRatedropList1_click1");
  OpenPortbutton1 = new GButton(Communicationwindow1.papplet, 210, 10, 50, 30);
  OpenPortbutton1.setText("Open");
  OpenPortbutton1.addEventHandler(this, "OpenPortbutton1_click1");
  ClosePortbutton1 = new GButton(Communicationwindow1.papplet, 270, 10, 50, 30);
  ClosePortbutton1.setText("Close");
  ClosePortbutton1.addEventHandler(this, "ClosePortbutton1_click1");
  Controlswindow1 = new GWindow(this, "Controls", 330, 0, 240, 130, false, JAVA2D);
  Controlswindow1.addDrawHandler(this, "Controlswin_draw1");
  Readbutton1 = new GButton(Controlswindow1.papplet, 10, 10, 80, 30);
  Readbutton1.setText("Read");
  Readbutton1.addEventHandler(this, "Readbutton1_click1");
  Startbutton1 = new GButton(Controlswindow1.papplet, 10, 50, 80, 30);
  Startbutton1.setText("Start");
  Startbutton1.addEventHandler(this, "Startbutton1_click1");
  Stopbutton2 = new GButton(Controlswindow1.papplet, 10, 90, 80, 30);
  Stopbutton2.setText("Stop");
  Stopbutton2.addEventHandler(this, "Stopbutton2_click1");
}
