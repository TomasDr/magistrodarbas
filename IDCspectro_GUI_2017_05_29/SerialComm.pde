

public class SerialComm {
  
 public String[] getPorts(){
//   if (Serial.list() != null){
     return Serial.list();
//   }
   //else
   //  return {"no devices"};
 }
 
 public void stopPort(){
  myPort.stop(); 
 }
  
 public void openPort(int portIndex, int baudRate){
//     myPort = new Serial(this, Serial.list()[SerialPortdropList1.getSelectedIndex()], BaudrateNumber);
//     myPort = new Serial(this, portIndex, baudRate);
//     myPort.bufferUntil('\n');
 }
  
}

public void openSerialPort(){
     myPort = new Serial(this, Serial.list()[SerialPortdropList1.getSelectedIndex()], BaudrateNumber);
     myPort.bufferUntil('\n');
     println("Port Open");
}

void serialEvent (Serial myPort) {
 // get the ASCII string:
// while (myPort.available() > 0) {
 String inString = myPort.readStringUntil('\n');
  if (inString != null && !inString.isEmpty()) {
 // trim off any whitespace:
 inString = trim(inString);
 println(inString);
  }
  
  JSONObject json;
  float temp, time;
  float[] frequencyArray, phaseArray, magnitudeArray, reapPartArray, imgPartArray, impedanceArray;
  int[] realValArray, imgValArray;
  
  json = JSONObject.parse(inString);
//  println(json);
  int arrayLength = getRealValArray(json).length;
  
  frequencyArray = new float[arrayLength];
  magnitudeArray = new float[arrayLength];
  reapPartArray = new float[arrayLength];  
  imgPartArray = new float[arrayLength];
  impedanceArray = new float[arrayLength];
  phaseArray = new float[arrayLength];
  realValArray = new int[arrayLength];
  imgValArray = new int[arrayLength];
  
//  frequencyArray = getJSONFrequency(json);
  frequencyArray = getFrequencyArray(json);
  realValArray = getRealValArray(json);
  imgValArray = getImgValArray(json);
  temp = getTemp(json);
  time = getTime(json);
  magnitudeArray = getMagnitude(realValArray, imgValArray);
    
  if (calibrationFlag){
  sysPhaseDataArray = setSysPhase(realValArray, imgValArray);
  systemPhaseArray = sysPhaseDataArray;
  setGainFactor(magnitudeArray, calImpedance);
  calibrationFlag = false;
  }
  phaseArray = getPhase(realValArray, imgValArray);
  impedanceArray = getImpedance(magnitudeArray, gainFactorArray);
  reapPartArray = getRealPart(impedanceArray, phaseArray);
  imgPartArray = getImgPart(impedanceArray, phaseArray);
//  println(impedanceArray);
  println(time);
  currentTime = time;
  impedanceDraw = impedanceArray;
  
    //global variables in other functions
   dataArrayF1 = frequencyArray[dataArrayNum1];
   dataArrayF2 = frequencyArray[dataArrayNum2];
   dataArrayF3 = frequencyArray[dataArrayNum3];
  
  jsonStatus = getJsonStatus(json);
  
  frequencyWrite = frequencyArray;
  phaseWrite = phaseArray;
  realWrite = reapPartArray;
  imgWrite = imgPartArray;
  
  updateGlobals();
  
  if (analysisStarted == true){
    startAnalysis();
    collectData();
    getSpectra();
  }
  
}
