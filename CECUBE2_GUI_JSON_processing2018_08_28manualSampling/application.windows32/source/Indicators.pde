void showIndicator(){
String indicatorstring = "no data";
String timeValue = "0";
String TempIndicator = "no data";
String CurrentIndicator = "no data";

  indicatorstring = String.format("%.6f", inByte1);
//  timeValue = str(AnalysisTiming %.1f);
  timeValue = String.format("%.1f", AnalysisTiming);
  
if (startflag == false){
  strokeWeight(0);
  fill(255, 0, 0);
  ellipse(30, height-30, 40, 40);
  }
if (startflag == true){
  strokeWeight(0);
  fill(0, 255, 0);
  ellipse(30, height-30, 40, 40);
  }
  
fill(255);
rect(55, height-55, 100, 40);

textSize(12);
fill(0);
text(indicatorstring, 60, height-25);
text("Value", 60, height-40); 

  TempIndicator = String.format("%.4f", inByte3);
  fill(255);
rect(55, height-85, 100, 30);

textSize(12);
fill(0);
text(TempIndicator, 60, height-58);
text("Temperature", 60, height-72); 

  CurrentIndicator = String.format("%.2f", inByte2);
  fill(255);
rect(5, height-85, 50, 30);

textSize(12);
fill(0);
text(CurrentIndicator, 10, height-58);
text("Current", 10, height-72);

//injectindicators();

fill(255);
rect(160, height-55, 100, 40);

textSize(12);
fill(0);
text(timeValue, 165, height-25);
text("Time (s)", 165, height-40); 

fill(255);
rect(265, height-55, 170, 40);

textSize(12);
fill(0);
text("File Name", 270, height-40);
text(joinedDate, 270, height-25);

fill(255);
rect(440, height-55, 170, 40);

textSize(12);
fill(0);
text("Analysis Time", 445, height-40);
text(String.format("%.1f", AnalysisTime) + " (min)", 445, height-25);
text(String.format("%.1f", AnalysisTime*60) + " (s)", 530, height-25);

fill(255);
rect(615, height-55, width - 615-5, 40);

textSize(12);
fill(0);
text("File Path", 620, height-40);
if(fullPathName != null){
text(fullPathName, 620, height-25); }
//text(String.format("%.1f", AnalysisTime*60) + " (s)", 530, height-25);

} 
void StartTheAnalysis(){
startflag = true;
makefilename();
createDAQFile();
AnalysisTiming = 0.0;
timingsubtract = millis();
index = 0;
float[] timingValueArray = new float[1000000];
float[] measuredValueArray = new float[1000000];
Previoustiming = 0.0;
initializeClasses();
}

void ZeroTimeMark(){
//    println(AnalysisTiming);
  AnalysisTiming = 0.0;
  timingsubtract = millis();
//    println(timingsubtract);
  index = 0;
  float[] timingValueArray = new float[1000000];
  float[] measuredValueArray = new float[1000000];
  Previoustiming = 0.0;
//    println(AnalysisTiming);
  initializeClasses();
}

void injectindicators(){
    if (injectionSatrted == true){
  String InjectionIndicator = "no  data";

  injectionTiming = (injectionTimingTOT * 1000 + injectpresenttime - millis())/1000;
  InjectionIndicator = String.format("%.2f", injectionTiming);
  fill(255);
rect(160, height-85, 100, 30);
  
  textSize(12);
fill(0);
text(InjectionIndicator, 165, height-58);
text("Injection Timer", 165, height-72); 
  
if (injectionTiming <= 0.0){
  injectionSatrted = false;
    fill(255);
rect(160, height-85, 100, 30);

player.play();
delay(1750);

  player.close();

  //since close closes the file, we'll load it again
  player = minim.loadFile("injectionsample.mp3");
redrawAxis = true;
}
}
}
