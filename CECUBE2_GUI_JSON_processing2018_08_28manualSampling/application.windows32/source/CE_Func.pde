void LiftUp() {
  //  TempCompensation = false;
  myPort.write("LIFTUP*");

  //  TempCompensation = true;
}

void LiftDown() {
  //  TempCompensation = false;

  myPort.write("LIFTDOWN*");

  //    TempCompensation = true;
}

void HVON() {
  myPort.write("HVONN*");
}

void HVOFF() {
  myPort.write("HVOFF*");
}

void carouselClockwise() {
  myPort.write("CCLOCK*");
}

void carouselCounterClockwise() {
  myPort.write("CCOUNT*");
}

void adjustClockwise() {
  myPort.write("ACLOCK*");
}

void adjustCounterClockwise() {
  myPort.write("ACOUNT*");
}

void startAnalysis() {
  myPort.write("STARTA*");
}

void stopAnalysis() {
  myPort.write("STOPA*");
}

void setElectrokineticInjection() {
  myPort.write("SETELECTRO*");
}

void setHydrodynamicInjection() {
  myPort.write("SETHYDRO*");
}

void setPumpON() {
  myPort.write("PUMPON*");
}

void setPumpOFF() {
  myPort.write("PUMPOFF*");
}

void setValvesON() {
  myPort.write("VALVESON*");
}

void setValvesOFF() {
  myPort.write("VALVESOFF*");
}

void setSamplingON() {
  myPort.write("SAMPLEON*");
}

void setSamplingOFF() {
  myPort.write("SAMPLEOFF*");
}

void performInjection() {
  myPort.write("INJECT*");
}

void setOnElectroConditioning() {
  myPort.write("ONELC*");
}

void setOffElectroConditioning() {
  myPort.write("OFFELC*");
}

void setOnPressureFlush() {
  myPort.write("ONPFL*");
}

void setOffPressureFlush() {
  myPort.write("OFFPFL*");
}

void setOnVacuumFlush() {
  myPort.write("ONVFL*");
}

void setOffVacuumFlush() {
  myPort.write("OFFVFL*");
}

void selectInjectionTime() {
  if (InjectiondropList1.getSelectedIndex() == 0) {
    injectionTimingTOT = 10.0;
    //    myPort.write("SIPHON10*");
    //    println("SIPHOOINJECT");
  }
  if (InjectiondropList1.getSelectedIndex() == 1) {
    injectionTimingTOT = 20.0;
    //    myPort.write("SIPHON20*");
    //    println("SIPHOOINJECT");
  }
  if (InjectiondropList1.getSelectedIndex() == 2) {
    injectionTimingTOT = 40.0;   
    // myPort.write("SIPHON40*");
    //    println("SIPHOOINJECT");
  }

  if (InjectiondropList1.getSelectedIndex() == 3) {
    injectionTimingTOT = 80.0;    
    //      myPort.write("SIPHON80*");
    //    println("SIPHOOINJECT");
  }
  if (InjectiondropList1.getSelectedIndex() == 4) {
    injectionTimingTOT = 160.0;    
    //    myPort.write("SIPHON160*");
    //    println("SIPHOOINJECT");
  }
  if (InjectiondropList1.getSelectedIndex() == 5) {
    injectionTimingTOT = 320.0;
    //    myPort.write("SIPHON320*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 6) {
    injectionTimingTOT = 1.0;
    //      myPort.write("ELECTRO1*");
    //      println("ELECTROOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 7) {
    injectionTimingTOT = 2.0;      
    //      myPort.write("ELECTRO2*");
    //      println("ELECTROOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 8) {
    injectionTimingTOT = 5.0;
    //      myPort.write("ELECTRO5*");
    //      println("ELECTROOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 9) {
    injectionTimingTOT = 10.0;
    //      myPort.write("ELECTRO10*");
    //      println("ELECTROOINJECT");
  }
}

void setInjection() {
  if (InjectiondropList1.getSelectedIndex() == 0) {
    myPort.write("INJ5*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 1) {
    myPort.write("INJ10*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 2) {
    myPort.write("INJ20*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 3) {
    myPort.write("INJ40*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 4) {
    myPort.write("INJ80*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 5) {
    myPort.write("INJ160*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 6) {
    myPort.write("INJ320*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 7) {
    myPort.write("INJ640*");
    //    println(InjectiondropList1.getSelectedIndex());
  }
}

void setCollection() {
  if (CollectiondropList1.getSelectedIndex() == 0) {
    myPort.write("COL1*");
    //    println("SIPHOOINJECT");
  } else if (CollectiondropList1.getSelectedIndex() == 1) {
    myPort.write("COL2*");
    //    println("SIPHOOINJECT");
  } else if (CollectiondropList1.getSelectedIndex() == 2) {
    myPort.write("COL4*");
    //    println("SIPHOOINJECT");
  } else if (CollectiondropList1.getSelectedIndex() == 3) {
    myPort.write("COL8*");
    //    println("SIPHOOINJECT");
  } else if (CollectiondropList1.getSelectedIndex() == 4) {
    myPort.write("COL16*");
//        println("SIPHOOINJECT");
  }
}

void setAnalysisTime() {
  if (anTimedropList1.getSelectedIndex() == 0) {
    myPort.write("ANT5*");
    //    println("SIPHOOINJECT");
  } else if (anTimedropList1.getSelectedIndex() == 1) {
    myPort.write("ANT10*");
    //    println("SIPHOOINJECT");
  } else if (anTimedropList1.getSelectedIndex() == 2) {
    myPort.write("ANT12*");
    //    println("SIPHOOINJECT");
  } else if (anTimedropList1.getSelectedIndex() == 3) {
    myPort.write("ANT15*");
    //    println("SIPHOOINJECT");
  }
}

