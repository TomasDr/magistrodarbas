public float getCap(JSONObject json) {
  if (json.isNull("c") == true) {
    return 0.0;
  } else
    return json.getFloat("c");
}

public float getTemp(JSONObject json) {
  if (json.isNull("t") == true) {
    return 20.0;
  } else
    return json.getFloat("t");
}

public float getCurrent(JSONObject json) {
  if (json.isNull("i") == true) {
    return 0.0;
  } else
    return json.getFloat("i");
}

