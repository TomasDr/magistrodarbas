public void FindSerialPorts() {
  SerialArray = Serial.list();
  if (SerialArray.length == 0) {
    SerialArray = loadStrings("list_757299");
    nodevice = true;
  } else if (SerialArray.length > 0) {
    nodevice = false;
  }
}

void serialPortSelect() {
  myPort = new Serial(this, Serial.list()[Serial_Ports.getSelectedIndex()], BaudrateNumber);
  println("Serial port index" + Serial_Ports.getSelectedIndex() + " " + "Baud rate" + BaudrateNumber);
  myPort.bufferUntil('\n');
}


void serialEvent (Serial myPort) {
  // get the ASCII string:
  // while (myPort.available() > 0) {
  inString1 = myPort.readStringUntil('\n');
  if (inString1 != null && !inString1.isEmpty()) {
    // trim off any whitespace:
    inString1 = trim(inString1);
    //  println(inString1);

    JSONObject json;
    //  float temperature, capacitance;
    json = JSONObject.parse(inString1);
    if (json == null)
    {
      inString1 = inStringPrev;
    } else
    {
      temperature = getTemp(json);
      capacitance = getCap(json);
      inByte1 = getCap(json);
      //  inByte2 = getCurrent(json);
      inByte3 = getTemp(json);
      inStringPrev = inString1;
    }

    if (inByte1 == 1.234567890) {
      autostartfunction();
    }
    //  StringConversion();
    serialTiming = millis();
    timing = float(millis())/1000.0;
    graphtiming = timing - float(timingsubtract)/1000.0;
    // collectData();
    collectJsonData();
    calculateAnalysisTiming();
    showIndicator();
    calculateValues();
    myElectropherogram.run(pXposition, pYposition, Xposition, Yposition);
    preserveData(graphtiming, inByte1); //float xvalue, float yvalue
    timingfunction();
    AutoStop();
  }
}
//}

void timingfunction() {
  Previoustiming = graphtiming;
  PreviousinByte1 = inByte1;
}

void autostartfunction() {
  // if (inString1.equals(StartPass) == true){
  StopTheAnalysis();
  StartTheAnalysis();
  
  //  println(inString1);
  // }
}

void StopTheAnalysis() {
  if (startflag == true) {
    startflag = false;
    closeDAQFile();
    currentAnalysisRepetition++;
    if (currentAnalysisRepetition > repetitionsNo){
     currentAnalysisNumber++;
     currentAnalysisRepetition = 1;
    }
  }
}

void AutoStop() {
  AnalysisTime = 10.0 + AnalysisButton + SliderAnTime;
  if (startflag == true && AnalysisTiming > AnalysisTime*60.0) {
    StopTheAnalysis();
  }
}

void StringConversion() {
  // String[] ReceivedStrings = split(inString1, " ");
  String[] ReceivedStrings = splitTokens(inString1, " ");
  // println(ReceivedStrings.length + " values found");
  // println(ReceivedStrings[0]);
  // println(ReceivedStrings[1]);
  // println(ReceivedStrings[2]);
  // println(inString1);
  // println(ReceivedStrings[0]);
  //if ("1234567890.123456789".equals(inString1) == true){
  //autostartfunction();
  //}
  // println(inString1);
  // println(StartPass);
  // inByte1 = float(inString1);
  // ReceivedStrings[0] = trim(ReceivedStrings[0]);
  if (ReceivedStrings.length >= 1 && ReceivedStrings[0] != null && !ReceivedStrings[0].isEmpty()) {
    // ReceivedStrings[0] = trim(ReceivedStrings[0]);
    inByte1 = float(ReceivedStrings[0]);
  }

  if (ReceivedStrings.length >= 2 && ReceivedStrings[1] != null && !ReceivedStrings[1].isEmpty()) {
    //  ReceivedStrings[1] = trim(ReceivedStrings[1]);
    inByte2 = float(ReceivedStrings[1]);
    //   println(inByte2);
  }

  if (ReceivedStrings.length >= 3 && ReceivedStrings[2] != null && !ReceivedStrings[2].isEmpty()) {
    //  ReceivedStrings[2] = trim(ReceivedStrings[2]);  
    inByte3 = float(ReceivedStrings[2]);
    // println(inByte3);
  }
}

void handleSerialPort(long timingMarker)
{
  if (timingMarker + serialTimingPeriod <= millis() && serialStarted == true) {
    myPort.stop();
    println("Serial stopped");
    delay(500);
    serialPortSelect();
    timingfunction();
    initializeClasses();
    println("Serial open attempt");
    serialTiming = millis();
  }
  else {}
}

