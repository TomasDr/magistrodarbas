/* =========================================================
 * ====                   WARNING                        ===
 * =========================================================
 * The code in this tab has been generated from the GUI form
 * designer and care should be taken when editing this file.
 * Only add/edit code inside the event handlers i.e. only
 * use lines between the matching comment tags. e.g.

 void myBtnEvents(GButton button) { //_CODE_:button1:12356:
     // It is safe to enter your event code here  
 } //_CODE_:button1:12356:
 
 * Do not rename this tab!
 * =========================================================
 */

synchronized public void Setupwin_draw1(GWinApplet appc, GWinData data) { //_CODE_:window1:457692:
  appc.background(230);
} //_CODE_:window1:457692:

public void OpenSerialbutton1_click1(GButton source, GEvent event) { //_CODE_:OpenSerialbutton:332947:
//  println("OpenSerialbutton - GButton event occured " + System.currentTimeMillis()%10000000 );
if (nodevice == false){
  serialPortSelect();
  timingfunction();
  initializeClasses();
  serialStarted = true;
}
} //_CODE_:OpenSerialbutton:332947:

public void Serial_PortsdropList1_click1(GDropList source, GEvent event) { //_CODE_:Serial_Ports:605734:
  println("dropList1 - GDropList event occured " + System.currentTimeMillis()%10000000 );
  println(Serial_Ports.getSelectedIndex());
  println(Serial_Ports.getSelectedText());
} //_CODE_:Serial_Ports:605734:

public void StopSerialbutton1_click3(GButton source, GEvent event) { //_CODE_:StopSerial:770184:
  println("StopSerial - GButton event occured " + System.currentTimeMillis()%10000000 );
  myPort.stop();
  serialStarted = false;
} //_CODE_:StopSerial:770184:

public void BaudRate_click1(GDropList source, GEvent event) { //_CODE_:BaudRate:868333:
//  println("Serial_Ports4 - GDropList event occured " + System.currentTimeMillis()%10000000 );
BaudrateNumber = int(BaudRate.getSelectedText());
println(BaudrateNumber);
} //_CODE_:BaudRate:868333:

public void Phrasetextfield1_change1(GTextField source, GEvent event) { //_CODE_:Phrasetextfield1:641956:
//  println("Phrasetextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
//println(StartPass);
} //_CODE_:Phrasetextfield1:641956:

public void OKPhrasebutton1_click2(GButton source, GEvent event) { //_CODE_:OKPhrasebutton1:955155:
//  println("OKPhrasebutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
StartPass = Phrasetextfield1.getText();
//println(StartPass);
} //_CODE_:OKPhrasebutton1:955155:

synchronized public void win_draw2(GWinApplet appc, GWinData data) { //_CODE_:window2:639670:
  appc.background(230);
} //_CODE_:window2:639670:

public void ZoomInbutton1_click1(GButton source, GEvent event) { //_CODE_:ZoomIn:293652:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  ZoomIn();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:ZoomIn:293652:

public void ZoomOutbutton2_click1(GButton source, GEvent event) { //_CODE_:ZoomOut:547055:
//  println("button2 - GButton event occured " + System.currentTimeMillis()%10000000 );
//  println(yScaleMaximum);
  ZoomOut();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:ZoomOut:547055:

public void TimePlusbutton3_click1(GButton source, GEvent event) { //_CODE_:TimePlus:301917:
//  println("button3 - GButton event occured " + System.currentTimeMillis()%10000000 );
  TimePlus();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:TimePlus:301917:

public void TimeMinusbutton4_click1(GButton source, GEvent event) { //_CODE_:TimeMinus:450192:
//  println("button4 - GButton event occured " + System.currentTimeMillis()%10000000 );
  TimeMinus();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:TimeMinus:450192:

public void RUNbutton5_click1(GButton source, GEvent event) { //_CODE_:RUN:205394:
//  println("button5 - GButton event occured " + System.currentTimeMillis()%10000000 );
StartTheAnalysis();

} //_CODE_:RUN:205394:

public void STOPbutton6_click1(GButton source, GEvent event) { //_CODE_:STOP:563742:
//  println("button6 - GButton event occured " + System.currentTimeMillis()%10000000 );
startflag = false;
closeDAQFile();
} //_CODE_:STOP:563742:

public void button1_click1(GButton source, GEvent event) { //_CODE_:OffsetUp:500480:
//  println("OffsetUp - GButton event occured " + System.currentTimeMillis()%10000000 );
  OffsetPlus();
    initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:OffsetUp:500480:

public void OffsetDownbutton1_click1(GButton source, GEvent event) { //_CODE_:OffsetDown:201743:
//  println("OffsetDown - GButton event occured " + System.currentTimeMillis()%10000000 );
  OffsetMinus();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:OffsetDown:201743:

public void Timingslider1_change1(GSlider source, GEvent event) { //_CODE_:Timingslider1:686558:
//  println("slider1 - GSlider event occured " + System.currentTimeMillis()%10000000 );
  TimingOffsetSlider = Timingslider1.getValueF();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:Timingslider1:686558:

public void Offsetslider2_change1(GSlider source, GEvent event) { //_CODE_:Offsetslider2:317894:
//  println("slider2 - GSlider event occured " + System.currentTimeMillis()%10000000 );
  OffsetSliderVal = Offsetslider2.getValueF();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:Offsetslider2:317894:

public void Settingsbutton1_click2(GButton source, GEvent event) { //_CODE_:Settingsbutton1:657155:
//  println("Settingsbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
if (SettingsWindow == false)
openSettings();
else if (SettingsWindow == true){
  window1.close();
 SettingsWindow = false; 
}
} //_CODE_:Settingsbutton1:657155:

public void EXITbutton1_click2(GButton source, GEvent event) { //_CODE_:EXITbutton1:287530:
//  println("EXITbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
if (startflag == true)
closeDAQFile();
exit();
} //_CODE_:EXITbutton1:287530:

public void ChoosePathbutton1_click2(GButton source, GEvent event) { //_CODE_:ChoosePathbutton1:390093:
  //println("ChoosePathbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  selectFolder("Select folder", "folderSelected");
} //_CODE_:ChoosePathbutton1:390093:

public void AnalysisUP_click2(GButton source, GEvent event) { //_CODE_:AnalysisUPbutton1:537956:
//  println("AnalysisUPbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
AnalysisTimeUP();
} //_CODE_:AnalysisUPbutton1:537956:

public void AnalysisDown_click2(GButton source, GEvent event) { //_CODE_:AnalysisDownbutton1:222072:
//  println("AnalysisDownbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
AnalysisTimeDown();
} //_CODE_:AnalysisDownbutton1:222072:

public void Analysisslider1_change1(GSlider source, GEvent event) { //_CODE_:Analysisslider1:262109:
//  println("Analysisslider1 - GSlider event occured " + System.currentTimeMillis()%10000000 );
SliderAnTime = Analysisslider1.getValueF();
} //_CODE_:Analysisslider1:262109:

public void buttonCEControls_click3(GButton source, GEvent event) { //_CODE_:buttonCEControls:977250:
//  println("buttonCEControls - GButton event occured " + System.currentTimeMillis()%10000000 );
if (CEcontrolswindow == false)
CE_Controls();
else if (CEcontrolswindow == true){
  window3.close();
 CEcontrolswindow = false; 
}
} //_CODE_:buttonCEControls:977250:

public void buttonCDCControls_click1(GButton source, GEvent event) { //_CODE_:buttonCDCControls:580484:
//  println("buttonCDCControls - GButton event occured " + System.currentTimeMillis()%10000000 );
if (CDCcontrolsWindow == false)
CDC_Controls();
else if (CDCcontrolsWindow == true){
  window4.close();
 CDCcontrolsWindow = false; 
}
} //_CODE_:buttonCDCControls:580484:

public void buttonAutoZero_click3(GButton source, GEvent event) { //_CODE_:buttonAutoZero:986137:
//  println("buttonAutoZero - GButton event occured " + System.currentTimeMillis()%10000000 );
Auto_Zero();
} //_CODE_:buttonAutoZero:986137:

public void buttonAutoZeroDel_click3(GButton source, GEvent event) { //_CODE_:buttonAutoZeroDel:665328:
//  println("buttonAutoZeroDel - GButton event occured " + System.currentTimeMillis()%10000000 );
Auto_Zero_Delete();
} //_CODE_:buttonAutoZeroDel:665328:

public void ZeroTimebutton1_click4(GButton source, GEvent event) { //_CODE_:ZeroTimebutton1:853513:
//  println("ZeroTimebutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
ZeroTimeMark();
} //_CODE_:ZeroTimebutton1:853513:

public void AutoRun_button1_click4(GButton source, GEvent event) { //_CODE_:AutoRun_button1:968922:
//  println("AutoRun_button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
 LiftUp();
   HVON();
   StartTheAnalysis();
} //_CODE_:AutoRun_button1:968922:

public void OffUpSmall_button1_click4(GButton source, GEvent event) { //_CODE_:OffUpSmall_button1:751547:
//  println("OffUpSmall_button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  OffsetUPSmall();
    initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:OffUpSmall_button1:751547:

public void OffDownSmall_button1_click5(GButton source, GEvent event) { //_CODE_:OffDownSmall_button1:315439:
//  println("OffDownSmall_button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  OffsetDOWNSmall();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:OffDownSmall_button1:315439:

public void ATimePlusTenbutton1_click4(GButton source, GEvent event) { //_CODE_:ATimePlusTenbutton1:754106:
//  println("ATimePlusTenbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
AnalysisTimeUPTen();
} //_CODE_:ATimePlusTenbutton1:754106:

public void ATimeminusTenbutton1_click4(GButton source, GEvent event) { //_CODE_:ATimeminusTenbutton1:401252:
//  println("ATimeminusTenbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
AnalysisTimeDownTen();
} //_CODE_:ATimeminusTenbutton1:401252:

public void textfield1_change1(GTextField source, GEvent event) { //_CODE_:TimeAnalysistextfield1:551700:
//  println("TimeAnalysistextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:TimeAnalysistextfield1:551700:

public void Selecttimebutton1_click4(GButton source, GEvent event) { //_CODE_:Selecttimebutton1:909384:
//  println("Selecttimebutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
ChangeAnalysisTime();
} //_CODE_:Selecttimebutton1:909384:

public void COLORIMETERbutton1_click4(GButton source, GEvent event) { //_CODE_:COLORIMETERbutton1:303350:
//  println("COLORIMETERbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
if (Colorimeterwindow == false)
ColorimeterWin();
else if (Colorimeterwindow == true){
window5.close();
Colorimeterwindow = false;
}
} //_CODE_:COLORIMETERbutton1:303350:

synchronized public void win_draw3(GWinApplet appc, GWinData data) { //_CODE_:window3:390555:
  appc.background(230);
} //_CODE_:window3:390555:

public void LiftUP_click1(GButton source, GEvent event) { //_CODE_:Lift_UP:307497:
 // println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
 LiftUp();
} //_CODE_:Lift_UP:307497:

public void LiftDown_click1(GButton source, GEvent event) { //_CODE_:Lift_DOWN:335070:
  //println("Lift_DOWN - GButton event occured " + System.currentTimeMillis()%10000000 );
  LiftDown();
} //_CODE_:Lift_DOWN:335070:

public void HVbutton1_click1(GButton source, GEvent event) { //_CODE_:HVbutton1:344984:
  //println("HVbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  HVON();
} //_CODE_:HVbutton1:344984:

public void HVOFFbutton1_click1(GButton source, GEvent event) { //_CODE_:HVOFFbutton1:924694:
  //println("HVOFFbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  HVOFF();
} //_CODE_:HVOFFbutton1:924694:

public void InjectiondropList1_click1(GDropList source, GEvent event) { //_CODE_:InjectiondropList1:296461:
//  println(InjectiondropList1.getSelectedIndex());
//  println(InjectiondropList1.getSelectedText());
 // println("InjectiondropList1 - GDropList event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:InjectiondropList1:296461:

public void Injectbutton1_click2(GButton source, GEvent event) { //_CODE_:Injectbutton1:429946:
  //println("Injectbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  setInjection();
//  injectionSatrted = true;
//  selectInjectionTime();
//  injectionTimingTOT = 320.0;
//  injectpresenttime = millis();
} //_CODE_:Injectbutton1:429946:

public void VialClockbutton1_click4(GButton source, GEvent event) { //_CODE_:VialClockbutton1:831412:
//  println("VialClockbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
carouselClockwise();
} //_CODE_:VialClockbutton1:831412:

public void VialCounterbutton2_click1(GButton source, GEvent event) { //_CODE_:VialCounterbutton2:772136:
//  println("VialCounterbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
carouselCounterClockwise();
} //_CODE_:VialCounterbutton2:772136:

public void AdjustClockbutton1_click4(GButton source, GEvent event) { //_CODE_:AdjustClockbutton1:438671:
//  println("AdjustClockbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
adjustClockwise();
} //_CODE_:AdjustClockbutton1:438671:

public void AdjustCounterbutton2_click1(GButton source, GEvent event) { //_CODE_:AdjustCounterbutton2:485420:
//  println("button2 - GButton event occured " + System.currentTimeMillis()%10000000 );
adjustCounterClockwise();
} //_CODE_:AdjustCounterbutton2:485420:

public void StaretAnalysisbutton1_click4(GButton source, GEvent event) { //_CODE_:StartAnalysisbutton1:528062:
//  println("StartAnalysisbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
startAnalysis();
  currentAnalysisNumber = 1;
  currentAnalysisRepetition = 1;
} //_CODE_:StartAnalysisbutton1:528062:

public void StopAnalysisbutton2_click1(GButton source, GEvent event) { //_CODE_:StopAnalysisbutton2:483616:
//  println("StopAnalysisbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
stopAnalysis();
 if(startflag == true){
     startflag = false;
     closeDAQFile();
 } 
} //_CODE_:StopAnalysisbutton2:483616:

public void ElectrokineticInbutton1_click4(GButton source, GEvent event) { //_CODE_:ElectrokineticInjbutton1:546233:
//  println("ElectrokineticInjbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setElectrokineticInjection();
} //_CODE_:ElectrokineticInjbutton1:546233:

public void Hydrodynamicbutton2_click1(GButton source, GEvent event) { //_CODE_:Hydrodynamicbutton2:857095:
//  println("Hydrodynamicbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
setHydrodynamicInjection();
} //_CODE_:Hydrodynamicbutton2:857095:

public void PONbutton1_click4(GButton source, GEvent event) { //_CODE_:PONbutton1:839225:
//  println("PONbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setPumpON();
} //_CODE_:PONbutton1:839225:

public void POFFbutton1_click4(GButton source, GEvent event) { //_CODE_:POFFbutton1:500656:
//  println("POFFbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setPumpOFF();
} //_CODE_:POFFbutton1:500656:

public void VONbutton1_click4(GButton source, GEvent event) { //_CODE_:VONbutton1:990027:
//  println("VONbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
 setValvesON();
} //_CODE_:VONbutton1:990027:

public void VOFFbutton1_click4(GButton source, GEvent event) { //_CODE_:VOFFbutton1:344415:
//  println("VOFFbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setValvesOFF();
} //_CODE_:VOFFbutton1:344415:

public void startSamplingbutton1_click4(GButton source, GEvent event) { //_CODE_:startSamplingbutton1:740497:
//  println("startSamplingbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setSamplingON();
} //_CODE_:startSamplingbutton1:740497:

public void stopSamplingbutton1_click4(GButton source, GEvent event) { //_CODE_:stopSamplingbutton1:969761:
//  println("stopSamplingbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setSamplingOFF();
} //_CODE_:stopSamplingbutton1:969761:

public void injectbutton1_click4(GButton source, GEvent event) { //_CODE_:injectbutton1:869616:
//  println("injectbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
performInjection();
} //_CODE_:injectbutton1:869616:

public void CollectiondropList1_click1(GDropList source, GEvent event) { //_CODE_:CollectiondropList1:272968:
  println("CollectiondropList1 - GDropList event occured " + System.currentTimeMillis()%10000000 );
  println(CollectiondropList1.getSelectedIndex());
} //_CODE_:CollectiondropList1:272968:

public void setCollectionbutton1_click4(GButton source, GEvent event) { //_CODE_:setCollectionbutton1:306504:
//  println("setCollectionbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setCollection();
} //_CODE_:setCollectionbutton1:306504:

public void ElectroCondONbutton1_click4(GButton source, GEvent event) { //_CODE_:ElectroCondONbutton1:257761:
//  println("ElectroCondONbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setOnElectroConditioning();
} //_CODE_:ElectroCondONbutton1:257761:

public void ElectroCondOFFbutton2_click1(GButton source, GEvent event) { //_CODE_:bElectroCondOFFutton2:722340:
//  println("bElectroCondOFFutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
setOffElectroConditioning();
} //_CODE_:bElectroCondOFFutton2:722340:

public void pFlushOnbutton1_click4(GButton source, GEvent event) { //_CODE_:pFlushOnbutton1:657319:
//  println("pFlushOnbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setOnPressureFlush();
} //_CODE_:pFlushOnbutton1:657319:

public void pFlushOffbutton1_click4(GButton source, GEvent event) { //_CODE_:pFlushOffbutton1:363217:
//  println("pFlushOffbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setOffPressureFlush();
} //_CODE_:pFlushOffbutton1:363217:

public void vFlushOnbutton1_click4(GButton source, GEvent event) { //_CODE_:vFlushOnbutton1:791362:
//  println("vFlushOnbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setOnVacuumFlush();
} //_CODE_:vFlushOnbutton1:791362:

public void vFlushOffbutton1_click4(GButton source, GEvent event) { //_CODE_:vFlushOffbutton1:971901:
//  println("vFlushOffbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setOffVacuumFlush();
} //_CODE_:vFlushOffbutton1:971901:

public void anTimedropList1_click1(GDropList source, GEvent event) { //_CODE_:anTimedropList1:630116:
//  println("anTimedropList1 - GDropList event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:anTimedropList1:630116:

public void setAnTimebutton1_click4(GButton source, GEvent event) { //_CODE_:setAnTimebutton1:705114:
//  println("setAnTimebutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setAnalysisTime();
} //_CODE_:setAnTimebutton1:705114:

synchronized public void CDCwin_draw1(GWinApplet appc, GWinData data) { //_CODE_:window4:234483:
  appc.background(230);
} //_CODE_:window4:234483:

public void button9Hz_click(GButton source, GEvent event) { //_CODE_:button9Hz:938928:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz9();
} //_CODE_:button9Hz:938928:

public void button26Hz_click1(GButton source, GEvent event) { //_CODE_:button26Hz:356445:
//  println("button2 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz26();
} //_CODE_:button26Hz:356445:

public void button91Hz_click1(GButton source, GEvent event) { //_CODE_:button91Hz:370334:
//  println("button3 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz91();
} //_CODE_:button91Hz:370334:

public void button11Hz_click1(GButton source, GEvent event) { //_CODE_:button11Hz:293365:
//  println("button4 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz11();
} //_CODE_:button11Hz:293365:

public void button13Hz_click2(GButton source, GEvent event) { //_CODE_:button13Hz:986125:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz13();
} //_CODE_:button13Hz:986125:

public void button16Hz_click2(GButton source, GEvent event) { //_CODE_:button16Hz:782219:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz16();
} //_CODE_:button16Hz:782219:

public void button50Hz_click2(GButton source, GEvent event) { //_CODE_:button50Hz:636183:
//  println("button50Hz - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz50();
} //_CODE_:button50Hz:636183:

public void button84Hz_click2(GButton source, GEvent event) { //_CODE_:button84Hz:820197:
//  println("button84Hz - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz84();
} //_CODE_:button84Hz:820197:

public void button16kHz_click2(GButton source, GEvent event) { //_CODE_:button16kHz:508751:
//  println("button16kHz - GButton event occured " + System.currentTimeMillis()%10000000 );
Excitation16kHz();
} //_CODE_:button16kHz:508751:

public void button32kHz_click2(GButton source, GEvent event) { //_CODE_:button32kHz:267911:
//  println("button32kHz - GButton event occured " + System.currentTimeMillis()%10000000 );
Excitation32kHz();
} //_CODE_:button32kHz:267911:

public void buttonVdd8_click2(GButton source, GEvent event) { //_CODE_:buttonVdd8:960809:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Vdd8();
} //_CODE_:buttonVdd8:960809:

public void buttonVdd4_click2(GButton source, GEvent event) { //_CODE_:buttonVdd4:500688:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Vdd4();
} //_CODE_:buttonVdd4:500688:

public void buttonVddx38_click2(GButton source, GEvent event) { //_CODE_:buttonVddx38:944936:
//  println("buttonVddx38 - GButton event occured " + System.currentTimeMillis()%10000000 );
Vddx38();
} //_CODE_:buttonVddx38:944936:

public void buttonVdd2_click2(GButton source, GEvent event) { //_CODE_:buttonVdd2:629822:
//  println("buttonVdd2 - GButton event occured " + System.currentTimeMillis()%10000000 );
Vdd2();
} //_CODE_:buttonVdd2:629822:

public void buttonCapChop_click2(GButton source, GEvent event) { //_CODE_:buttonCapChop:961003:
//  println("buttonCapChop - GButton event occured " + System.currentTimeMillis()%10000000 );
CapChop();
} //_CODE_:buttonCapChop:961003:

public void button1_click2(GButton source, GEvent event) { //_CODE_:buttonNormal:944383:
//  println("buttonNormal - GButton event occured " + System.currentTimeMillis()%10000000 );
NormalOperation();
} //_CODE_:buttonNormal:944383:

public void button1_click3(GButton source, GEvent event) { //_CODE_:buttonVT_ON:795627:
//  println("buttonVT_ON - GButton event occured " + System.currentTimeMillis()%10000000 );
//if (TempCompensation == false){
VT_on();
TempCompensation = true;
//}
} //_CODE_:buttonVT_ON:795627:

public void buttonVT_OFF_click3(GButton source, GEvent event) { //_CODE_:buttonVT_OFF:874859:
//  println("buttonVT_OFF - GButton event occured " + System.currentTimeMillis()%10000000 );
//if (TempCompensation == true){
VT_off();
TempCompensation = false;
//}
} //_CODE_:buttonVT_OFF:874859:

synchronized public void Colorimeterwin_draw1(GWinApplet appc, GWinData data) { //_CODE_:window5:632533:
  appc.background(230);
} //_CODE_:window5:632533:

public void REDbutton1_click4(GButton source, GEvent event) { //_CODE_:REDbutton1:498322:
//  println("REDbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Red_Led();
} //_CODE_:REDbutton1:498322:

public void GREENbutton2_click1(GButton source, GEvent event) { //_CODE_:GREENbutton2:876667:
//  println("GREENbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
Green_Led();
} //_CODE_:GREENbutton2:876667:

public void BLUEbutton3_click1(GButton source, GEvent event) { //_CODE_:BLUEbutton3:723190:
//  println("BLUEbutton3 - GButton event occured " + System.currentTimeMillis()%10000000 );
Blue_Led();
} //_CODE_:BLUEbutton3:723190:

public void ZERObutton1_click4(GButton source, GEvent event) { //_CODE_:ZERObutton1:303635:
//  println("ZERObutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Zero_color();
} //_CODE_:ZERObutton1:303635:

public void Clearbutton1_click4(GButton source, GEvent event) { //_CODE_:Clearbutton1:609341:
//  println("Clearbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Clear_Filter();
} //_CODE_:Clearbutton1:609341:



// Create all the GUI controls. 
// autogenerated do not edit
public void createGUI(){
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
  G4P.setCursor(ARROW);
  if(frame != null)
    frame.setTitle("Universal Acquisition 0.2");
  window1 = new GWindow(this, "Serial Port Setup", 300, 0, 520, 265, false, JAVA2D);
  window1.setActionOnClose(G4P.CLOSE_WINDOW);
  window1.addDrawHandler(this, "Setupwin_draw1");
  OpenSerialbutton = new GButton(window1.papplet, 6, 6, 80, 30);
  OpenSerialbutton.setText("Open Serial Port1");
  OpenSerialbutton.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OpenSerialbutton.addEventHandler(this, "OpenSerialbutton1_click1");
  Serial_Ports = new GDropList(window1.papplet, 6, 42, 165, 220, 10);
  Serial_Ports.setItems(loadStrings("list_605734"), 0);
  Serial_Ports.addEventHandler(this, "Serial_PortsdropList1_click1");
  StopSerial = new GButton(window1.papplet, 90, 6, 80, 30);
  StopSerial.setText("STOP Serial Port");
  StopSerial.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  StopSerial.addEventHandler(this, "StopSerialbutton1_click3");
  BaudRate = new GDropList(window1.papplet, 174, 42, 165, 220, 10);
  BaudRate.setItems(loadStrings("list_868333"), 0);
  BaudRate.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  BaudRate.addEventHandler(this, "BaudRate_click1");
  Baudlabel1 = new GLabel(window1.papplet, 174, 6, 165, 30);
  Baudlabel1.setText("Baud Rate");
  Baudlabel1.setTextBold();
  Baudlabel1.setOpaque(false);
  Phrasetextfield1 = new GTextField(window1.papplet, 342, 42, 165, 22, G4P.SCROLLBARS_NONE);
  Phrasetextfield1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  Phrasetextfield1.setOpaque(false);
  Phrasetextfield1.addEventHandler(this, "Phrasetextfield1_change1");
  Initiationlabel1 = new GLabel(window1.papplet, 342, 6, 120, 30);
  Initiationlabel1.setText("Start Phrase");
  Initiationlabel1.setTextBold();
  Initiationlabel1.setOpaque(false);
  OKPhrasebutton1 = new GButton(window1.papplet, 468, 6, 40, 30);
  OKPhrasebutton1.setText("OK");
  OKPhrasebutton1.setTextBold();
  OKPhrasebutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  OKPhrasebutton1.addEventHandler(this, "OKPhrasebutton1_click2");
  window2 = new GWindow(this, "Controls", 0, 0, 375, 170, false, JAVA2D);
  window2.addDrawHandler(this, "win_draw2");
  ZoomIn = new GButton(window2.papplet, 5, 5, 80, 30);
  ZoomIn.setText("Zoom in");
  ZoomIn.setTextBold();
  ZoomIn.addEventHandler(this, "ZoomInbutton1_click1");
  ZoomOut = new GButton(window2.papplet, 5, 40, 80, 30);
  ZoomOut.setText("Zoom out");
  ZoomOut.setTextBold();
  ZoomOut.addEventHandler(this, "ZoomOutbutton2_click1");
  TimePlus = new GButton(window2.papplet, 90, 5, 80, 30);
  TimePlus.setText("Time +");
  TimePlus.setTextBold();
  TimePlus.addEventHandler(this, "TimePlusbutton3_click1");
  TimeMinus = new GButton(window2.papplet, 90, 40, 80, 30);
  TimeMinus.setText("Time -");
  TimeMinus.setTextBold();
  TimeMinus.addEventHandler(this, "TimeMinusbutton4_click1");
  RUN = new GButton(window2.papplet, 180, 5, 39, 30);
  RUN.setText("RUN");
  RUN.setTextBold();
  RUN.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  RUN.addEventHandler(this, "RUNbutton5_click1");
  STOP = new GButton(window2.papplet, 180, 40, 39, 30);
  STOP.setText("STOP");
  STOP.setLocalColorScheme(GCScheme.RED_SCHEME);
  STOP.addEventHandler(this, "STOPbutton6_click1");
  OffsetUp = new GButton(window2.papplet, 5, 75, 60, 30);
  OffsetUp.setText("Offset up");
  OffsetUp.setTextBold();
  OffsetUp.addEventHandler(this, "button1_click1");
  OffsetDown = new GButton(window2.papplet, 90, 75, 60, 30);
  OffsetDown.setText("Offset down");
  OffsetDown.setTextBold();
  OffsetDown.addEventHandler(this, "OffsetDownbutton1_click1");
  Timingslider1 = new GSlider(window2.papplet, 0, 145, 265, 15, 10.0);
  Timingslider1.setLimits(0.0, 0.0, 4000.0);
  Timingslider1.setNumberFormat(G4P.DECIMAL, 1);
  Timingslider1.setOpaque(false);
  Timingslider1.addEventHandler(this, "Timingslider1_change1");
  Offsetslider2 = new GSlider(window2.papplet, 279, 0, 160, 15, 10.0);
  Offsetslider2.setRotation(PI/2, GControlMode.CORNER);
  Offsetslider2.setLimits(0.0, -2000.0, 2000.0);
  Offsetslider2.setNumberFormat(G4P.DECIMAL, 2);
  Offsetslider2.setOpaque(false);
  Offsetslider2.addEventHandler(this, "Offsetslider2_change1");
  Settingsbutton1 = new GButton(window2.papplet, 180, 75, 80, 30);
  Settingsbutton1.setText("Settings");
  Settingsbutton1.setTextBold();
  Settingsbutton1.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  Settingsbutton1.addEventHandler(this, "Settingsbutton1_click2");
  EXITbutton1 = new GButton(window2.papplet, 180, 110, 80, 30);
  EXITbutton1.setText("EXIT");
  EXITbutton1.setTextBold();
  EXITbutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  EXITbutton1.addEventHandler(this, "EXITbutton1_click2");
  ChoosePathbutton1 = new GButton(window2.papplet, 282, 132, 80, 30);
  ChoosePathbutton1.setText("Choose Path");
  ChoosePathbutton1.setTextBold();
  ChoosePathbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ChoosePathbutton1.addEventHandler(this, "ChoosePathbutton1_click2");
  AnalysisUPbutton1 = new GButton(window2.papplet, 5, 110, 15, 15);
  AnalysisUPbutton1.setText("↑");
  AnalysisUPbutton1.setTextBold();
  AnalysisUPbutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  AnalysisUPbutton1.addEventHandler(this, "AnalysisUP_click2");
  AnalysisDownbutton1 = new GButton(window2.papplet, 47, 110, 15, 15);
  AnalysisDownbutton1.setText("↓");
  AnalysisDownbutton1.setTextBold();
  AnalysisDownbutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  AnalysisDownbutton1.addEventHandler(this, "AnalysisDown_click2");
  Analysisslider1 = new GSlider(window2.papplet, 0, 128, 89, 15, 10.0);
  Analysisslider1.setLimits(0.0, 0.0, 100.0);
  Analysisslider1.setNumberFormat(G4P.DECIMAL, 0);
  Analysisslider1.setOpaque(false);
  Analysisslider1.addEventHandler(this, "Analysisslider1_change1");
  buttonCEControls = new GButton(window2.papplet, 282, 24, 80, 20);
  buttonCEControls.setText("CE Controls");
  buttonCEControls.setTextBold();
  buttonCEControls.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonCEControls.addEventHandler(this, "buttonCEControls_click3");
  buttonCDCControls = new GButton(window2.papplet, 282, 48, 80, 20);
  buttonCDCControls.setText("CDC Controls");
  buttonCDCControls.setTextBold();
  buttonCDCControls.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonCDCControls.addEventHandler(this, "buttonCDCControls_click1");
  buttonAutoZero = new GButton(window2.papplet, 282, 72, 80, 20);
  buttonAutoZero.setText("Auto Zero");
  buttonAutoZero.setTextBold();
  buttonAutoZero.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  buttonAutoZero.addEventHandler(this, "buttonAutoZero_click3");
  buttonAutoZeroDel = new GButton(window2.papplet, 282, 96, 80, 30);
  buttonAutoZeroDel.setText("Auto Zero Delete");
  buttonAutoZeroDel.setTextBold();
  buttonAutoZeroDel.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  buttonAutoZeroDel.addEventHandler(this, "buttonAutoZeroDel_click3");
  ZeroTimebutton1 = new GButton(window2.papplet, 221, 40, 39, 30);
  ZeroTimebutton1.setText("Zero Time");
  ZeroTimebutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  ZeroTimebutton1.addEventHandler(this, "ZeroTimebutton1_click4");
  AutoRun_button1 = new GButton(window2.papplet, 221, 5, 39, 30);
  AutoRun_button1.setText("Auto Run");
  AutoRun_button1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  AutoRun_button1.addEventHandler(this, "AutoRun_button1_click4");
  OffUpSmall_button1 = new GButton(window2.papplet, 70, 75, 15, 30);
  OffUpSmall_button1.setText("↑");
  OffUpSmall_button1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OffUpSmall_button1.addEventHandler(this, "OffUpSmall_button1_click4");
  OffDownSmall_button1 = new GButton(window2.papplet, 155, 75, 15, 30);
  OffDownSmall_button1.setText("↓");
  OffDownSmall_button1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OffDownSmall_button1.addEventHandler(this, "OffDownSmall_button1_click5");
  ATimePlusTenbutton1 = new GButton(window2.papplet, 21, 110, 24, 15);
  ATimePlusTenbutton1.setText("10↑");
  ATimePlusTenbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ATimePlusTenbutton1.addEventHandler(this, "ATimePlusTenbutton1_click4");
  ATimeminusTenbutton1 = new GButton(window2.papplet, 63, 110, 24, 15);
  ATimeminusTenbutton1.setText("10↓");
  ATimeminusTenbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ATimeminusTenbutton1.addEventHandler(this, "ATimeminusTenbutton1_click4");
  TimeAnalysistextfield1 = new GTextField(window2.papplet, 90, 110, 50, 30, G4P.SCROLLBARS_NONE);
  TimeAnalysistextfield1.setOpaque(true);
  TimeAnalysistextfield1.addEventHandler(this, "textfield1_change1");
  Selecttimebutton1 = new GButton(window2.papplet, 140, 110, 30, 30);
  Selecttimebutton1.setText("OK");
  Selecttimebutton1.setTextBold();
  Selecttimebutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  Selecttimebutton1.addEventHandler(this, "Selecttimebutton1_click4");
  COLORIMETERbutton1 = new GButton(window2.papplet, 282, 0, 80, 20);
  COLORIMETERbutton1.setText("Colorimeter");
  COLORIMETERbutton1.setTextBold();
  COLORIMETERbutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  COLORIMETERbutton1.addEventHandler(this, "COLORIMETERbutton1_click4");
  window3 = new GWindow(this, "CE Controls", 0, 200, 460, 300, false, JAVA2D);
  window3.setActionOnClose(G4P.CLOSE_WINDOW);
  window3.addDrawHandler(this, "win_draw3");
  Lift_UP = new GButton(window3.papplet, 0, 20, 80, 30);
  Lift_UP.setText("Lift Up");
  Lift_UP.setTextBold();
  Lift_UP.addEventHandler(this, "LiftUP_click1");
  Lift_DOWN = new GButton(window3.papplet, 0, 60, 80, 30);
  Lift_DOWN.setText("Lift Down");
  Lift_DOWN.setTextBold();
  Lift_DOWN.addEventHandler(this, "LiftDown_click1");
  HVbutton1 = new GButton(window3.papplet, 90, 20, 80, 30);
  HVbutton1.setText("HV ON");
  HVbutton1.setTextBold();
  HVbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  HVbutton1.addEventHandler(this, "HVbutton1_click1");
  HVOFFbutton1 = new GButton(window3.papplet, 90, 60, 80, 30);
  HVOFFbutton1.setText("HV OFF");
  HVOFFbutton1.setTextBold();
  HVOFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  HVOFFbutton1.addEventHandler(this, "HVOFFbutton1_click1");
  Injectionlabel1 = new GLabel(window3.papplet, 172, 0, 80, 20);
  Injectionlabel1.setText("Injection");
  Injectionlabel1.setTextBold();
  Injectionlabel1.setOpaque(false);
  InjectiondropList1 = new GDropList(window3.papplet, 180, 20, 90, 200, 10);
  InjectiondropList1.setItems(loadStrings("list_296461"), 0);
  InjectiondropList1.addEventHandler(this, "InjectiondropList1_click1");
  Injectbutton1 = new GButton(window3.papplet, 180, 60, 80, 30);
  Injectbutton1.setText("Set Injection");
  Injectbutton1.setTextBold();
  Injectbutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  Injectbutton1.addEventHandler(this, "Injectbutton1_click2");
  Lifterlabel1 = new GLabel(window3.papplet, 0, 0, 80, 20);
  Lifterlabel1.setText("Lifter");
  Lifterlabel1.setTextBold();
  Lifterlabel1.setOpaque(false);
  Voltagelabel1 = new GLabel(window3.papplet, 90, 0, 80, 20);
  Voltagelabel1.setText("Voltage");
  Voltagelabel1.setTextBold();
  Voltagelabel1.setOpaque(false);
  Carousellabel1 = new GLabel(window3.papplet, 0, 100, 80, 20);
  Carousellabel1.setText("Carousel");
  Carousellabel1.setTextBold();
  Carousellabel1.setOpaque(false);
  VialClockbutton1 = new GButton(window3.papplet, 0, 120, 80, 30);
  VialClockbutton1.setText("Vial →");
  VialClockbutton1.addEventHandler(this, "VialClockbutton1_click4");
  VialCounterbutton2 = new GButton(window3.papplet, 0, 160, 80, 30);
  VialCounterbutton2.setText("Vial ←");
  VialCounterbutton2.addEventHandler(this, "VialCounterbutton2_click1");
  AdjustClockbutton1 = new GButton(window3.papplet, 90, 120, 80, 30);
  AdjustClockbutton1.setText("Adjust →");
  AdjustClockbutton1.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  AdjustClockbutton1.addEventHandler(this, "AdjustClockbutton1_click4");
  AdjustCounterbutton2 = new GButton(window3.papplet, 90, 160, 80, 30);
  AdjustCounterbutton2.setText("Adjust ←");
  AdjustCounterbutton2.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  AdjustCounterbutton2.addEventHandler(this, "AdjustCounterbutton2_click1");
  StartAnalysisbutton1 = new GButton(window3.papplet, 180, 120, 80, 30);
  StartAnalysisbutton1.setText("START ANALYSIS");
  StartAnalysisbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  StartAnalysisbutton1.addEventHandler(this, "StaretAnalysisbutton1_click4");
  StopAnalysisbutton2 = new GButton(window3.papplet, 180, 160, 80, 30);
  StopAnalysisbutton2.setText("STOP ANALYSIS");
  StopAnalysisbutton2.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  StopAnalysisbutton2.addEventHandler(this, "StopAnalysisbutton2_click1");
  InjectMethodlabel1 = new GLabel(window3.papplet, 0, 190, 170, 20);
  InjectMethodlabel1.setText("Injection Method");
  InjectMethodlabel1.setTextBold();
  InjectMethodlabel1.setOpaque(false);
  ElectrokineticInjbutton1 = new GButton(window3.papplet, 0, 210, 80, 30);
  ElectrokineticInjbutton1.setText("Electrokinetic");
  ElectrokineticInjbutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  ElectrokineticInjbutton1.addEventHandler(this, "ElectrokineticInbutton1_click4");
  Hydrodynamicbutton2 = new GButton(window3.papplet, 90, 210, 80, 30);
  Hydrodynamicbutton2.setText("Hydrodynamic");
  Hydrodynamicbutton2.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  Hydrodynamicbutton2.addEventHandler(this, "Hydrodynamicbutton2_click1");
  Pump_label1 = new GLabel(window3.papplet, 0, 240, 80, 20);
  Pump_label1.setText("Pump");
  Pump_label1.setTextBold();
  Pump_label1.setOpaque(false);
  PONbutton1 = new GButton(window3.papplet, 0, 260, 40, 30);
  PONbutton1.setText("ON");
  PONbutton1.setTextBold();
  PONbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  PONbutton1.addEventHandler(this, "PONbutton1_click4");
  POFFbutton1 = new GButton(window3.papplet, 50, 260, 40, 30);
  POFFbutton1.setText("OFF");
  POFFbutton1.setTextBold();
  POFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  POFFbutton1.addEventHandler(this, "POFFbutton1_click4");
  Valveslabel1 = new GLabel(window3.papplet, 100, 240, 80, 20);
  Valveslabel1.setText("Valves");
  Valveslabel1.setTextBold();
  Valveslabel1.setOpaque(false);
  VONbutton1 = new GButton(window3.papplet, 100, 260, 40, 30);
  VONbutton1.setText("ON");
  VONbutton1.setTextBold();
  VONbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  VONbutton1.addEventHandler(this, "VONbutton1_click4");
  VOFFbutton1 = new GButton(window3.papplet, 150, 260, 40, 30);
  VOFFbutton1.setText("OFF");
  VOFFbutton1.setTextBold();
  VOFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  VOFFbutton1.addEventHandler(this, "VOFFbutton1_click4");
  samplingLabel1 = new GLabel(window3.papplet, 200, 240, 80, 20);
  samplingLabel1.setText("Sampling");
  samplingLabel1.setTextBold();
  samplingLabel1.setOpaque(false);
  startSamplingbutton1 = new GButton(window3.papplet, 200, 260, 40, 30);
  startSamplingbutton1.setText("ON");
  startSamplingbutton1.setTextBold();
  startSamplingbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  startSamplingbutton1.addEventHandler(this, "startSamplingbutton1_click4");
  stopSamplingbutton1 = new GButton(window3.papplet, 250, 260, 40, 30);
  stopSamplingbutton1.setText("OFF");
  stopSamplingbutton1.setTextBold();
  stopSamplingbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  stopSamplingbutton1.addEventHandler(this, "stopSamplingbutton1_click4");
  injectbutton1 = new GButton(window3.papplet, 180, 210, 80, 30);
  injectbutton1.setText("Inject");
  injectbutton1.setTextBold();
  injectbutton1.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  injectbutton1.addEventHandler(this, "injectbutton1_click4");
  Collectionlabel1 = new GLabel(window3.papplet, 280, 0, 80, 20);
  Collectionlabel1.setText("Collection");
  Collectionlabel1.setOpaque(false);
  CollectiondropList1 = new GDropList(window3.papplet, 280, 20, 90, 110, 5);
  CollectiondropList1.setItems(loadStrings("list_272968"), 0);
  CollectiondropList1.addEventHandler(this, "CollectiondropList1_click1");
  setCollectionbutton1 = new GButton(window3.papplet, 280, 60, 80, 30);
  setCollectionbutton1.setText("Set Collection");
  setCollectionbutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  setCollectionbutton1.addEventHandler(this, "setCollectionbutton1_click4");
  ElectroCondlabel1 = new GLabel(window3.papplet, 280, 90, 80, 30);
  ElectroCondlabel1.setText("Electro Conditining");
  ElectroCondlabel1.setOpaque(false);
  ElectroCondONbutton1 = new GButton(window3.papplet, 280, 120, 40, 20);
  ElectroCondONbutton1.setText("ON");
  ElectroCondONbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ElectroCondONbutton1.addEventHandler(this, "ElectroCondONbutton1_click4");
  bElectroCondOFFutton2 = new GButton(window3.papplet, 330, 120, 40, 20);
  bElectroCondOFFutton2.setText("OFF");
  bElectroCondOFFutton2.setLocalColorScheme(GCScheme.RED_SCHEME);
  bElectroCondOFFutton2.addEventHandler(this, "ElectroCondOFFbutton2_click1");
  PressFlushlabel1 = new GLabel(window3.papplet, 280, 140, 80, 20);
  PressFlushlabel1.setText("P Flush");
  PressFlushlabel1.setOpaque(false);
  pFlushOnbutton1 = new GButton(window3.papplet, 280, 160, 40, 20);
  pFlushOnbutton1.setText("ON");
  pFlushOnbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  pFlushOnbutton1.addEventHandler(this, "pFlushOnbutton1_click4");
  pFlushOffbutton1 = new GButton(window3.papplet, 330, 160, 40, 20);
  pFlushOffbutton1.setText("OFF");
  pFlushOffbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  pFlushOffbutton1.addEventHandler(this, "pFlushOffbutton1_click4");
  vFlushlabel1 = new GLabel(window3.papplet, 280, 180, 80, 20);
  vFlushlabel1.setText("V Flush");
  vFlushlabel1.setOpaque(false);
  vFlushOnbutton1 = new GButton(window3.papplet, 280, 200, 40, 20);
  vFlushOnbutton1.setText("ON");
  vFlushOnbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  vFlushOnbutton1.addEventHandler(this, "vFlushOnbutton1_click4");
  vFlushOffbutton1 = new GButton(window3.papplet, 330, 200, 40, 20);
  vFlushOffbutton1.setText("OFF");
  vFlushOffbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  vFlushOffbutton1.addEventHandler(this, "vFlushOffbutton1_click4");
  anTimelabel1 = new GLabel(window3.papplet, 380, 0, 80, 20);
  anTimelabel1.setText("AnalysisTime");
  anTimelabel1.setOpaque(false);
  anTimedropList1 = new GDropList(window3.papplet, 380, 20, 90, 88, 4);
  anTimedropList1.setItems(loadStrings("list_630116"), 0);
  anTimedropList1.addEventHandler(this, "anTimedropList1_click1");
  setAnTimebutton1 = new GButton(window3.papplet, 380, 60, 80, 30);
  setAnTimebutton1.setText("Set");
  setAnTimebutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  setAnTimebutton1.addEventHandler(this, "setAnTimebutton1_click4");
  window4 = new GWindow(this, "CDC Functions", 0, 350, 370, 160, false, JAVA2D);
  window4.setActionOnClose(G4P.CLOSE_WINDOW);
  window4.addDrawHandler(this, "CDCwin_draw1");
  button9Hz = new GButton(window4.papplet, 5, 5, 40, 30);
  button9Hz.setText("9Hz");
  button9Hz.setTextBold();
  button9Hz.addEventHandler(this, "button9Hz_click");
  button26Hz = new GButton(window4.papplet, 185, 5, 40, 30);
  button26Hz.setText("26Hz");
  button26Hz.setTextBold();
  button26Hz.addEventHandler(this, "button26Hz_click1");
  button91Hz = new GButton(window4.papplet, 320, 5, 40, 30);
  button91Hz.setText("91Hz");
  button91Hz.setTextBold();
  button91Hz.addEventHandler(this, "button91Hz_click1");
  button11Hz = new GButton(window4.papplet, 50, 5, 40, 30);
  button11Hz.setText("11Hz");
  button11Hz.setTextBold();
  button11Hz.addEventHandler(this, "button11Hz_click1");
  button13Hz = new GButton(window4.papplet, 95, 5, 40, 30);
  button13Hz.setText("13Hz");
  button13Hz.setTextBold();
  button13Hz.addEventHandler(this, "button13Hz_click2");
  button16Hz = new GButton(window4.papplet, 140, 5, 40, 30);
  button16Hz.setText("16Hz");
  button16Hz.setTextBold();
  button16Hz.addEventHandler(this, "button16Hz_click2");
  button50Hz = new GButton(window4.papplet, 230, 5, 40, 30);
  button50Hz.setText("50Hz");
  button50Hz.setTextBold();
  button50Hz.addEventHandler(this, "button50Hz_click2");
  button84Hz = new GButton(window4.papplet, 275, 5, 40, 30);
  button84Hz.setText("84Hz");
  button84Hz.setTextBold();
  button84Hz.addEventHandler(this, "button84Hz_click2");
  button16kHz = new GButton(window4.papplet, 5, 40, 80, 30);
  button16kHz.setText("16kHz");
  button16kHz.setTextBold();
  button16kHz.setLocalColorScheme(GCScheme.RED_SCHEME);
  button16kHz.addEventHandler(this, "button16kHz_click2");
  button32kHz = new GButton(window4.papplet, 90, 40, 80, 30);
  button32kHz.setText("32kHz");
  button32kHz.setTextBold();
  button32kHz.setLocalColorScheme(GCScheme.RED_SCHEME);
  button32kHz.addEventHandler(this, "button32kHz_click2");
  buttonVdd8 = new GButton(window4.papplet, 5, 75, 50, 30);
  buttonVdd8.setText("Vdd/8");
  buttonVdd8.setTextBold();
  buttonVdd8.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVdd8.addEventHandler(this, "buttonVdd8_click2");
  buttonVdd4 = new GButton(window4.papplet, 60, 75, 50, 30);
  buttonVdd4.setText("Vdd/4");
  buttonVdd4.setTextBold();
  buttonVdd4.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVdd4.addEventHandler(this, "buttonVdd4_click2");
  buttonVddx38 = new GButton(window4.papplet, 115, 75, 50, 30);
  buttonVddx38.setText("Vddx3/8");
  buttonVddx38.setTextBold();
  buttonVddx38.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVddx38.addEventHandler(this, "buttonVddx38_click2");
  buttonVdd2 = new GButton(window4.papplet, 170, 75, 50, 30);
  buttonVdd2.setText("Vdd/2");
  buttonVdd2.setTextBold();
  buttonVdd2.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVdd2.addEventHandler(this, "buttonVdd2_click2");
  buttonCapChop = new GButton(window4.papplet, 175, 40, 80, 30);
  buttonCapChop.setText("CapChop");
  buttonCapChop.setTextBold();
  buttonCapChop.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  buttonCapChop.addEventHandler(this, "buttonCapChop_click2");
  buttonNormal = new GButton(window4.papplet, 260, 40, 80, 30);
  buttonNormal.setText("Normal");
  buttonNormal.setTextBold();
  buttonNormal.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  buttonNormal.addEventHandler(this, "button1_click2");
  buttonVT_ON = new GButton(window4.papplet, 5, 110, 50, 30);
  buttonVT_ON.setText("VT ON");
  buttonVT_ON.setTextBold();
  buttonVT_ON.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonVT_ON.addEventHandler(this, "button1_click3");
  buttonVT_OFF = new GButton(window4.papplet, 60, 110, 50, 30);
  buttonVT_OFF.setText("VT OFF");
  buttonVT_OFF.setTextBold();
  buttonVT_OFF.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonVT_OFF.addEventHandler(this, "buttonVT_OFF_click3");
  window5 = new GWindow(this, "Colorimeter", 300, 0, 185, 120, false, P2D);
  window5.setActionOnClose(G4P.CLOSE_WINDOW);
  window5.addDrawHandler(this, "Colorimeterwin_draw1");
  REDbutton1 = new GButton(window5.papplet, 6, 6, 80, 30);
  REDbutton1.setText("RED");
  REDbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  REDbutton1.addEventHandler(this, "REDbutton1_click4");
  GREENbutton2 = new GButton(window5.papplet, 6, 42, 80, 30);
  GREENbutton2.setText("GREEN");
  GREENbutton2.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  GREENbutton2.addEventHandler(this, "GREENbutton2_click1");
  BLUEbutton3 = new GButton(window5.papplet, 6, 78, 80, 30);
  BLUEbutton3.setText("BLUE");
  BLUEbutton3.addEventHandler(this, "BLUEbutton3_click1");
  ZERObutton1 = new GButton(window5.papplet, 96, 6, 80, 30);
  ZERObutton1.setText("ZERO");
  ZERObutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  ZERObutton1.addEventHandler(this, "ZERObutton1_click4");
  Clearbutton1 = new GButton(window5.papplet, 96, 42, 80, 30);
  Clearbutton1.setText("CLEAR");
  Clearbutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  Clearbutton1.addEventHandler(this, "Clearbutton1_click4");
}

// Variable declarations 
// autogenerated do not edit
GWindow window1;
GButton OpenSerialbutton; 
GDropList Serial_Ports; 
GButton StopSerial; 
GDropList BaudRate; 
GLabel Baudlabel1; 
GTextField Phrasetextfield1; 
GLabel Initiationlabel1; 
GButton OKPhrasebutton1; 
GWindow window2;
GButton ZoomIn; 
GButton ZoomOut; 
GButton TimePlus; 
GButton TimeMinus; 
GButton RUN; 
GButton STOP; 
GButton OffsetUp; 
GButton OffsetDown; 
GSlider Timingslider1; 
GSlider Offsetslider2; 
GButton Settingsbutton1; 
GButton EXITbutton1; 
GButton ChoosePathbutton1; 
GButton AnalysisUPbutton1; 
GButton AnalysisDownbutton1; 
GSlider Analysisslider1; 
GButton buttonCEControls; 
GButton buttonCDCControls; 
GButton buttonAutoZero; 
GButton buttonAutoZeroDel; 
GButton ZeroTimebutton1; 
GButton AutoRun_button1; 
GButton OffUpSmall_button1; 
GButton OffDownSmall_button1; 
GButton ATimePlusTenbutton1; 
GButton ATimeminusTenbutton1; 
GTextField TimeAnalysistextfield1; 
GButton Selecttimebutton1; 
GButton COLORIMETERbutton1; 
GWindow window3;
GButton Lift_UP; 
GButton Lift_DOWN; 
GButton HVbutton1; 
GButton HVOFFbutton1; 
GLabel Injectionlabel1; 
GDropList InjectiondropList1; 
GButton Injectbutton1; 
GLabel Lifterlabel1; 
GLabel Voltagelabel1; 
GLabel Carousellabel1; 
GButton VialClockbutton1; 
GButton VialCounterbutton2; 
GButton AdjustClockbutton1; 
GButton AdjustCounterbutton2; 
GButton StartAnalysisbutton1; 
GButton StopAnalysisbutton2; 
GLabel InjectMethodlabel1; 
GButton ElectrokineticInjbutton1; 
GButton Hydrodynamicbutton2; 
GLabel Pump_label1; 
GButton PONbutton1; 
GButton POFFbutton1; 
GLabel Valveslabel1; 
GButton VONbutton1; 
GButton VOFFbutton1; 
GLabel samplingLabel1; 
GButton startSamplingbutton1; 
GButton stopSamplingbutton1; 
GButton injectbutton1; 
GLabel Collectionlabel1; 
GDropList CollectiondropList1; 
GButton setCollectionbutton1; 
GLabel ElectroCondlabel1; 
GButton ElectroCondONbutton1; 
GButton bElectroCondOFFutton2; 
GLabel PressFlushlabel1; 
GButton pFlushOnbutton1; 
GButton pFlushOffbutton1; 
GLabel vFlushlabel1; 
GButton vFlushOnbutton1; 
GButton vFlushOffbutton1; 
GLabel anTimelabel1; 
GDropList anTimedropList1; 
GButton setAnTimebutton1; 
GWindow window4;
GButton button9Hz; 
GButton button26Hz; 
GButton button91Hz; 
GButton button11Hz; 
GButton button13Hz; 
GButton button16Hz; 
GButton button50Hz; 
GButton button84Hz; 
GButton button16kHz; 
GButton button32kHz; 
GButton buttonVdd8; 
GButton buttonVdd4; 
GButton buttonVddx38; 
GButton buttonVdd2; 
GButton buttonCapChop; 
GButton buttonNormal; 
GButton buttonVT_ON; 
GButton buttonVT_OFF; 
GWindow window5;
GButton REDbutton1; 
GButton GREENbutton2; 
GButton BLUEbutton3; 
GButton ZERObutton1; 
GButton Clearbutton1; 

