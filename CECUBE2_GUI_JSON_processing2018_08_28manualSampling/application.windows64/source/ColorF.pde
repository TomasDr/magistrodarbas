void Red_Led(){
  myPort.write("REDLED*");  
}

void Green_Led(){
  myPort.write("GREENLED*");
}

void Blue_Led(){
  myPort.write("BLUELED*");
}

void Zero_color(){
  myPort.write("ZeroAbsorbance*");
}

void Clear_Filter(){
  myPort.write("ClearF*");
}
