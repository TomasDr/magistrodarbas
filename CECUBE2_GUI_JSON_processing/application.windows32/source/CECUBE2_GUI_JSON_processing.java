import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import g4p_controls.*; 
import java.awt.event.*; 
import processing.serial.*; 
import cc.arduino.*; 
import ddf.minim.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class CECUBE2_GUI_JSON_processing extends PApplet {

// Need G4P library







Serial myPort; 
Serial myPort2;
Serial myPort3;
Serial myPort4;

Electropherogram myElectropherogram;
Axes2D myAxes2D;

Minim minim;
AudioPlayer player;
AudioInput input;

public void setup(){
  size(1000, 480, JAVA2D);
  initializeClasses();
  myAxes2D.run();
//  createGUI();
  customGUI();
  
    minim = new Minim(this);
  player = minim.loadFile("injectionsample.mp3");
  input = minim.getLineIn();
  
  // Place your setup code here
 ArrayInit(); 
 frame.setResizable(true);
 frame.addComponentListener(new ComponentAdapter() {
  public void componentResized(ComponentEvent e) {
    if(e.getSource()==frame) {
    redrawAxis = true;
    redraw();
    }
  }
});
  println(Serial.list());
    frameRate(15);
}

public void initializeClasses(){
  myElectropherogram = new Electropherogram(XstartPos, YstartPos, scalingFactor); //int _Xstart, int _Ystart, float _xPos, Float _yPos, float _xPrevious, Float _yPrevious
  myAxes2D = new Axes2D(XstartPos, YstartPos, 10, 5, 2); //int _Xposition, int _Yposition, int _XtickNumber, int _YtickNumber, int _LineWidth
  myAxes2D.run();
}

public void draw(){
if (redrawAxis == true){
  initializeClasses();
  myElectropherogram.showPreserved();
  redrawAxis = false;
}
injectindicators();
handleSerialPort(serialTiming);
}

class Axes2D{
  //Global variables
  float Ylength, Xlength, YscaleFactor, XscaleFactor, Xposition, Yposition;

  int XtickNumber, YtickNumber, LineWidth;
    
  //Constructor
  Axes2D(float _Xposition, float _Yposition, int _XtickNumber, int _YtickNumber, int _LineWidth){
    background(255);
    Xposition = _Xposition;
    Yposition = _Yposition;
    Xlength = width - width/5;
    Ylength = height - height/4;
    XtickNumber = _XtickNumber;
    YtickNumber = _YtickNumber;
    LineWidth = _LineWidth;
  }  //timing, inByte1, Previoustiming, PreviousinByte1
  //Functions
  public void run(){
    showLines();
    showTicks();
    showNumbers();
  }

  public void showLines(){
   stroke(0);
   strokeWeight(LineWidth);
  //top x
   line(Xposition, Yposition, Xposition+Xlength, Yposition);
   // left Y
   line(Xposition, Yposition, Xposition, Yposition+Ylength);
   //bot x
   line(Xposition, Yposition+Ylength, Xposition+Xlength, Yposition+Ylength);
   //right Y
   line(Xposition+Xlength, Yposition, Xposition+Xlength, Yposition+Ylength);
  }

  public void showTicks(){
    int XtickLength = PApplet.parseInt(Xlength) / (XtickNumber+1);
    int YtickLength = PApplet.parseInt(Ylength) / (YtickNumber+1);
   // Y
   stroke(0);
   strokeWeight(1);
   for(int i = 1; i < XtickNumber+1; i++){
   line(Xposition + XtickLength*i, Yposition+Ylength+5, Xposition + XtickLength*i, Yposition+Ylength-5);
   }
   for(int i = 1; i < YtickNumber+1; i++){
   line(Xposition+5, Yposition + YtickLength*i, Xposition-5, Yposition + YtickLength*i);
   }
  }
  public void showNumbers(){
    String TheFormatOfString = "%.6f";
   float calculatedYTicks = Ylength/((YtickNumber+1)*scalingFactor);
   float calculatedXTicks = Xlength/((XtickNumber+1)*timingFactor);
   int XtickLength = PApplet.parseInt(Xlength) / (XtickNumber+1);
   int YtickLength = PApplet.parseInt(Ylength) / (YtickNumber+1);
   for(int i = 1; i < XtickNumber+1; i++){
   float NumbersX = calculatedXTicks * i + TimingOffsetSlider/timingFactor;
   textSize(10);
   fill(0, 0, 0);
   text(String.format("%.1f", NumbersX), Xposition + XtickLength*i - 10, Yposition+Ylength+20);
   }
   for(int i = 1; i < YtickNumber+1; i++){
   float NumbersY = calculatedYTicks * i + offset/scalingFactor + OffsetSliderVal/scalingFactor;
   
      if (NumbersY >= 1000000){
     TheFormatOfString = "%.0f"; }
   else if (NumbersY >= 100000 && NumbersY < 1000000){
     TheFormatOfString = "%.1f"; }
   else if (NumbersY >= 10000 && NumbersY < 100000){
     TheFormatOfString = "%.2f"; }
   else if (NumbersY >= 1000 && NumbersY < 10000){
     TheFormatOfString = "%.3f"; }
   else if (NumbersY >= 100 && NumbersY < 1000){
     TheFormatOfString = "%.4f"; }
   else if (NumbersY >= 10 && NumbersY < 100){
     TheFormatOfString = "%.5f"; }
   else {
     TheFormatOfString = "%.6f"; }  
   
   textSize(10);
   fill(0, 0, 0);
   text(String.format(TheFormatOfString, NumbersY), Xposition-70, Yposition + Ylength - YtickLength*i+5);
   }
  }

}
public void ZoomIn(){
  scalingFactor = scalingFactor * 2.0f;
}

public void ZoomOut(){
  scalingFactor = scalingFactor / 2.0f;
}

public void TimePlus(){
 timingFactor = timingFactor * 2.0f ;
}
public void TimeMinus(){
 timingFactor = timingFactor / 2.0f; 
}

public void OffsetPlus(){
 offset = offset - 300.0f; 
}
public void OffsetUPSmall(){
 offset = offset - 60.0f; 
}

public void OffsetMinus(){
 offset = offset + 300.0f; 
}
public void OffsetDOWNSmall(){
 offset = offset + 60.0f; 
}

public void AnalysisTimeUP(){
 AnalysisButton = AnalysisButton + 1; 
}

public void AnalysisTimeDown(){
 AnalysisButton = AnalysisButton - 1; 
}

public void AnalysisTimeUPTen(){
 AnalysisButton = AnalysisButton + 10; 
}
public void AnalysisTimeDownTen(){
 AnalysisButton = AnalysisButton - 10; 
}

public void ChangeAnalysisTime(){
 String Analysislength = TimeAnalysistextfield1.getText();
 AnalysisButton = PApplet.parseInt(Analysislength) - 10;
}
public void Hz9() {
  myPort.write("9Hz*");
}

public void Hz11() {
  myPort.write("11Hz*");
}

public void Hz13() {
  myPort.write("13Hz*");
}

public void Hz16() {
  myPort.write("16Hz*");
}

public void Hz26() {
  myPort.write("26Hz*");
}

public void Hz50() {
  myPort.write("50Hz*");
}

public void Hz84() {
  myPort.write("84Hz*");
}

public void Hz91() {
  myPort.write("91Hz*");
}

public void Excitation16kHz(){
  myPort.write("16kHz*");
}

public void Excitation32kHz(){
  myPort.write("32kHz*");
}

public void Vdd8(){
  myPort.write("Vdd/8*");
}

public void Vdd4(){
  myPort.write("Vdd/4*");
}

public void Vddx38(){
  myPort.write("Vddx3/8*");
}
public void Vdd2(){
  myPort.write("Vdd/2*");
}

public void CapChop(){
    myPort.write("CAPCHOP1*");
    myPort.clear();
}

public void NormalOperation(){
    myPort.write("NormalOper*");
    myPort.clear();
}

public void VT_on(){
    myPort.write("TemperON*");
    myPort.clear();
    delay(200);
}

public void VT_off(){
    myPort.write("TemperOFF*");
    myPort.clear();
//    delay(100);
}

public void Auto_Zero(){
      myPort.write("Auto Zero*");
      myPort.clear();
}

public void Auto_Zero_Delete(){
      myPort.write("Zero Delete*");
      myPort.clear();
}
public void LiftUp() {
  //  TempCompensation = false;
  myPort.write("LIFTUP*");

  //  TempCompensation = true;
}

public void LiftDown() {
  //  TempCompensation = false;

  myPort.write("LIFTDOWN*");

  //    TempCompensation = true;
}

public void HVON() {
  myPort.write("HVONN*");
}

public void HVOFF() {
  myPort.write("HVOFF*");
}

public void carouselClockwise() {
  myPort.write("CCLOCK*");
}

public void carouselCounterClockwise() {
  myPort.write("CCOUNT*");
}

public void adjustClockwise() {
  myPort.write("ACLOCK*");
}

public void adjustCounterClockwise() {
  myPort.write("ACOUNT*");
}

public void startAnalysis() {
  myPort.write("STARTA*");
}

public void stopAnalysis() {
  myPort.write("STOPA*");
}

public void setElectrokineticInjection() {
  myPort.write("SETELECTRO*");
}

public void setHydrodynamicInjection() {
  myPort.write("SETHYDRO*");
}

public void setPumpON(){
 myPort.write("PUMPON*"); 
}

public void setPumpOFF(){
 myPort.write("PUMPOFF*"); 
}

public void setValvesON(){
 myPort.write("VALVESON*"); 
}

public void setValvesOFF(){
 myPort.write("VALVESOFF*"); 
}

public void selectInjectionTime() {
  if (InjectiondropList1.getSelectedIndex() == 0) {
    injectionTimingTOT = 10.0f;
    //    myPort.write("SIPHON10*");
    //    println("SIPHOOINJECT");
  }
  if (InjectiondropList1.getSelectedIndex() == 1) {
    injectionTimingTOT = 20.0f;
    //    myPort.write("SIPHON20*");
    //    println("SIPHOOINJECT");
  }
  if (InjectiondropList1.getSelectedIndex() == 2) {
    injectionTimingTOT = 40.0f;   
    // myPort.write("SIPHON40*");
    //    println("SIPHOOINJECT");
  }

  if (InjectiondropList1.getSelectedIndex() == 3) {
    injectionTimingTOT = 80.0f;    
    //      myPort.write("SIPHON80*");
    //    println("SIPHOOINJECT");
  }
  if (InjectiondropList1.getSelectedIndex() == 4) {
    injectionTimingTOT = 160.0f;    
    //    myPort.write("SIPHON160*");
    //    println("SIPHOOINJECT");
  }
  if (InjectiondropList1.getSelectedIndex() == 5) {
    injectionTimingTOT = 320.0f;
    //    myPort.write("SIPHON320*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 6) {
    injectionTimingTOT = 1.0f;
    //      myPort.write("ELECTRO1*");
    //      println("ELECTROOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 7) {
    injectionTimingTOT = 2.0f;      
    //      myPort.write("ELECTRO2*");
    //      println("ELECTROOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 8) {
    injectionTimingTOT = 5.0f;
    //      myPort.write("ELECTRO5*");
    //      println("ELECTROOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 9) {
    injectionTimingTOT = 10.0f;
    //      myPort.write("ELECTRO10*");
    //      println("ELECTROOINJECT");
  }
}

public void setInjection() {
  if (InjectiondropList1.getSelectedIndex() == 0) {
    myPort.write("INJ5*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 1) {
    myPort.write("INJ10*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 2) {
    myPort.write("INJ20*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 3) {
    myPort.write("INJ40*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 4) {
    myPort.write("INJ80*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 5) {
    myPort.write("INJ160*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 6) {
    myPort.write("INJ320*");
    //    println("SIPHOOINJECT");
  } else if (InjectiondropList1.getSelectedIndex() == 7) {
    myPort.write("INJ640*");
//    println(InjectiondropList1.getSelectedIndex());
  }
}

public void Red_Led(){
  myPort.write("REDLED*");  
}

public void Green_Led(){
  myPort.write("GREENLED*");
}

public void Blue_Led(){
  myPort.write("BLUELED*");
}

public void Zero_color(){
  myPort.write("ZeroAbsorbance*");
}

public void Clear_Filter(){
  myPort.write("ClearF*");
}
class Electropherogram{
  //Global variables
  float scaleFactor, Xstart, Ystart;
//  int ;
  //Constructor
  Electropherogram(float _Xstart, float _Ystart, float _scaleFactor){
    scaleFactor = _scaleFactor;
    Xstart = _Xstart;
    Ystart = _Ystart + height - height/4;
  }  
  public void run(float _pXpos, float _pYpos, float _Xpos, float _Ypos){
    float pXpos, pYpos, Xpos, Ypos;
    pXpos = _pXpos;
    pYpos = _pYpos;
    Xpos = _Xpos;
    Ypos = _Ypos;
        if (pXpos >= Xstart && pXpos <= Xstart + width - width/5 && Xpos >= Xstart && Xpos <= Xstart + width - width/5
    && pYpos <= Ystart && pYpos >= Ystart - height + height/4 && Ypos <= Ystart && Ypos >= Ystart - height + height/4){
   strokeWeight(2);
   line(pXpos, pYpos, Xpos, Ypos);    
  }
  }
  public void showPreserved(){
     for (int i = 1; i < index; i++){
float  pXshowposition = timingValueArray[i-1]*timingFactor - TimingOffsetSlider + Xstart;
float  pYshowposition = -measuredValueArray[i-1]*scaleFactor + Ystart + offset + OffsetSliderVal;
float  Xshowposition = timingValueArray[i]*timingFactor - TimingOffsetSlider + Xstart;
float  Yshowposition = -measuredValueArray[i]*scaleFactor + Ystart + offset + OffsetSliderVal;   
       if(pXshowposition >= Xstart && pYshowposition <= Xstart + width - width/5 && Xshowposition >= Xstart && Xshowposition <= Xstart + width - width/5
    && pYshowposition <= Ystart && pYshowposition >= Ystart - height + height/4 && Yshowposition <= Ystart && Yshowposition >= Ystart - height + height/4){
     strokeWeight(2);
     line(pXshowposition, pYshowposition, Xshowposition, Yshowposition);
//     println(timingValueArray[i] + " " + measuredValueArray[i]);
//println(pXshowposition + " " + pYshowposition + " " + Xshowposition + " " + Yshowposition);
}
}
}
}
  public void preserveData(float _Xpos, float _Ypos){
    float Xpos, Ypos;
    Xpos = _Xpos;
    Ypos = _Ypos;
    timingValueArray[index] = Xpos;
    measuredValueArray[index] = Ypos;
//    println(timingValueArray[index] + " " + measuredValueArray[index]);
    index++;
  }
  
public void calculateValues(){
  pXposition = Previoustiming*timingFactor - TimingOffsetSlider + XstartPos;
  pYposition = -PreviousinByte1*scalingFactor + YstartPos + height - height/4 + offset + OffsetSliderVal;
  Xposition = graphtiming*timingFactor - TimingOffsetSlider + XstartPos;
  Yposition = -inByte1*scalingFactor + YstartPos + height - height/4 + offset + OffsetSliderVal;
}
public void folderSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    fullPathName = selection.getAbsolutePath();
  }
}

 public void makefilename() //generates filename
{
  int d = day();    // Values from 1 - 31
  String sd = nfs(d, 2);
  int mo = month();  // Values from 1 - 12
  String smo = nfs(mo, 2);
  int y = year();   // 2003, 2004, 2005, etc. 
  String sy = nfs(y, 4);
  int mi = minute();
  String smi = nfs(mi, 2);
  int se = second();
  String sse = nfs(se, 2);
  int h = hour();
  String sh = nfs(h, 2);
  String ending = ".txt";

  String[] date = new String[7];
  date[0] = sy; 
  date[1] = smo; 
  date[2] = sd; 
  date[3] = sh; 
  date[4] = smi; 
  date[5] = sse; 
  date[6] = ending;
  joinedDate = join(date, "");

  String[] fileParts = new String[3];
  fileParts[0] = fullPathName;
  fileParts[1] = "/";
  fileParts[2] = joinedDate;
  DAQfileName = join(fileParts,"");
  println(DAQfileName);
}

public void createDAQFile(){
  outputFile1 = createWriter(DAQfileName); 
}

public void collectData() {
  if (startflag == true && TempCompensation == true){
  outputFile1.println(AnalysisTiming + " " + inByte1 + " " + inByte2 + " " + inByte3);
  }
  else if (startflag == true && TempCompensation == false){
  outputFile1.println(AnalysisTiming + " " + inByte1 + " " + inByte2);
  }
}

public void collectJsonData() {
  if (startflag == true){
  outputFile1.println(AnalysisTiming + " " + capacitance + " " + temperature + " " + current);
  }
}

public void closeDAQFile() {
  outputFile1.flush(); // Writes the remaining data to the file
  outputFile1.close(); // Finishes the file
}

public void calculateAnalysisTiming() {
  if (startflag == true){
 AnalysisTiming = timing - PApplet.parseFloat(timingsubtract)/1000.0f;
  } 
}
public void customGUI() {
  FindSerialPorts(); 
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
  G4P.setCursor(ARROW);
  if (frame != null)
    frame.setTitle("Universal Acquisition 0.2");
  window1 = new GWindow(this, "Serial Port Setup", 400, 100, 520, 300, false, JAVA2D);
  window1.setActionOnClose(G4P.CLOSE_WINDOW);
  window1.addDrawHandler(this, "Setupwin_draw1");
  OpenSerialbutton = new GButton(window1.papplet, 5, 5, 80, 30);
  OpenSerialbutton.setText("Open Serial Port1");
  OpenSerialbutton.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OpenSerialbutton.addEventHandler(this, "OpenSerialbutton1_click1");
  Serial_Ports = new GDropList(window1.papplet, 5, 40, 165, 220, 10);
  Serial_Ports.setItems(SerialArray, 0);
  Serial_Ports.addEventHandler(this, "Serial_PortsdropList1_click1");
  StopSerial = new GButton(window1.papplet, 90, 5, 80, 30);
  StopSerial.setText("STOP Serial Port");
  StopSerial.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  StopSerial.addEventHandler(this, "StopSerialbutton1_click3");
  BaudRate = new GDropList(window1.papplet, 174, 42, 165, 220, 10);
  BaudRate.setItems(loadStrings("list_868333"), 0);
  BaudRate.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  BaudRate.addEventHandler(this, "BaudRate_click1");
  Baudlabel1 = new GLabel(window1.papplet, 174, 6, 165, 30);
  Baudlabel1.setText("Baud Rate");
  Baudlabel1.setTextBold();
  Baudlabel1.setOpaque(false);
  Phrasetextfield1 = new GTextField(window1.papplet, 342, 42, 165, 22, G4P.SCROLLBARS_NONE);
  Phrasetextfield1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  Phrasetextfield1.setOpaque(false);
  Phrasetextfield1.addEventHandler(this, "Phrasetextfield1_change1");
  Initiationlabel1 = new GLabel(window1.papplet, 342, 6, 120, 30);
  Initiationlabel1.setText("Start Phrase");
  Initiationlabel1.setTextBold();
  Initiationlabel1.setOpaque(false);
  OKPhrasebutton1 = new GButton(window1.papplet, 468, 6, 40, 30);
  OKPhrasebutton1.setText("OK");
  OKPhrasebutton1.setTextBold();
  OKPhrasebutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  OKPhrasebutton1.addEventHandler(this, "OKPhrasebutton1_click2");

  window2 = new GWindow(this, "Controls", 0, 0, 375, 170, false, JAVA2D);
  window2.addDrawHandler(this, "win_draw2");
  ZoomIn = new GButton(window2.papplet, 5, 5, 80, 30);
  ZoomIn.setText("Zoom in");
  ZoomIn.setTextBold();
  ZoomIn.addEventHandler(this, "ZoomInbutton1_click1");
  ZoomOut = new GButton(window2.papplet, 5, 40, 80, 30);
  ZoomOut.setText("Zoom out");
  ZoomOut.setTextBold();
  ZoomOut.addEventHandler(this, "ZoomOutbutton2_click1");
  TimePlus = new GButton(window2.papplet, 90, 5, 80, 30);
  TimePlus.setText("Time +");
  TimePlus.setTextBold();
  TimePlus.addEventHandler(this, "TimePlusbutton3_click1");
  TimeMinus = new GButton(window2.papplet, 90, 40, 80, 30);
  TimeMinus.setText("Time -");
  TimeMinus.setTextBold();
  TimeMinus.addEventHandler(this, "TimeMinusbutton4_click1");
  RUN = new GButton(window2.papplet, 180, 5, 39, 30);
  RUN.setText("RUN");
  RUN.setTextBold();
  RUN.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  RUN.addEventHandler(this, "RUNbutton5_click1");
  STOP = new GButton(window2.papplet, 180, 40, 39, 30);
  STOP.setText("STOP");
  STOP.setLocalColorScheme(GCScheme.RED_SCHEME);
  STOP.addEventHandler(this, "STOPbutton6_click1");
  OffsetUp = new GButton(window2.papplet, 5, 75, 60, 30);
  OffsetUp.setText("Offset up");
  OffsetUp.setTextBold();
  OffsetUp.addEventHandler(this, "button1_click1");
  OffsetDown = new GButton(window2.papplet, 90, 75, 60, 30);
  OffsetDown.setText("Offset down");
  OffsetDown.setTextBold();
  OffsetDown.addEventHandler(this, "OffsetDownbutton1_click1");
  Timingslider1 = new GSlider(window2.papplet, 0, 145, 265, 15, 10.0f);
  Timingslider1.setLimits(0.0f, 0.0f, 4000.0f);
  Timingslider1.setNumberFormat(G4P.DECIMAL, 1);
  Timingslider1.setOpaque(false);
  Timingslider1.addEventHandler(this, "Timingslider1_change1");
  Offsetslider2 = new GSlider(window2.papplet, 279, 0, 160, 15, 10.0f);
  Offsetslider2.setRotation(PI/2, GControlMode.CORNER);
  Offsetslider2.setLimits(0.0f, -2000.0f, 2000.0f);
  Offsetslider2.setNumberFormat(G4P.DECIMAL, 2);
  Offsetslider2.setOpaque(false);
  Offsetslider2.addEventHandler(this, "Offsetslider2_change1");
  Settingsbutton1 = new GButton(window2.papplet, 180, 75, 80, 30);
  Settingsbutton1.setText("Settings");
  Settingsbutton1.setTextBold();
  Settingsbutton1.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  Settingsbutton1.addEventHandler(this, "Settingsbutton1_click2");
  EXITbutton1 = new GButton(window2.papplet, 180, 110, 80, 30);
  EXITbutton1.setText("EXIT");
  EXITbutton1.setTextBold();
  EXITbutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  EXITbutton1.addEventHandler(this, "EXITbutton1_click2");
  ChoosePathbutton1 = new GButton(window2.papplet, 282, 132, 80, 30);
  ChoosePathbutton1.setText("Choose Path");
  ChoosePathbutton1.setTextBold();
  ChoosePathbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ChoosePathbutton1.addEventHandler(this, "ChoosePathbutton1_click2");
  AnalysisUPbutton1 = new GButton(window2.papplet, 5, 110, 15, 15);
  AnalysisUPbutton1.setText("\u2191");
  AnalysisUPbutton1.setTextBold();
  AnalysisUPbutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  AnalysisUPbutton1.addEventHandler(this, "AnalysisUP_click2");
  AnalysisDownbutton1 = new GButton(window2.papplet, 47, 110, 15, 15);
  AnalysisDownbutton1.setText("\u2193");
  AnalysisDownbutton1.setTextBold();
  AnalysisDownbutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  AnalysisDownbutton1.addEventHandler(this, "AnalysisDown_click2");
  Analysisslider1 = new GSlider(window2.papplet, 0, 128, 89, 15, 10.0f);
  Analysisslider1.setLimits(0.0f, 0.0f, 100.0f);
  Analysisslider1.setNumberFormat(G4P.DECIMAL, 0);
  Analysisslider1.setOpaque(false);
  Analysisslider1.addEventHandler(this, "Analysisslider1_change1");
  buttonCEControls = new GButton(window2.papplet, 282, 24, 80, 20);
  buttonCEControls.setText("CE Controls");
  buttonCEControls.setTextBold();
  buttonCEControls.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonCEControls.addEventHandler(this, "buttonCEControls_click3");
  buttonCDCControls = new GButton(window2.papplet, 282, 48, 80, 20);
  buttonCDCControls.setText("CDC Controls");
  buttonCDCControls.setTextBold();
  buttonCDCControls.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonCDCControls.addEventHandler(this, "buttonCDCControls_click1");
  buttonAutoZero = new GButton(window2.papplet, 282, 72, 80, 20);
  buttonAutoZero.setText("Auto Zero");
  buttonAutoZero.setTextBold();
  buttonAutoZero.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  buttonAutoZero.addEventHandler(this, "buttonAutoZero_click3");
  buttonAutoZeroDel = new GButton(window2.papplet, 282, 96, 80, 30);
  buttonAutoZeroDel.setText("Auto Zero Delete");
  buttonAutoZeroDel.setTextBold();
  buttonAutoZeroDel.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  buttonAutoZeroDel.addEventHandler(this, "buttonAutoZeroDel_click3");
  ZeroTimebutton1 = new GButton(window2.papplet, 221, 40, 39, 30);
  ZeroTimebutton1.setText("Zero Time");
  ZeroTimebutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  ZeroTimebutton1.addEventHandler(this, "ZeroTimebutton1_click4");
  AutoRun_button1 = new GButton(window2.papplet, 221, 5, 39, 30);
  AutoRun_button1.setText("Auto Run");
  AutoRun_button1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  AutoRun_button1.addEventHandler(this, "AutoRun_button1_click4");
  OffUpSmall_button1 = new GButton(window2.papplet, 70, 75, 15, 30);
  OffUpSmall_button1.setText("\u2191");
  OffUpSmall_button1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OffUpSmall_button1.addEventHandler(this, "OffUpSmall_button1_click4");
  OffDownSmall_button1 = new GButton(window2.papplet, 155, 75, 15, 30);
  OffDownSmall_button1.setText("\u2193");
  OffDownSmall_button1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OffDownSmall_button1.addEventHandler(this, "OffDownSmall_button1_click5");
  ATimePlusTenbutton1 = new GButton(window2.papplet, 21, 110, 24, 15);
  ATimePlusTenbutton1.setText("10\u2191");
  ATimePlusTenbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ATimePlusTenbutton1.addEventHandler(this, "ATimePlusTenbutton1_click4");
  ATimeminusTenbutton1 = new GButton(window2.papplet, 63, 110, 24, 15);
  ATimeminusTenbutton1.setText("10\u2193");
  ATimeminusTenbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ATimeminusTenbutton1.addEventHandler(this, "ATimeminusTenbutton1_click4");
  TimeAnalysistextfield1 = new GTextField(window2.papplet, 90, 110, 50, 30, G4P.SCROLLBARS_NONE);
  TimeAnalysistextfield1.setOpaque(true);
  TimeAnalysistextfield1.addEventHandler(this, "textfield1_change1");
  Selecttimebutton1 = new GButton(window2.papplet, 140, 110, 30, 30);
  Selecttimebutton1.setText("OK");
  Selecttimebutton1.setTextBold();
  Selecttimebutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  Selecttimebutton1.addEventHandler(this, "Selecttimebutton1_click4");
  COLORIMETERbutton1 = new GButton(window2.papplet, 282, 0, 80, 20);
  COLORIMETERbutton1.setText("Colorimeter");
  COLORIMETERbutton1.setTextBold();
  COLORIMETERbutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  COLORIMETERbutton1.addEventHandler(this, "COLORIMETERbutton1_click4");

  //  window3 = new GWindow(this, "CE Controls", 0, 200, 285, 90, false, JAVA2D);
  //  window3.setActionOnClose(G4P.CLOSE_WINDOW);
  //  window3.addDrawHandler(this, "win_draw3");
  //  Lift_UP = new GButton(window3.papplet, 4, 12, 80, 30);
  //  Lift_UP.setText("Lift Up");
  //  Lift_UP.setTextBold();
  //  Lift_UP.addEventHandler(this, "LiftUP_click1");
  //  Lift_DOWN = new GButton(window3.papplet, 4, 48, 80, 30);
  //  Lift_DOWN.setText("Lift Down");
  //  Lift_DOWN.setTextBold();
  //  Lift_DOWN.addEventHandler(this, "LiftDown_click1");
  //  HVbutton1 = new GButton(window3.papplet, 88, 12, 80, 30);
  //  HVbutton1.setText("HV ON");
  //  HVbutton1.setTextBold();
  //  HVbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  //  HVbutton1.addEventHandler(this, "HVbutton1_click1");
  //  HVOFFbutton1 = new GButton(window3.papplet, 88, 48, 80, 30);
  //  HVOFFbutton1.setText("HV OFF");
  //  HVOFFbutton1.setTextBold();
  //  HVOFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  //  HVOFFbutton1.addEventHandler(this, "HVOFFbutton1_click1");
  //  Injectionlabel1 = new GLabel(window3.papplet, 172, 0, 80, 20);
  //  Injectionlabel1.setText("Injection");
  //  Injectionlabel1.setTextBold();
  //  Injectionlabel1.setOpaque(false);
  //  InjectiondropList1 = new GDropList(window3.papplet, 172, 20, 90, 150, 10);
  //  InjectiondropList1.setItems(loadStrings("list_296461"), 0);
  //  InjectiondropList1.addEventHandler(this, "InjectiondropList1_click1");
  //  Injectbutton1 = new GButton(window3.papplet, 172, 48, 80, 30);
  //  Injectbutton1.setText("Inject");
  //  Injectbutton1.setTextBold();
  //  Injectbutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  //  Injectbutton1.addEventHandler(this, "Injectbutton1_click2");
  //  
  //  window4 = new GWindow(this, "CDC Functions", 0, 350, 370, 160, false, JAVA2D);
  //  window4.setActionOnClose(G4P.CLOSE_WINDOW);
  //  window4.addDrawHandler(this, "CDCwin_draw1");
  //  button9Hz = new GButton(window4.papplet, 5, 5, 40, 30);
  //  button9Hz.setText("9Hz");
  //  button9Hz.setTextBold();
  //  button9Hz.addEventHandler(this, "button9Hz_click");
  //  button26Hz = new GButton(window4.papplet, 185, 5, 40, 30);
  //  button26Hz.setText("26Hz");
  //  button26Hz.setTextBold();
  //  button26Hz.addEventHandler(this, "button26Hz_click1");
  //  button91Hz = new GButton(window4.papplet, 320, 5, 40, 30);
  //  button91Hz.setText("91Hz");
  //  button91Hz.setTextBold();
  //  button91Hz.addEventHandler(this, "button91Hz_click1");
  //  button11Hz = new GButton(window4.papplet, 50, 5, 40, 30);
  //  button11Hz.setText("11Hz");
  //  button11Hz.setTextBold();
  //  button11Hz.addEventHandler(this, "button11Hz_click1");
  //  button13Hz = new GButton(window4.papplet, 95, 5, 40, 30);
  //  button13Hz.setText("13Hz");
  //  button13Hz.setTextBold();
  //  button13Hz.addEventHandler(this, "button13Hz_click2");
  //  button16Hz = new GButton(window4.papplet, 140, 5, 40, 30);
  //  button16Hz.setText("16Hz");
  //  button16Hz.setTextBold();
  //  button16Hz.addEventHandler(this, "button16Hz_click2");
  //  button50Hz = new GButton(window4.papplet, 230, 5, 40, 30);
  //  button50Hz.setText("50Hz");
  //  button50Hz.setTextBold();
  //  button50Hz.addEventHandler(this, "button50Hz_click2");
  //  button84Hz = new GButton(window4.papplet, 275, 5, 40, 30);
  //  button84Hz.setText("84Hz");
  //  button84Hz.setTextBold();
  //  button84Hz.addEventHandler(this, "button84Hz_click2");
  //  button16kHz = new GButton(window4.papplet, 5, 40, 80, 30);
  //  button16kHz.setText("16kHz");
  //  button16kHz.setTextBold();
  //  button16kHz.setLocalColorScheme(GCScheme.RED_SCHEME);
  //  button16kHz.addEventHandler(this, "button16kHz_click2");
  //  button32kHz = new GButton(window4.papplet, 90, 40, 80, 30);
  //  button32kHz.setText("32kHz");
  //  button32kHz.setTextBold();
  //  button32kHz.setLocalColorScheme(GCScheme.RED_SCHEME);
  //  button32kHz.addEventHandler(this, "button32kHz_click2");
  //  buttonVdd8 = new GButton(window4.papplet, 5, 75, 50, 30);
  //  buttonVdd8.setText("Vdd/8");
  //  buttonVdd8.setTextBold();
  //  buttonVdd8.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  //  buttonVdd8.addEventHandler(this, "buttonVdd8_click2");
  //  buttonVdd4 = new GButton(window4.papplet, 60, 75, 50, 30);
  //  buttonVdd4.setText("Vdd/4");
  //  buttonVdd4.setTextBold();
  //  buttonVdd4.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  //  buttonVdd4.addEventHandler(this, "buttonVdd4_click2");
  //  buttonVddx38 = new GButton(window4.papplet, 115, 75, 50, 30);
  //  buttonVddx38.setText("Vddx3/8");
  //  buttonVddx38.setTextBold();
  //  buttonVddx38.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  //  buttonVddx38.addEventHandler(this, "buttonVddx38_click2");
  //  buttonVdd2 = new GButton(window4.papplet, 170, 75, 50, 30);
  //  buttonVdd2.setText("Vdd/2");
  //  buttonVdd2.setTextBold();
  //  buttonVdd2.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  //  buttonVdd2.addEventHandler(this, "buttonVdd2_click2");
  //  buttonCapChop = new GButton(window4.papplet, 175, 40, 80, 30);
  //  buttonCapChop.setText("CapChop");
  //  buttonCapChop.setTextBold();
  //  buttonCapChop.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  //  buttonCapChop.addEventHandler(this, "buttonCapChop_click2");
  //  buttonNormal = new GButton(window4.papplet, 260, 40, 80, 30);
  //  buttonNormal.setText("Normal");
  //  buttonNormal.setTextBold();
  //  buttonNormal.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  //  buttonNormal.addEventHandler(this, "button1_click2");
  //  buttonVT_ON = new GButton(window4.papplet, 5, 110, 50, 30);
  //  buttonVT_ON.setText("VT ON");
  //  buttonVT_ON.setTextBold();
  //  buttonVT_ON.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  //  buttonVT_ON.addEventHandler(this, "button1_click3");
  //  buttonVT_OFF = new GButton(window4.papplet, 60, 110, 50, 30);
  //  buttonVT_OFF.setText("VT OFF");
  //  buttonVT_OFF.setTextBold();
  //  buttonVT_OFF.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  //  buttonVT_OFF.addEventHandler(this, "buttonVT_OFF_click3");
}

public void openSettings() {
  FindSerialPorts();
  window1 = new GWindow(this, "Serial Port Setup", 400, 100, 685, 300, false, JAVA2D);
  window1.setActionOnClose(G4P.CLOSE_WINDOW);
  window1.addDrawHandler(this, "Setupwin_draw1");
  OpenSerialbutton = new GButton(window1.papplet, 5, 5, 80, 30);
  OpenSerialbutton.setText("Open Serial Port1");
  OpenSerialbutton.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OpenSerialbutton.addEventHandler(this, "OpenSerialbutton1_click1");
  Serial_Ports = new GDropList(window1.papplet, 5, 40, 165, 220, 10);
  Serial_Ports.setItems(SerialArray, 0);
  Serial_Ports.addEventHandler(this, "Serial_PortsdropList1_click1");
  StopSerial = new GButton(window1.papplet, 90, 5, 80, 30);
  StopSerial.setText("STOP Serial Port");
  StopSerial.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  StopSerial.addEventHandler(this, "StopSerialbutton1_click3");
  BaudRate = new GDropList(window1.papplet, 174, 42, 165, 220, 10);
  BaudRate.setItems(loadStrings("list_868333"), 0);
  BaudRate.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  BaudRate.addEventHandler(this, "BaudRate_click1");
  Baudlabel1 = new GLabel(window1.papplet, 174, 6, 165, 30);
  Baudlabel1.setText("Baud Rate");
  Baudlabel1.setTextBold();
  Baudlabel1.setOpaque(false);
  Phrasetextfield1 = new GTextField(window1.papplet, 342, 42, 165, 22, G4P.SCROLLBARS_NONE);
  Phrasetextfield1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  Phrasetextfield1.setOpaque(false);
  Phrasetextfield1.addEventHandler(this, "Phrasetextfield1_change1");
  Initiationlabel1 = new GLabel(window1.papplet, 342, 6, 120, 30);
  Initiationlabel1.setText("Start Phrase");
  Initiationlabel1.setTextBold();
  Initiationlabel1.setOpaque(false);
  OKPhrasebutton1 = new GButton(window1.papplet, 468, 6, 40, 30);
  OKPhrasebutton1.setText("OK");
  OKPhrasebutton1.setTextBold();
  OKPhrasebutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  OKPhrasebutton1.addEventHandler(this, "OKPhrasebutton1_click2");
  SettingsWindow = true;
}

public void CE_Controls() {
  window3 = new GWindow(this, "CE Controls", 0, 200, 285, 300, false, JAVA2D);
  window3.setActionOnClose(G4P.CLOSE_WINDOW);
  window3.addDrawHandler(this, "win_draw3");
  Lift_UP = new GButton(window3.papplet, 0, 20, 80, 30);
  Lift_UP.setText("Lift Up");
  Lift_UP.setTextBold();
  Lift_UP.addEventHandler(this, "LiftUP_click1");
  Lift_DOWN = new GButton(window3.papplet, 0, 60, 80, 30);
  Lift_DOWN.setText("Lift Down");
  Lift_DOWN.setTextBold();
  Lift_DOWN.addEventHandler(this, "LiftDown_click1");
  HVbutton1 = new GButton(window3.papplet, 90, 20, 80, 30);
  HVbutton1.setText("HV ON");
  HVbutton1.setTextBold();
  HVbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  HVbutton1.addEventHandler(this, "HVbutton1_click1");
  HVOFFbutton1 = new GButton(window3.papplet, 90, 60, 80, 30);
  HVOFFbutton1.setText("HV OFF");
  HVOFFbutton1.setTextBold();
  HVOFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  HVOFFbutton1.addEventHandler(this, "HVOFFbutton1_click1");
  Injectionlabel1 = new GLabel(window3.papplet, 172, 0, 80, 20);
  Injectionlabel1.setText("Injection");
  Injectionlabel1.setTextBold();
  Injectionlabel1.setOpaque(false);
  InjectiondropList1 = new GDropList(window3.papplet, 180, 20, 90, 200, 10);
  InjectiondropList1.setItems(loadStrings("list_296461"), 0);
  InjectiondropList1.addEventHandler(this, "InjectiondropList1_click1");
  Injectbutton1 = new GButton(window3.papplet, 180, 60, 80, 30);
  Injectbutton1.setText("Set Injection");
  Injectbutton1.setTextBold();
  Injectbutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  Injectbutton1.addEventHandler(this, "Injectbutton1_click2");
  Lifterlabel1 = new GLabel(window3.papplet, 0, 0, 80, 20);
  Lifterlabel1.setText("Lifter");
  Lifterlabel1.setTextBold();
  Lifterlabel1.setOpaque(false);
  Voltagelabel1 = new GLabel(window3.papplet, 90, 0, 80, 20);
  Voltagelabel1.setText("Voltage");
  Voltagelabel1.setTextBold();
  Voltagelabel1.setOpaque(false);
  Carousellabel1 = new GLabel(window3.papplet, 0, 100, 80, 20);
  Carousellabel1.setText("Carousel");
  Carousellabel1.setTextBold();
  Carousellabel1.setOpaque(false);
  VialClockbutton1 = new GButton(window3.papplet, 0, 120, 80, 30);
  VialClockbutton1.setText("Vial \u2192");
  VialClockbutton1.addEventHandler(this, "VialClockbutton1_click4");
  VialCounterbutton2 = new GButton(window3.papplet, 0, 160, 80, 30);
  VialCounterbutton2.setText("Vial \u2190");
  VialCounterbutton2.addEventHandler(this, "VialCounterbutton2_click1");
  AdjustClockbutton1 = new GButton(window3.papplet, 90, 120, 80, 30);
  AdjustClockbutton1.setText("Adjust \u2192");
  AdjustClockbutton1.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  AdjustClockbutton1.addEventHandler(this, "AdjustClockbutton1_click4");
  AdjustCounterbutton2 = new GButton(window3.papplet, 90, 160, 80, 30);
  AdjustCounterbutton2.setText("Adjust \u2190");
  AdjustCounterbutton2.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  AdjustCounterbutton2.addEventHandler(this, "AdjustCounterbutton2_click1");
  StartAnalysisbutton1 = new GButton(window3.papplet, 180, 120, 80, 30);
  StartAnalysisbutton1.setText("START ANALYSIS");
  StartAnalysisbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  StartAnalysisbutton1.addEventHandler(this, "StaretAnalysisbutton1_click4");
  StopAnalysisbutton2 = new GButton(window3.papplet, 180, 160, 80, 30);
  StopAnalysisbutton2.setText("STOP ANALYSIS");
  StopAnalysisbutton2.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  StopAnalysisbutton2.addEventHandler(this, "StopAnalysisbutton2_click1");
  InjectMethodlabel1 = new GLabel(window3.papplet, 0, 190, 170, 20);
  InjectMethodlabel1.setText("Injection Method");
  InjectMethodlabel1.setTextBold();
  InjectMethodlabel1.setOpaque(false);
  ElectrokineticInjbutton1 = new GButton(window3.papplet, 0, 210, 80, 30);
  ElectrokineticInjbutton1.setText("Electrokinetic");
  ElectrokineticInjbutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  ElectrokineticInjbutton1.addEventHandler(this, "ElectrokineticInbutton1_click4");
  Hydrodynamicbutton2 = new GButton(window3.papplet, 90, 210, 80, 30);
  Hydrodynamicbutton2.setText("Hydrodynamic");
  Hydrodynamicbutton2.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  Hydrodynamicbutton2.addEventHandler(this, "Hydrodynamicbutton2_click1");
  Pump_label1 = new GLabel(window3.papplet, 0, 240, 80, 20);
  Pump_label1.setText("Pump");
  Pump_label1.setTextBold();
  Pump_label1.setOpaque(false);
  PONbutton1 = new GButton(window3.papplet, 0, 260, 40, 30);
  PONbutton1.setText("ON");
  PONbutton1.setTextBold();
  PONbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  PONbutton1.addEventHandler(this, "PONbutton1_click4");
  POFFbutton1 = new GButton(window3.papplet, 50, 260, 40, 30);
  POFFbutton1.setText("OFF");
  POFFbutton1.setTextBold();
  POFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  POFFbutton1.addEventHandler(this, "POFFbutton1_click4");
  Valveslabel1 = new GLabel(window3.papplet, 130, 240, 80, 20);
  Valveslabel1.setText("Valves");
  Valveslabel1.setTextBold();
  Valveslabel1.setOpaque(false);
  VONbutton1 = new GButton(window3.papplet, 120, 260, 40, 30);
  VONbutton1.setText("ON");
  VONbutton1.setTextBold();
  VONbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  VONbutton1.addEventHandler(this, "VONbutton1_click4");
  VOFFbutton1 = new GButton(window3.papplet, 170, 260, 40, 30);
  VOFFbutton1.setText("OFF");
  VOFFbutton1.setTextBold();
  VOFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  VOFFbutton1.addEventHandler(this, "VOFFbutton1_click4");
}

public void CDC_Controls() {
  window4 = new GWindow(this, "CDC Functions", 0, 350, 370, 160, false, JAVA2D);
  window4.setActionOnClose(G4P.CLOSE_WINDOW);
  window4.addDrawHandler(this, "CDCwin_draw1");
  button9Hz = new GButton(window4.papplet, 5, 5, 40, 30);
  button9Hz.setText("9Hz");
  button9Hz.setTextBold();
  button9Hz.addEventHandler(this, "button9Hz_click");
  button26Hz = new GButton(window4.papplet, 185, 5, 40, 30);
  button26Hz.setText("26Hz");
  button26Hz.setTextBold();
  button26Hz.addEventHandler(this, "button26Hz_click1");
  button91Hz = new GButton(window4.papplet, 320, 5, 40, 30);
  button91Hz.setText("91Hz");
  button91Hz.setTextBold();
  button91Hz.addEventHandler(this, "button91Hz_click1");
  button11Hz = new GButton(window4.papplet, 50, 5, 40, 30);
  button11Hz.setText("11Hz");
  button11Hz.setTextBold();
  button11Hz.addEventHandler(this, "button11Hz_click1");
  button13Hz = new GButton(window4.papplet, 95, 5, 40, 30);
  button13Hz.setText("13Hz");
  button13Hz.setTextBold();
  button13Hz.addEventHandler(this, "button13Hz_click2");
  button16Hz = new GButton(window4.papplet, 140, 5, 40, 30);
  button16Hz.setText("16Hz");
  button16Hz.setTextBold();
  button16Hz.addEventHandler(this, "button16Hz_click2");
  button50Hz = new GButton(window4.papplet, 230, 5, 40, 30);
  button50Hz.setText("50Hz");
  button50Hz.setTextBold();
  button50Hz.addEventHandler(this, "button50Hz_click2");
  button84Hz = new GButton(window4.papplet, 275, 5, 40, 30);
  button84Hz.setText("84Hz");
  button84Hz.setTextBold();
  button84Hz.addEventHandler(this, "button84Hz_click2");
  button16kHz = new GButton(window4.papplet, 5, 40, 80, 30);
  button16kHz.setText("16kHz");
  button16kHz.setTextBold();
  button16kHz.setLocalColorScheme(GCScheme.RED_SCHEME);
  button16kHz.addEventHandler(this, "button16kHz_click2");
  button32kHz = new GButton(window4.papplet, 90, 40, 80, 30);
  button32kHz.setText("32kHz");
  button32kHz.setTextBold();
  button32kHz.setLocalColorScheme(GCScheme.RED_SCHEME);
  button32kHz.addEventHandler(this, "button32kHz_click2");
  buttonVdd8 = new GButton(window4.papplet, 5, 75, 50, 30);
  buttonVdd8.setText("Vdd/8");
  buttonVdd8.setTextBold();
  buttonVdd8.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVdd8.addEventHandler(this, "buttonVdd8_click2");
  buttonVdd4 = new GButton(window4.papplet, 60, 75, 50, 30);
  buttonVdd4.setText("Vdd/4");
  buttonVdd4.setTextBold();
  buttonVdd4.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVdd4.addEventHandler(this, "buttonVdd4_click2");
  buttonVddx38 = new GButton(window4.papplet, 115, 75, 50, 30);
  buttonVddx38.setText("Vddx3/8");
  buttonVddx38.setTextBold();
  buttonVddx38.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVddx38.addEventHandler(this, "buttonVddx38_click2");
  buttonVdd2 = new GButton(window4.papplet, 170, 75, 50, 30);
  buttonVdd2.setText("Vdd/2");
  buttonVdd2.setTextBold();
  buttonVdd2.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVdd2.addEventHandler(this, "buttonVdd2_click2");
  buttonCapChop = new GButton(window4.papplet, 175, 40, 80, 30);
  buttonCapChop.setText("CapChop");
  buttonCapChop.setTextBold();
  buttonCapChop.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  buttonCapChop.addEventHandler(this, "buttonCapChop_click2");
  buttonNormal = new GButton(window4.papplet, 260, 40, 80, 30);
  buttonNormal.setText("Normal");
  buttonNormal.setTextBold();
  buttonNormal.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  buttonNormal.addEventHandler(this, "button1_click2");
  buttonVT_ON = new GButton(window4.papplet, 5, 110, 50, 30);
  buttonVT_ON.setText("VT ON");
  buttonVT_ON.setTextBold();
  buttonVT_ON.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonVT_ON.addEventHandler(this, "button1_click3");
  buttonVT_OFF = new GButton(window4.papplet, 60, 110, 50, 30);
  buttonVT_OFF.setText("VT OFF");
  buttonVT_OFF.setTextBold();
  buttonVT_OFF.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonVT_OFF.addEventHandler(this, "buttonVT_OFF_click3");
  CDCcontrolsWindow = true;
}

public void ColorimeterWin() {
  window5 = new GWindow(this, "Colorimeter", 400, 0, 185, 120, false, JAVA2D);
  window5.setActionOnClose(G4P.CLOSE_WINDOW);
  window5.addDrawHandler(this, "Colorimeterwin_draw1");
  REDbutton1 = new GButton(window5.papplet, 6, 6, 80, 30);
  REDbutton1.setText("RED");
  REDbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  REDbutton1.addEventHandler(this, "REDbutton1_click4");
  GREENbutton2 = new GButton(window5.papplet, 6, 42, 80, 30);
  GREENbutton2.setText("GREEN");
  GREENbutton2.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  GREENbutton2.addEventHandler(this, "GREENbutton2_click1");
  BLUEbutton3 = new GButton(window5.papplet, 6, 78, 80, 30);
  BLUEbutton3.setText("BLUE");
  BLUEbutton3.addEventHandler(this, "BLUEbutton3_click1");
  ZERObutton1 = new GButton(window5.papplet, 96, 6, 80, 30);
  ZERObutton1.setText("ZERO");
  ZERObutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  ZERObutton1.addEventHandler(this, "ZERObutton1_click4");
  Clearbutton1 = new GButton(window5.papplet, 96, 42, 80, 30);
  Clearbutton1.setText("CLEAR");
  Clearbutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  Clearbutton1.addEventHandler(this, "Clearbutton1_click4");
  Colorimeterwindow = true;
}

public void showIndicator(){
String indicatorstring = "no data";
String timeValue = "0";
String TempIndicator = "no data";
String CurrentIndicator = "no data";

  indicatorstring = String.format("%.6f", inByte1);
//  timeValue = str(AnalysisTiming %.1f);
  timeValue = String.format("%.1f", AnalysisTiming);
  
if (startflag == false){
  strokeWeight(0);
  fill(255, 0, 0);
  ellipse(30, height-30, 40, 40);
  }
if (startflag == true){
  strokeWeight(0);
  fill(0, 255, 0);
  ellipse(30, height-30, 40, 40);
  }
  
fill(255);
rect(55, height-55, 100, 40);

textSize(12);
fill(0);
text(indicatorstring, 60, height-25);
text("Value", 60, height-40); 

  TempIndicator = String.format("%.4f", inByte3);
  fill(255);
rect(55, height-85, 100, 30);

textSize(12);
fill(0);
text(TempIndicator, 60, height-58);
text("Temperature", 60, height-72); 

  CurrentIndicator = String.format("%.2f", inByte2);
  fill(255);
rect(5, height-85, 50, 30);

textSize(12);
fill(0);
text(CurrentIndicator, 10, height-58);
text("Current", 10, height-72);

//injectindicators();

fill(255);
rect(160, height-55, 100, 40);

textSize(12);
fill(0);
text(timeValue, 165, height-25);
text("Time (s)", 165, height-40); 

fill(255);
rect(265, height-55, 170, 40);

textSize(12);
fill(0);
text("File Name", 270, height-40);
text(joinedDate, 270, height-25);

fill(255);
rect(440, height-55, 170, 40);

textSize(12);
fill(0);
text("Analysis Time", 445, height-40);
text(String.format("%.1f", AnalysisTime) + " (min)", 445, height-25);
text(String.format("%.1f", AnalysisTime*60) + " (s)", 530, height-25);

fill(255);
rect(615, height-55, width - 615-5, 40);

textSize(12);
fill(0);
text("File Path", 620, height-40);
if(fullPathName != null){
text(fullPathName, 620, height-25); }
//text(String.format("%.1f", AnalysisTime*60) + " (s)", 530, height-25);

} 
public void StartTheAnalysis(){
startflag = true;
makefilename();
createDAQFile();
AnalysisTiming = 0.0f;
timingsubtract = millis();
index = 0;
float[] timingValueArray = new float[1000000];
float[] measuredValueArray = new float[1000000];
Previoustiming = 0.0f;
initializeClasses();
}

public void ZeroTimeMark(){
//    println(AnalysisTiming);
  AnalysisTiming = 0.0f;
  timingsubtract = millis();
//    println(timingsubtract);
  index = 0;
  float[] timingValueArray = new float[1000000];
  float[] measuredValueArray = new float[1000000];
  Previoustiming = 0.0f;
//    println(AnalysisTiming);
  initializeClasses();
}

public void injectindicators(){
    if (injectionSatrted == true){
  String InjectionIndicator = "no  data";

  injectionTiming = (injectionTimingTOT * 1000 + injectpresenttime - millis())/1000;
  InjectionIndicator = String.format("%.2f", injectionTiming);
  fill(255);
rect(160, height-85, 100, 30);
  
  textSize(12);
fill(0);
text(InjectionIndicator, 165, height-58);
text("Injection Timer", 165, height-72); 
  
if (injectionTiming <= 0.0f){
  injectionSatrted = false;
    fill(255);
rect(160, height-85, 100, 30);

player.play();
delay(1750);

  player.close();

  //since close closes the file, we'll load it again
  player = minim.loadFile("injectionsample.mp3");
redrawAxis = true;
}
}
}
public void FindSerialPorts() {
  SerialArray = Serial.list();
  if (SerialArray.length == 0) {
    SerialArray = loadStrings("list_757299");
    nodevice = true;
  } else if (SerialArray.length > 0) {
    nodevice = false;
  }
}

public void serialPortSelect() {
  myPort = new Serial(this, Serial.list()[Serial_Ports.getSelectedIndex()], BaudrateNumber);
  println("Serial port index" + Serial_Ports.getSelectedIndex() + " " + "Baud rate" + BaudrateNumber);
  myPort.bufferUntil('\n');
}


public void serialEvent (Serial myPort) {
  // get the ASCII string:
  // while (myPort.available() > 0) {
  inString1 = myPort.readStringUntil('\n');
  if (inString1 != null && !inString1.isEmpty()) {
    // trim off any whitespace:
    inString1 = trim(inString1);
    //  println(inString1);

    JSONObject json;
    //  float temperature, capacitance;
    json = JSONObject.parse(inString1);
    if (json == null)
    {
      inString1 = inStringPrev;
    } else
    {
      temperature = getTemp(json);
      capacitance = getCap(json);
      inByte1 = getCap(json);
      //  inByte2 = getCurrent(json);
      inByte3 = getTemp(json);
      inStringPrev = inString1;
    }

    if (inByte1 == 1.234567890f) {
      autostartfunction();
    }
    //  StringConversion();
    serialTiming = millis();
    timing = PApplet.parseFloat(millis())/1000.0f;
    graphtiming = timing - PApplet.parseFloat(timingsubtract)/1000.0f;
    // collectData();
    collectJsonData();
    calculateAnalysisTiming();
    showIndicator();
    calculateValues();
    myElectropherogram.run(pXposition, pYposition, Xposition, Yposition);
    preserveData(graphtiming, inByte1); //float xvalue, float yvalue
    timingfunction();
    AutoStop();
  }
}
//}

public void timingfunction() {
  Previoustiming = graphtiming;
  PreviousinByte1 = inByte1;
}

public void autostartfunction() {
  // if (inString1.equals(StartPass) == true){
  StopTheAnalysis();
  StartTheAnalysis();
  //  println(inString1);
  // }
}

public void StopTheAnalysis() {
  if (startflag == true) {
    startflag = false;
    closeDAQFile();
  }
}

public void AutoStop() {
  AnalysisTime = 10.0f + AnalysisButton + SliderAnTime;
  if (startflag == true && AnalysisTiming > AnalysisTime*60.0f) {
    StopTheAnalysis();
  }
}

public void StringConversion() {
  // String[] ReceivedStrings = split(inString1, " ");
  String[] ReceivedStrings = splitTokens(inString1, " ");
  // println(ReceivedStrings.length + " values found");
  // println(ReceivedStrings[0]);
  // println(ReceivedStrings[1]);
  // println(ReceivedStrings[2]);
  // println(inString1);
  // println(ReceivedStrings[0]);
  //if ("1234567890.123456789".equals(inString1) == true){
  //autostartfunction();
  //}
  // println(inString1);
  // println(StartPass);
  // inByte1 = float(inString1);
  // ReceivedStrings[0] = trim(ReceivedStrings[0]);
  if (ReceivedStrings.length >= 1 && ReceivedStrings[0] != null && !ReceivedStrings[0].isEmpty()) {
    // ReceivedStrings[0] = trim(ReceivedStrings[0]);
    inByte1 = PApplet.parseFloat(ReceivedStrings[0]);
  }

  if (ReceivedStrings.length >= 2 && ReceivedStrings[1] != null && !ReceivedStrings[1].isEmpty()) {
    //  ReceivedStrings[1] = trim(ReceivedStrings[1]);
    inByte2 = PApplet.parseFloat(ReceivedStrings[1]);
    //   println(inByte2);
  }

  if (ReceivedStrings.length >= 3 && ReceivedStrings[2] != null && !ReceivedStrings[2].isEmpty()) {
    //  ReceivedStrings[2] = trim(ReceivedStrings[2]);  
    inByte3 = PApplet.parseFloat(ReceivedStrings[2]);
    // println(inByte3);
  }
}

public void handleSerialPort(long timingMarker)
{
  if (timingMarker + serialTimingPeriod <= millis() && serialStarted == true) {
    myPort.stop();
    println("Serial stopped");
    delay(300);
    serialPortSelect();
    timingfunction();
    initializeClasses();
    println("Serial open attempt");
    serialTiming = millis();
  }
  else {}
}

float Previoustiming, PreviousinByte1, timing, graphtiming = 0.0f, AnalysisTiming = 0.0f, inByte1, inByte2, inByte3, scalingFactor = 40.0f, timingFactor = 1.0f, offset = 0.0f, OffsetSliderVal = 0.0f, TimingOffsetSlider = 0.0f;
float pXposition, pYposition, Xposition, Yposition, XstartPos = 100.0f, YstartPos = 20.0f, SliderAnTime = 0.0f, AnalysisTime = 10.0f;
int index = 0, timingsubtract = 0, AnalysisButton = 0;

float capacitance = 0.0f, temperature = 25.0f, current = 0.0f;

float injectionTiming = 0.0f, injectionTimingTOT = 0.0f, injectpresenttime = 0.0f;

PrintWriter outputFile1;

String StartPass = "1234567890.123456789"; //"Analysis Start"; "START"; //

String inString1, inString2, inStringPrev;

String fullPathName, DAQfileName, joinedDate = "No File Selected";

boolean redrawAxis = false, startflag = false, nodevice = true, TempCompensation = false, injectionSatrted = false, Colorimeterwindow = false, CEcontrolswindow = false, CDCcontrolsWindow = false, SettingsWindow = true;

int BaudrateNumber;

int SerialPortNumber = 0; 
String[] SerialArray;

float[] timingValueArray = new float[1000000];
float[] measuredValueArray = new float[1000000];

long serialTiming = 0, serialTimingPeriod = 8000;
boolean serialStarted = false;

public void ArrayInit() {
  SerialArray = new String[20];
}

/* =========================================================
 * ====                   WARNING                        ===
 * =========================================================
 * The code in this tab has been generated from the GUI form
 * designer and care should be taken when editing this file.
 * Only add/edit code inside the event handlers i.e. only
 * use lines between the matching comment tags. e.g.

 void myBtnEvents(GButton button) { //_CODE_:button1:12356:
     // It is safe to enter your event code here  
 } //_CODE_:button1:12356:
 
 * Do not rename this tab!
 * =========================================================
 */

synchronized public void Setupwin_draw1(GWinApplet appc, GWinData data) { //_CODE_:window1:457692:
  appc.background(230);
} //_CODE_:window1:457692:

public void OpenSerialbutton1_click1(GButton source, GEvent event) { //_CODE_:OpenSerialbutton:332947:
//  println("OpenSerialbutton - GButton event occured " + System.currentTimeMillis()%10000000 );
if (nodevice == false){
  serialPortSelect();
  timingfunction();
  initializeClasses();
  serialStarted = true;
}
} //_CODE_:OpenSerialbutton:332947:

public void Serial_PortsdropList1_click1(GDropList source, GEvent event) { //_CODE_:Serial_Ports:605734:
  println("dropList1 - GDropList event occured " + System.currentTimeMillis()%10000000 );
  println(Serial_Ports.getSelectedIndex());
  println(Serial_Ports.getSelectedText());
} //_CODE_:Serial_Ports:605734:

public void StopSerialbutton1_click3(GButton source, GEvent event) { //_CODE_:StopSerial:770184:
  println("StopSerial - GButton event occured " + System.currentTimeMillis()%10000000 );
  myPort.stop();
  serialStarted = false;
} //_CODE_:StopSerial:770184:

public void BaudRate_click1(GDropList source, GEvent event) { //_CODE_:BaudRate:868333:
//  println("Serial_Ports4 - GDropList event occured " + System.currentTimeMillis()%10000000 );
BaudrateNumber = PApplet.parseInt(BaudRate.getSelectedText());
println(BaudrateNumber);
} //_CODE_:BaudRate:868333:

public void Phrasetextfield1_change1(GTextField source, GEvent event) { //_CODE_:Phrasetextfield1:641956:
//  println("Phrasetextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
//println(StartPass);
} //_CODE_:Phrasetextfield1:641956:

public void OKPhrasebutton1_click2(GButton source, GEvent event) { //_CODE_:OKPhrasebutton1:955155:
//  println("OKPhrasebutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
StartPass = Phrasetextfield1.getText();
//println(StartPass);
} //_CODE_:OKPhrasebutton1:955155:

synchronized public void win_draw2(GWinApplet appc, GWinData data) { //_CODE_:window2:639670:
  appc.background(230);
} //_CODE_:window2:639670:

public void ZoomInbutton1_click1(GButton source, GEvent event) { //_CODE_:ZoomIn:293652:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  ZoomIn();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:ZoomIn:293652:

public void ZoomOutbutton2_click1(GButton source, GEvent event) { //_CODE_:ZoomOut:547055:
//  println("button2 - GButton event occured " + System.currentTimeMillis()%10000000 );
//  println(yScaleMaximum);
  ZoomOut();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:ZoomOut:547055:

public void TimePlusbutton3_click1(GButton source, GEvent event) { //_CODE_:TimePlus:301917:
//  println("button3 - GButton event occured " + System.currentTimeMillis()%10000000 );
  TimePlus();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:TimePlus:301917:

public void TimeMinusbutton4_click1(GButton source, GEvent event) { //_CODE_:TimeMinus:450192:
//  println("button4 - GButton event occured " + System.currentTimeMillis()%10000000 );
  TimeMinus();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:TimeMinus:450192:

public void RUNbutton5_click1(GButton source, GEvent event) { //_CODE_:RUN:205394:
//  println("button5 - GButton event occured " + System.currentTimeMillis()%10000000 );
StartTheAnalysis();

} //_CODE_:RUN:205394:

public void STOPbutton6_click1(GButton source, GEvent event) { //_CODE_:STOP:563742:
//  println("button6 - GButton event occured " + System.currentTimeMillis()%10000000 );
startflag = false;
closeDAQFile();
} //_CODE_:STOP:563742:

public void button1_click1(GButton source, GEvent event) { //_CODE_:OffsetUp:500480:
//  println("OffsetUp - GButton event occured " + System.currentTimeMillis()%10000000 );
  OffsetPlus();
    initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:OffsetUp:500480:

public void OffsetDownbutton1_click1(GButton source, GEvent event) { //_CODE_:OffsetDown:201743:
//  println("OffsetDown - GButton event occured " + System.currentTimeMillis()%10000000 );
  OffsetMinus();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:OffsetDown:201743:

public void Timingslider1_change1(GSlider source, GEvent event) { //_CODE_:Timingslider1:686558:
//  println("slider1 - GSlider event occured " + System.currentTimeMillis()%10000000 );
  TimingOffsetSlider = Timingslider1.getValueF();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:Timingslider1:686558:

public void Offsetslider2_change1(GSlider source, GEvent event) { //_CODE_:Offsetslider2:317894:
//  println("slider2 - GSlider event occured " + System.currentTimeMillis()%10000000 );
  OffsetSliderVal = Offsetslider2.getValueF();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:Offsetslider2:317894:

public void Settingsbutton1_click2(GButton source, GEvent event) { //_CODE_:Settingsbutton1:657155:
//  println("Settingsbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
if (SettingsWindow == false)
openSettings();
else if (SettingsWindow == true){
  window1.close();
 SettingsWindow = false; 
}
} //_CODE_:Settingsbutton1:657155:

public void EXITbutton1_click2(GButton source, GEvent event) { //_CODE_:EXITbutton1:287530:
//  println("EXITbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
if (startflag == true)
closeDAQFile();
exit();
} //_CODE_:EXITbutton1:287530:

public void ChoosePathbutton1_click2(GButton source, GEvent event) { //_CODE_:ChoosePathbutton1:390093:
  //println("ChoosePathbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  selectFolder("Select folder", "folderSelected");
} //_CODE_:ChoosePathbutton1:390093:

public void AnalysisUP_click2(GButton source, GEvent event) { //_CODE_:AnalysisUPbutton1:537956:
//  println("AnalysisUPbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
AnalysisTimeUP();
} //_CODE_:AnalysisUPbutton1:537956:

public void AnalysisDown_click2(GButton source, GEvent event) { //_CODE_:AnalysisDownbutton1:222072:
//  println("AnalysisDownbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
AnalysisTimeDown();
} //_CODE_:AnalysisDownbutton1:222072:

public void Analysisslider1_change1(GSlider source, GEvent event) { //_CODE_:Analysisslider1:262109:
//  println("Analysisslider1 - GSlider event occured " + System.currentTimeMillis()%10000000 );
SliderAnTime = Analysisslider1.getValueF();
} //_CODE_:Analysisslider1:262109:

public void buttonCEControls_click3(GButton source, GEvent event) { //_CODE_:buttonCEControls:977250:
//  println("buttonCEControls - GButton event occured " + System.currentTimeMillis()%10000000 );
if (CEcontrolswindow == false)
CE_Controls();
else if (CEcontrolswindow == true){
  window3.close();
 CEcontrolswindow = false; 
}
} //_CODE_:buttonCEControls:977250:

public void buttonCDCControls_click1(GButton source, GEvent event) { //_CODE_:buttonCDCControls:580484:
//  println("buttonCDCControls - GButton event occured " + System.currentTimeMillis()%10000000 );
if (CDCcontrolsWindow == false)
CDC_Controls();
else if (CDCcontrolsWindow == true){
  window4.close();
 CDCcontrolsWindow = false; 
}
} //_CODE_:buttonCDCControls:580484:

public void buttonAutoZero_click3(GButton source, GEvent event) { //_CODE_:buttonAutoZero:986137:
//  println("buttonAutoZero - GButton event occured " + System.currentTimeMillis()%10000000 );
Auto_Zero();
} //_CODE_:buttonAutoZero:986137:

public void buttonAutoZeroDel_click3(GButton source, GEvent event) { //_CODE_:buttonAutoZeroDel:665328:
//  println("buttonAutoZeroDel - GButton event occured " + System.currentTimeMillis()%10000000 );
Auto_Zero_Delete();
} //_CODE_:buttonAutoZeroDel:665328:

public void ZeroTimebutton1_click4(GButton source, GEvent event) { //_CODE_:ZeroTimebutton1:853513:
//  println("ZeroTimebutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
ZeroTimeMark();
} //_CODE_:ZeroTimebutton1:853513:

public void AutoRun_button1_click4(GButton source, GEvent event) { //_CODE_:AutoRun_button1:968922:
//  println("AutoRun_button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
 LiftUp();
   HVON();
   StartTheAnalysis();
} //_CODE_:AutoRun_button1:968922:

public void OffUpSmall_button1_click4(GButton source, GEvent event) { //_CODE_:OffUpSmall_button1:751547:
//  println("OffUpSmall_button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  OffsetUPSmall();
    initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:OffUpSmall_button1:751547:

public void OffDownSmall_button1_click5(GButton source, GEvent event) { //_CODE_:OffDownSmall_button1:315439:
//  println("OffDownSmall_button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  OffsetDOWNSmall();
  initializeClasses();
  myElectropherogram.showPreserved();
} //_CODE_:OffDownSmall_button1:315439:

public void ATimePlusTenbutton1_click4(GButton source, GEvent event) { //_CODE_:ATimePlusTenbutton1:754106:
//  println("ATimePlusTenbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
AnalysisTimeUPTen();
} //_CODE_:ATimePlusTenbutton1:754106:

public void ATimeminusTenbutton1_click4(GButton source, GEvent event) { //_CODE_:ATimeminusTenbutton1:401252:
//  println("ATimeminusTenbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
AnalysisTimeDownTen();
} //_CODE_:ATimeminusTenbutton1:401252:

public void textfield1_change1(GTextField source, GEvent event) { //_CODE_:TimeAnalysistextfield1:551700:
//  println("TimeAnalysistextfield1 - GTextField event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:TimeAnalysistextfield1:551700:

public void Selecttimebutton1_click4(GButton source, GEvent event) { //_CODE_:Selecttimebutton1:909384:
//  println("Selecttimebutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
ChangeAnalysisTime();
} //_CODE_:Selecttimebutton1:909384:

public void COLORIMETERbutton1_click4(GButton source, GEvent event) { //_CODE_:COLORIMETERbutton1:303350:
//  println("COLORIMETERbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
if (Colorimeterwindow == false)
ColorimeterWin();
else if (Colorimeterwindow == true){
window5.close();
Colorimeterwindow = false;
}
} //_CODE_:COLORIMETERbutton1:303350:

synchronized public void win_draw3(GWinApplet appc, GWinData data) { //_CODE_:window3:390555:
  appc.background(230);
} //_CODE_:window3:390555:

public void LiftUP_click1(GButton source, GEvent event) { //_CODE_:Lift_UP:307497:
 // println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
 LiftUp();
} //_CODE_:Lift_UP:307497:

public void LiftDown_click1(GButton source, GEvent event) { //_CODE_:Lift_DOWN:335070:
  //println("Lift_DOWN - GButton event occured " + System.currentTimeMillis()%10000000 );
  LiftDown();
} //_CODE_:Lift_DOWN:335070:

public void HVbutton1_click1(GButton source, GEvent event) { //_CODE_:HVbutton1:344984:
  //println("HVbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  HVON();
} //_CODE_:HVbutton1:344984:

public void HVOFFbutton1_click1(GButton source, GEvent event) { //_CODE_:HVOFFbutton1:924694:
  //println("HVOFFbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  HVOFF();
} //_CODE_:HVOFFbutton1:924694:

public void InjectiondropList1_click1(GDropList source, GEvent event) { //_CODE_:InjectiondropList1:296461:
//  println(InjectiondropList1.getSelectedIndex());
//  println(InjectiondropList1.getSelectedText());
 // println("InjectiondropList1 - GDropList event occured " + System.currentTimeMillis()%10000000 );
} //_CODE_:InjectiondropList1:296461:

public void Injectbutton1_click2(GButton source, GEvent event) { //_CODE_:Injectbutton1:429946:
  //println("Injectbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
  setInjection();
//  injectionSatrted = true;
//  selectInjectionTime();
//  injectionTimingTOT = 320.0;
//  injectpresenttime = millis();
} //_CODE_:Injectbutton1:429946:

public void VialClockbutton1_click4(GButton source, GEvent event) { //_CODE_:VialClockbutton1:831412:
//  println("VialClockbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
carouselClockwise();
} //_CODE_:VialClockbutton1:831412:

public void VialCounterbutton2_click1(GButton source, GEvent event) { //_CODE_:VialCounterbutton2:772136:
//  println("VialCounterbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
carouselCounterClockwise();
} //_CODE_:VialCounterbutton2:772136:

public void AdjustClockbutton1_click4(GButton source, GEvent event) { //_CODE_:AdjustClockbutton1:438671:
//  println("AdjustClockbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
adjustClockwise();
} //_CODE_:AdjustClockbutton1:438671:

public void AdjustCounterbutton2_click1(GButton source, GEvent event) { //_CODE_:AdjustCounterbutton2:485420:
//  println("button2 - GButton event occured " + System.currentTimeMillis()%10000000 );
adjustCounterClockwise();
} //_CODE_:AdjustCounterbutton2:485420:

public void StaretAnalysisbutton1_click4(GButton source, GEvent event) { //_CODE_:StartAnalysisbutton1:528062:
//  println("StartAnalysisbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
startAnalysis();
} //_CODE_:StartAnalysisbutton1:528062:

public void StopAnalysisbutton2_click1(GButton source, GEvent event) { //_CODE_:StopAnalysisbutton2:483616:
//  println("StopAnalysisbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
stopAnalysis();
 if(startflag == true){
     startflag = false;
     closeDAQFile();
 } 
} //_CODE_:StopAnalysisbutton2:483616:

public void ElectrokineticInbutton1_click4(GButton source, GEvent event) { //_CODE_:ElectrokineticInjbutton1:546233:
//  println("ElectrokineticInjbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setElectrokineticInjection();
} //_CODE_:ElectrokineticInjbutton1:546233:

public void Hydrodynamicbutton2_click1(GButton source, GEvent event) { //_CODE_:Hydrodynamicbutton2:857095:
//  println("Hydrodynamicbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
setHydrodynamicInjection();
} //_CODE_:Hydrodynamicbutton2:857095:

public void PONbutton1_click4(GButton source, GEvent event) { //_CODE_:PONbutton1:839225:
//  println("PONbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setPumpON();
} //_CODE_:PONbutton1:839225:

public void POFFbutton1_click4(GButton source, GEvent event) { //_CODE_:POFFbutton1:500656:
//  println("POFFbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setPumpOFF();
} //_CODE_:POFFbutton1:500656:

public void VONbutton1_click4(GButton source, GEvent event) { //_CODE_:VONbutton1:990027:
//  println("VONbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
 setValvesON();
} //_CODE_:VONbutton1:990027:

public void VOFFbutton1_click4(GButton source, GEvent event) { //_CODE_:VOFFbutton1:344415:
//  println("VOFFbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
setValvesOFF();
} //_CODE_:VOFFbutton1:344415:

synchronized public void CDCwin_draw1(GWinApplet appc, GWinData data) { //_CODE_:window4:234483:
  appc.background(230);
} //_CODE_:window4:234483:

public void button9Hz_click(GButton source, GEvent event) { //_CODE_:button9Hz:938928:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz9();
} //_CODE_:button9Hz:938928:

public void button26Hz_click1(GButton source, GEvent event) { //_CODE_:button26Hz:356445:
//  println("button2 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz26();
} //_CODE_:button26Hz:356445:

public void button91Hz_click1(GButton source, GEvent event) { //_CODE_:button91Hz:370334:
//  println("button3 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz91();
} //_CODE_:button91Hz:370334:

public void button11Hz_click1(GButton source, GEvent event) { //_CODE_:button11Hz:293365:
//  println("button4 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz11();
} //_CODE_:button11Hz:293365:

public void button13Hz_click2(GButton source, GEvent event) { //_CODE_:button13Hz:986125:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz13();
} //_CODE_:button13Hz:986125:

public void button16Hz_click2(GButton source, GEvent event) { //_CODE_:button16Hz:782219:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz16();
} //_CODE_:button16Hz:782219:

public void button50Hz_click2(GButton source, GEvent event) { //_CODE_:button50Hz:636183:
//  println("button50Hz - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz50();
} //_CODE_:button50Hz:636183:

public void button84Hz_click2(GButton source, GEvent event) { //_CODE_:button84Hz:820197:
//  println("button84Hz - GButton event occured " + System.currentTimeMillis()%10000000 );
Hz84();
} //_CODE_:button84Hz:820197:

public void button16kHz_click2(GButton source, GEvent event) { //_CODE_:button16kHz:508751:
//  println("button16kHz - GButton event occured " + System.currentTimeMillis()%10000000 );
Excitation16kHz();
} //_CODE_:button16kHz:508751:

public void button32kHz_click2(GButton source, GEvent event) { //_CODE_:button32kHz:267911:
//  println("button32kHz - GButton event occured " + System.currentTimeMillis()%10000000 );
Excitation32kHz();
} //_CODE_:button32kHz:267911:

public void buttonVdd8_click2(GButton source, GEvent event) { //_CODE_:buttonVdd8:960809:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Vdd8();
} //_CODE_:buttonVdd8:960809:

public void buttonVdd4_click2(GButton source, GEvent event) { //_CODE_:buttonVdd4:500688:
//  println("button1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Vdd4();
} //_CODE_:buttonVdd4:500688:

public void buttonVddx38_click2(GButton source, GEvent event) { //_CODE_:buttonVddx38:944936:
//  println("buttonVddx38 - GButton event occured " + System.currentTimeMillis()%10000000 );
Vddx38();
} //_CODE_:buttonVddx38:944936:

public void buttonVdd2_click2(GButton source, GEvent event) { //_CODE_:buttonVdd2:629822:
//  println("buttonVdd2 - GButton event occured " + System.currentTimeMillis()%10000000 );
Vdd2();
} //_CODE_:buttonVdd2:629822:

public void buttonCapChop_click2(GButton source, GEvent event) { //_CODE_:buttonCapChop:961003:
//  println("buttonCapChop - GButton event occured " + System.currentTimeMillis()%10000000 );
CapChop();
} //_CODE_:buttonCapChop:961003:

public void button1_click2(GButton source, GEvent event) { //_CODE_:buttonNormal:944383:
//  println("buttonNormal - GButton event occured " + System.currentTimeMillis()%10000000 );
NormalOperation();
} //_CODE_:buttonNormal:944383:

public void button1_click3(GButton source, GEvent event) { //_CODE_:buttonVT_ON:795627:
//  println("buttonVT_ON - GButton event occured " + System.currentTimeMillis()%10000000 );
//if (TempCompensation == false){
VT_on();
TempCompensation = true;
//}
} //_CODE_:buttonVT_ON:795627:

public void buttonVT_OFF_click3(GButton source, GEvent event) { //_CODE_:buttonVT_OFF:874859:
//  println("buttonVT_OFF - GButton event occured " + System.currentTimeMillis()%10000000 );
//if (TempCompensation == true){
VT_off();
TempCompensation = false;
//}
} //_CODE_:buttonVT_OFF:874859:

synchronized public void Colorimeterwin_draw1(GWinApplet appc, GWinData data) { //_CODE_:window5:632533:
  appc.background(230);
} //_CODE_:window5:632533:

public void REDbutton1_click4(GButton source, GEvent event) { //_CODE_:REDbutton1:498322:
//  println("REDbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Red_Led();
} //_CODE_:REDbutton1:498322:

public void GREENbutton2_click1(GButton source, GEvent event) { //_CODE_:GREENbutton2:876667:
//  println("GREENbutton2 - GButton event occured " + System.currentTimeMillis()%10000000 );
Green_Led();
} //_CODE_:GREENbutton2:876667:

public void BLUEbutton3_click1(GButton source, GEvent event) { //_CODE_:BLUEbutton3:723190:
//  println("BLUEbutton3 - GButton event occured " + System.currentTimeMillis()%10000000 );
Blue_Led();
} //_CODE_:BLUEbutton3:723190:

public void ZERObutton1_click4(GButton source, GEvent event) { //_CODE_:ZERObutton1:303635:
//  println("ZERObutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Zero_color();
} //_CODE_:ZERObutton1:303635:

public void Clearbutton1_click4(GButton source, GEvent event) { //_CODE_:Clearbutton1:609341:
//  println("Clearbutton1 - GButton event occured " + System.currentTimeMillis()%10000000 );
Clear_Filter();
} //_CODE_:Clearbutton1:609341:



// Create all the GUI controls. 
// autogenerated do not edit
public void createGUI(){
  G4P.messagesEnabled(false);
  G4P.setGlobalColorScheme(GCScheme.BLUE_SCHEME);
  G4P.setCursor(ARROW);
  if(frame != null)
    frame.setTitle("Universal Acquisition 0.2");
  window1 = new GWindow(this, "Serial Port Setup", 300, 0, 520, 265, false, JAVA2D);
  window1.setActionOnClose(G4P.CLOSE_WINDOW);
  window1.addDrawHandler(this, "Setupwin_draw1");
  OpenSerialbutton = new GButton(window1.papplet, 6, 6, 80, 30);
  OpenSerialbutton.setText("Open Serial Port1");
  OpenSerialbutton.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OpenSerialbutton.addEventHandler(this, "OpenSerialbutton1_click1");
  Serial_Ports = new GDropList(window1.papplet, 6, 42, 165, 220, 10);
  Serial_Ports.setItems(loadStrings("list_605734"), 0);
  Serial_Ports.addEventHandler(this, "Serial_PortsdropList1_click1");
  StopSerial = new GButton(window1.papplet, 90, 6, 80, 30);
  StopSerial.setText("STOP Serial Port");
  StopSerial.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  StopSerial.addEventHandler(this, "StopSerialbutton1_click3");
  BaudRate = new GDropList(window1.papplet, 174, 42, 165, 220, 10);
  BaudRate.setItems(loadStrings("list_868333"), 0);
  BaudRate.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  BaudRate.addEventHandler(this, "BaudRate_click1");
  Baudlabel1 = new GLabel(window1.papplet, 174, 6, 165, 30);
  Baudlabel1.setText("Baud Rate");
  Baudlabel1.setTextBold();
  Baudlabel1.setOpaque(false);
  Phrasetextfield1 = new GTextField(window1.papplet, 342, 42, 165, 22, G4P.SCROLLBARS_NONE);
  Phrasetextfield1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  Phrasetextfield1.setOpaque(false);
  Phrasetextfield1.addEventHandler(this, "Phrasetextfield1_change1");
  Initiationlabel1 = new GLabel(window1.papplet, 342, 6, 120, 30);
  Initiationlabel1.setText("Start Phrase");
  Initiationlabel1.setTextBold();
  Initiationlabel1.setOpaque(false);
  OKPhrasebutton1 = new GButton(window1.papplet, 468, 6, 40, 30);
  OKPhrasebutton1.setText("OK");
  OKPhrasebutton1.setTextBold();
  OKPhrasebutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  OKPhrasebutton1.addEventHandler(this, "OKPhrasebutton1_click2");
  window2 = new GWindow(this, "Controls", 0, 0, 375, 170, false, JAVA2D);
  window2.addDrawHandler(this, "win_draw2");
  ZoomIn = new GButton(window2.papplet, 5, 5, 80, 30);
  ZoomIn.setText("Zoom in");
  ZoomIn.setTextBold();
  ZoomIn.addEventHandler(this, "ZoomInbutton1_click1");
  ZoomOut = new GButton(window2.papplet, 5, 40, 80, 30);
  ZoomOut.setText("Zoom out");
  ZoomOut.setTextBold();
  ZoomOut.addEventHandler(this, "ZoomOutbutton2_click1");
  TimePlus = new GButton(window2.papplet, 90, 5, 80, 30);
  TimePlus.setText("Time +");
  TimePlus.setTextBold();
  TimePlus.addEventHandler(this, "TimePlusbutton3_click1");
  TimeMinus = new GButton(window2.papplet, 90, 40, 80, 30);
  TimeMinus.setText("Time -");
  TimeMinus.setTextBold();
  TimeMinus.addEventHandler(this, "TimeMinusbutton4_click1");
  RUN = new GButton(window2.papplet, 180, 5, 39, 30);
  RUN.setText("RUN");
  RUN.setTextBold();
  RUN.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  RUN.addEventHandler(this, "RUNbutton5_click1");
  STOP = new GButton(window2.papplet, 180, 40, 39, 30);
  STOP.setText("STOP");
  STOP.setLocalColorScheme(GCScheme.RED_SCHEME);
  STOP.addEventHandler(this, "STOPbutton6_click1");
  OffsetUp = new GButton(window2.papplet, 5, 75, 60, 30);
  OffsetUp.setText("Offset up");
  OffsetUp.setTextBold();
  OffsetUp.addEventHandler(this, "button1_click1");
  OffsetDown = new GButton(window2.papplet, 90, 75, 60, 30);
  OffsetDown.setText("Offset down");
  OffsetDown.setTextBold();
  OffsetDown.addEventHandler(this, "OffsetDownbutton1_click1");
  Timingslider1 = new GSlider(window2.papplet, 0, 145, 265, 15, 10.0f);
  Timingslider1.setLimits(0.0f, 0.0f, 4000.0f);
  Timingslider1.setNumberFormat(G4P.DECIMAL, 1);
  Timingslider1.setOpaque(false);
  Timingslider1.addEventHandler(this, "Timingslider1_change1");
  Offsetslider2 = new GSlider(window2.papplet, 279, 0, 160, 15, 10.0f);
  Offsetslider2.setRotation(PI/2, GControlMode.CORNER);
  Offsetslider2.setLimits(0.0f, -2000.0f, 2000.0f);
  Offsetslider2.setNumberFormat(G4P.DECIMAL, 2);
  Offsetslider2.setOpaque(false);
  Offsetslider2.addEventHandler(this, "Offsetslider2_change1");
  Settingsbutton1 = new GButton(window2.papplet, 180, 75, 80, 30);
  Settingsbutton1.setText("Settings");
  Settingsbutton1.setTextBold();
  Settingsbutton1.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  Settingsbutton1.addEventHandler(this, "Settingsbutton1_click2");
  EXITbutton1 = new GButton(window2.papplet, 180, 110, 80, 30);
  EXITbutton1.setText("EXIT");
  EXITbutton1.setTextBold();
  EXITbutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  EXITbutton1.addEventHandler(this, "EXITbutton1_click2");
  ChoosePathbutton1 = new GButton(window2.papplet, 282, 132, 80, 30);
  ChoosePathbutton1.setText("Choose Path");
  ChoosePathbutton1.setTextBold();
  ChoosePathbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ChoosePathbutton1.addEventHandler(this, "ChoosePathbutton1_click2");
  AnalysisUPbutton1 = new GButton(window2.papplet, 5, 110, 15, 15);
  AnalysisUPbutton1.setText("\u2191");
  AnalysisUPbutton1.setTextBold();
  AnalysisUPbutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  AnalysisUPbutton1.addEventHandler(this, "AnalysisUP_click2");
  AnalysisDownbutton1 = new GButton(window2.papplet, 47, 110, 15, 15);
  AnalysisDownbutton1.setText("\u2193");
  AnalysisDownbutton1.setTextBold();
  AnalysisDownbutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  AnalysisDownbutton1.addEventHandler(this, "AnalysisDown_click2");
  Analysisslider1 = new GSlider(window2.papplet, 0, 128, 89, 15, 10.0f);
  Analysisslider1.setLimits(0.0f, 0.0f, 100.0f);
  Analysisslider1.setNumberFormat(G4P.DECIMAL, 0);
  Analysisslider1.setOpaque(false);
  Analysisslider1.addEventHandler(this, "Analysisslider1_change1");
  buttonCEControls = new GButton(window2.papplet, 282, 24, 80, 20);
  buttonCEControls.setText("CE Controls");
  buttonCEControls.setTextBold();
  buttonCEControls.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonCEControls.addEventHandler(this, "buttonCEControls_click3");
  buttonCDCControls = new GButton(window2.papplet, 282, 48, 80, 20);
  buttonCDCControls.setText("CDC Controls");
  buttonCDCControls.setTextBold();
  buttonCDCControls.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonCDCControls.addEventHandler(this, "buttonCDCControls_click1");
  buttonAutoZero = new GButton(window2.papplet, 282, 72, 80, 20);
  buttonAutoZero.setText("Auto Zero");
  buttonAutoZero.setTextBold();
  buttonAutoZero.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  buttonAutoZero.addEventHandler(this, "buttonAutoZero_click3");
  buttonAutoZeroDel = new GButton(window2.papplet, 282, 96, 80, 30);
  buttonAutoZeroDel.setText("Auto Zero Delete");
  buttonAutoZeroDel.setTextBold();
  buttonAutoZeroDel.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  buttonAutoZeroDel.addEventHandler(this, "buttonAutoZeroDel_click3");
  ZeroTimebutton1 = new GButton(window2.papplet, 221, 40, 39, 30);
  ZeroTimebutton1.setText("Zero Time");
  ZeroTimebutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  ZeroTimebutton1.addEventHandler(this, "ZeroTimebutton1_click4");
  AutoRun_button1 = new GButton(window2.papplet, 221, 5, 39, 30);
  AutoRun_button1.setText("Auto Run");
  AutoRun_button1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  AutoRun_button1.addEventHandler(this, "AutoRun_button1_click4");
  OffUpSmall_button1 = new GButton(window2.papplet, 70, 75, 15, 30);
  OffUpSmall_button1.setText("\u2191");
  OffUpSmall_button1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OffUpSmall_button1.addEventHandler(this, "OffUpSmall_button1_click4");
  OffDownSmall_button1 = new GButton(window2.papplet, 155, 75, 15, 30);
  OffDownSmall_button1.setText("\u2193");
  OffDownSmall_button1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  OffDownSmall_button1.addEventHandler(this, "OffDownSmall_button1_click5");
  ATimePlusTenbutton1 = new GButton(window2.papplet, 21, 110, 24, 15);
  ATimePlusTenbutton1.setText("10\u2191");
  ATimePlusTenbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ATimePlusTenbutton1.addEventHandler(this, "ATimePlusTenbutton1_click4");
  ATimeminusTenbutton1 = new GButton(window2.papplet, 63, 110, 24, 15);
  ATimeminusTenbutton1.setText("10\u2193");
  ATimeminusTenbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  ATimeminusTenbutton1.addEventHandler(this, "ATimeminusTenbutton1_click4");
  TimeAnalysistextfield1 = new GTextField(window2.papplet, 90, 110, 50, 30, G4P.SCROLLBARS_NONE);
  TimeAnalysistextfield1.setOpaque(true);
  TimeAnalysistextfield1.addEventHandler(this, "textfield1_change1");
  Selecttimebutton1 = new GButton(window2.papplet, 140, 110, 30, 30);
  Selecttimebutton1.setText("OK");
  Selecttimebutton1.setTextBold();
  Selecttimebutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  Selecttimebutton1.addEventHandler(this, "Selecttimebutton1_click4");
  COLORIMETERbutton1 = new GButton(window2.papplet, 282, 0, 80, 20);
  COLORIMETERbutton1.setText("Colorimeter");
  COLORIMETERbutton1.setTextBold();
  COLORIMETERbutton1.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  COLORIMETERbutton1.addEventHandler(this, "COLORIMETERbutton1_click4");
  window3 = new GWindow(this, "CE Controls", 0, 200, 285, 300, false, JAVA2D);
  window3.setActionOnClose(G4P.CLOSE_WINDOW);
  window3.addDrawHandler(this, "win_draw3");
  Lift_UP = new GButton(window3.papplet, 0, 20, 80, 30);
  Lift_UP.setText("Lift Up");
  Lift_UP.setTextBold();
  Lift_UP.addEventHandler(this, "LiftUP_click1");
  Lift_DOWN = new GButton(window3.papplet, 0, 60, 80, 30);
  Lift_DOWN.setText("Lift Down");
  Lift_DOWN.setTextBold();
  Lift_DOWN.addEventHandler(this, "LiftDown_click1");
  HVbutton1 = new GButton(window3.papplet, 90, 20, 80, 30);
  HVbutton1.setText("HV ON");
  HVbutton1.setTextBold();
  HVbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  HVbutton1.addEventHandler(this, "HVbutton1_click1");
  HVOFFbutton1 = new GButton(window3.papplet, 90, 60, 80, 30);
  HVOFFbutton1.setText("HV OFF");
  HVOFFbutton1.setTextBold();
  HVOFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  HVOFFbutton1.addEventHandler(this, "HVOFFbutton1_click1");
  Injectionlabel1 = new GLabel(window3.papplet, 172, 0, 80, 20);
  Injectionlabel1.setText("Injection");
  Injectionlabel1.setTextBold();
  Injectionlabel1.setOpaque(false);
  InjectiondropList1 = new GDropList(window3.papplet, 180, 20, 90, 200, 10);
  InjectiondropList1.setItems(loadStrings("list_296461"), 0);
  InjectiondropList1.addEventHandler(this, "InjectiondropList1_click1");
  Injectbutton1 = new GButton(window3.papplet, 180, 60, 80, 30);
  Injectbutton1.setText("Set Injection");
  Injectbutton1.setTextBold();
  Injectbutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  Injectbutton1.addEventHandler(this, "Injectbutton1_click2");
  Lifterlabel1 = new GLabel(window3.papplet, 0, 0, 80, 20);
  Lifterlabel1.setText("Lifter");
  Lifterlabel1.setTextBold();
  Lifterlabel1.setOpaque(false);
  Voltagelabel1 = new GLabel(window3.papplet, 90, 0, 80, 20);
  Voltagelabel1.setText("Voltage");
  Voltagelabel1.setTextBold();
  Voltagelabel1.setOpaque(false);
  Carousellabel1 = new GLabel(window3.papplet, 0, 100, 80, 20);
  Carousellabel1.setText("Carousel");
  Carousellabel1.setTextBold();
  Carousellabel1.setOpaque(false);
  VialClockbutton1 = new GButton(window3.papplet, 0, 120, 80, 30);
  VialClockbutton1.setText("Vial \u2192");
  VialClockbutton1.addEventHandler(this, "VialClockbutton1_click4");
  VialCounterbutton2 = new GButton(window3.papplet, 0, 160, 80, 30);
  VialCounterbutton2.setText("Vial \u2190");
  VialCounterbutton2.addEventHandler(this, "VialCounterbutton2_click1");
  AdjustClockbutton1 = new GButton(window3.papplet, 90, 120, 80, 30);
  AdjustClockbutton1.setText("Adjust \u2192");
  AdjustClockbutton1.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  AdjustClockbutton1.addEventHandler(this, "AdjustClockbutton1_click4");
  AdjustCounterbutton2 = new GButton(window3.papplet, 90, 160, 80, 30);
  AdjustCounterbutton2.setText("Adjust \u2190");
  AdjustCounterbutton2.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  AdjustCounterbutton2.addEventHandler(this, "AdjustCounterbutton2_click1");
  StartAnalysisbutton1 = new GButton(window3.papplet, 180, 120, 80, 30);
  StartAnalysisbutton1.setText("START ANALYSIS");
  StartAnalysisbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  StartAnalysisbutton1.addEventHandler(this, "StaretAnalysisbutton1_click4");
  StopAnalysisbutton2 = new GButton(window3.papplet, 180, 160, 80, 30);
  StopAnalysisbutton2.setText("STOP ANALYSIS");
  StopAnalysisbutton2.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  StopAnalysisbutton2.addEventHandler(this, "StopAnalysisbutton2_click1");
  InjectMethodlabel1 = new GLabel(window3.papplet, 0, 190, 170, 20);
  InjectMethodlabel1.setText("Injection Method");
  InjectMethodlabel1.setTextBold();
  InjectMethodlabel1.setOpaque(false);
  ElectrokineticInjbutton1 = new GButton(window3.papplet, 0, 210, 80, 30);
  ElectrokineticInjbutton1.setText("Electrokinetic");
  ElectrokineticInjbutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  ElectrokineticInjbutton1.addEventHandler(this, "ElectrokineticInbutton1_click4");
  Hydrodynamicbutton2 = new GButton(window3.papplet, 90, 210, 80, 30);
  Hydrodynamicbutton2.setText("Hydrodynamic");
  Hydrodynamicbutton2.setLocalColorScheme(GCScheme.PURPLE_SCHEME);
  Hydrodynamicbutton2.addEventHandler(this, "Hydrodynamicbutton2_click1");
  Pump_label1 = new GLabel(window3.papplet, 0, 240, 80, 20);
  Pump_label1.setText("Pump");
  Pump_label1.setTextBold();
  Pump_label1.setOpaque(false);
  PONbutton1 = new GButton(window3.papplet, 0, 260, 40, 30);
  PONbutton1.setText("ON");
  PONbutton1.setTextBold();
  PONbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  PONbutton1.addEventHandler(this, "PONbutton1_click4");
  POFFbutton1 = new GButton(window3.papplet, 50, 260, 40, 30);
  POFFbutton1.setText("OFF");
  POFFbutton1.setTextBold();
  POFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  POFFbutton1.addEventHandler(this, "POFFbutton1_click4");
  Valveslabel1 = new GLabel(window3.papplet, 130, 240, 80, 20);
  Valveslabel1.setText("Valves");
  Valveslabel1.setTextBold();
  Valveslabel1.setOpaque(false);
  VONbutton1 = new GButton(window3.papplet, 120, 260, 40, 30);
  VONbutton1.setText("ON");
  VONbutton1.setTextBold();
  VONbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  VONbutton1.addEventHandler(this, "VONbutton1_click4");
  VOFFbutton1 = new GButton(window3.papplet, 170, 260, 40, 30);
  VOFFbutton1.setText("OFF");
  VOFFbutton1.setTextBold();
  VOFFbutton1.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  VOFFbutton1.addEventHandler(this, "VOFFbutton1_click4");
  window4 = new GWindow(this, "CDC Functions", 0, 350, 370, 160, false, JAVA2D);
  window4.setActionOnClose(G4P.CLOSE_WINDOW);
  window4.addDrawHandler(this, "CDCwin_draw1");
  button9Hz = new GButton(window4.papplet, 5, 5, 40, 30);
  button9Hz.setText("9Hz");
  button9Hz.setTextBold();
  button9Hz.addEventHandler(this, "button9Hz_click");
  button26Hz = new GButton(window4.papplet, 185, 5, 40, 30);
  button26Hz.setText("26Hz");
  button26Hz.setTextBold();
  button26Hz.addEventHandler(this, "button26Hz_click1");
  button91Hz = new GButton(window4.papplet, 320, 5, 40, 30);
  button91Hz.setText("91Hz");
  button91Hz.setTextBold();
  button91Hz.addEventHandler(this, "button91Hz_click1");
  button11Hz = new GButton(window4.papplet, 50, 5, 40, 30);
  button11Hz.setText("11Hz");
  button11Hz.setTextBold();
  button11Hz.addEventHandler(this, "button11Hz_click1");
  button13Hz = new GButton(window4.papplet, 95, 5, 40, 30);
  button13Hz.setText("13Hz");
  button13Hz.setTextBold();
  button13Hz.addEventHandler(this, "button13Hz_click2");
  button16Hz = new GButton(window4.papplet, 140, 5, 40, 30);
  button16Hz.setText("16Hz");
  button16Hz.setTextBold();
  button16Hz.addEventHandler(this, "button16Hz_click2");
  button50Hz = new GButton(window4.papplet, 230, 5, 40, 30);
  button50Hz.setText("50Hz");
  button50Hz.setTextBold();
  button50Hz.addEventHandler(this, "button50Hz_click2");
  button84Hz = new GButton(window4.papplet, 275, 5, 40, 30);
  button84Hz.setText("84Hz");
  button84Hz.setTextBold();
  button84Hz.addEventHandler(this, "button84Hz_click2");
  button16kHz = new GButton(window4.papplet, 5, 40, 80, 30);
  button16kHz.setText("16kHz");
  button16kHz.setTextBold();
  button16kHz.setLocalColorScheme(GCScheme.RED_SCHEME);
  button16kHz.addEventHandler(this, "button16kHz_click2");
  button32kHz = new GButton(window4.papplet, 90, 40, 80, 30);
  button32kHz.setText("32kHz");
  button32kHz.setTextBold();
  button32kHz.setLocalColorScheme(GCScheme.RED_SCHEME);
  button32kHz.addEventHandler(this, "button32kHz_click2");
  buttonVdd8 = new GButton(window4.papplet, 5, 75, 50, 30);
  buttonVdd8.setText("Vdd/8");
  buttonVdd8.setTextBold();
  buttonVdd8.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVdd8.addEventHandler(this, "buttonVdd8_click2");
  buttonVdd4 = new GButton(window4.papplet, 60, 75, 50, 30);
  buttonVdd4.setText("Vdd/4");
  buttonVdd4.setTextBold();
  buttonVdd4.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVdd4.addEventHandler(this, "buttonVdd4_click2");
  buttonVddx38 = new GButton(window4.papplet, 115, 75, 50, 30);
  buttonVddx38.setText("Vddx3/8");
  buttonVddx38.setTextBold();
  buttonVddx38.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVddx38.addEventHandler(this, "buttonVddx38_click2");
  buttonVdd2 = new GButton(window4.papplet, 170, 75, 50, 30);
  buttonVdd2.setText("Vdd/2");
  buttonVdd2.setTextBold();
  buttonVdd2.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  buttonVdd2.addEventHandler(this, "buttonVdd2_click2");
  buttonCapChop = new GButton(window4.papplet, 175, 40, 80, 30);
  buttonCapChop.setText("CapChop");
  buttonCapChop.setTextBold();
  buttonCapChop.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  buttonCapChop.addEventHandler(this, "buttonCapChop_click2");
  buttonNormal = new GButton(window4.papplet, 260, 40, 80, 30);
  buttonNormal.setText("Normal");
  buttonNormal.setTextBold();
  buttonNormal.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  buttonNormal.addEventHandler(this, "button1_click2");
  buttonVT_ON = new GButton(window4.papplet, 5, 110, 50, 30);
  buttonVT_ON.setText("VT ON");
  buttonVT_ON.setTextBold();
  buttonVT_ON.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonVT_ON.addEventHandler(this, "button1_click3");
  buttonVT_OFF = new GButton(window4.papplet, 60, 110, 50, 30);
  buttonVT_OFF.setText("VT OFF");
  buttonVT_OFF.setTextBold();
  buttonVT_OFF.setLocalColorScheme(GCScheme.GOLD_SCHEME);
  buttonVT_OFF.addEventHandler(this, "buttonVT_OFF_click3");
  window5 = new GWindow(this, "Colorimeter", 300, 0, 185, 120, false, P2D);
  window5.setActionOnClose(G4P.CLOSE_WINDOW);
  window5.addDrawHandler(this, "Colorimeterwin_draw1");
  REDbutton1 = new GButton(window5.papplet, 6, 6, 80, 30);
  REDbutton1.setText("RED");
  REDbutton1.setLocalColorScheme(GCScheme.RED_SCHEME);
  REDbutton1.addEventHandler(this, "REDbutton1_click4");
  GREENbutton2 = new GButton(window5.papplet, 6, 42, 80, 30);
  GREENbutton2.setText("GREEN");
  GREENbutton2.setLocalColorScheme(GCScheme.GREEN_SCHEME);
  GREENbutton2.addEventHandler(this, "GREENbutton2_click1");
  BLUEbutton3 = new GButton(window5.papplet, 6, 78, 80, 30);
  BLUEbutton3.setText("BLUE");
  BLUEbutton3.addEventHandler(this, "BLUEbutton3_click1");
  ZERObutton1 = new GButton(window5.papplet, 96, 6, 80, 30);
  ZERObutton1.setText("ZERO");
  ZERObutton1.setLocalColorScheme(GCScheme.CYAN_SCHEME);
  ZERObutton1.addEventHandler(this, "ZERObutton1_click4");
  Clearbutton1 = new GButton(window5.papplet, 96, 42, 80, 30);
  Clearbutton1.setText("CLEAR");
  Clearbutton1.setLocalColorScheme(GCScheme.YELLOW_SCHEME);
  Clearbutton1.addEventHandler(this, "Clearbutton1_click4");
}

// Variable declarations 
// autogenerated do not edit
GWindow window1;
GButton OpenSerialbutton; 
GDropList Serial_Ports; 
GButton StopSerial; 
GDropList BaudRate; 
GLabel Baudlabel1; 
GTextField Phrasetextfield1; 
GLabel Initiationlabel1; 
GButton OKPhrasebutton1; 
GWindow window2;
GButton ZoomIn; 
GButton ZoomOut; 
GButton TimePlus; 
GButton TimeMinus; 
GButton RUN; 
GButton STOP; 
GButton OffsetUp; 
GButton OffsetDown; 
GSlider Timingslider1; 
GSlider Offsetslider2; 
GButton Settingsbutton1; 
GButton EXITbutton1; 
GButton ChoosePathbutton1; 
GButton AnalysisUPbutton1; 
GButton AnalysisDownbutton1; 
GSlider Analysisslider1; 
GButton buttonCEControls; 
GButton buttonCDCControls; 
GButton buttonAutoZero; 
GButton buttonAutoZeroDel; 
GButton ZeroTimebutton1; 
GButton AutoRun_button1; 
GButton OffUpSmall_button1; 
GButton OffDownSmall_button1; 
GButton ATimePlusTenbutton1; 
GButton ATimeminusTenbutton1; 
GTextField TimeAnalysistextfield1; 
GButton Selecttimebutton1; 
GButton COLORIMETERbutton1; 
GWindow window3;
GButton Lift_UP; 
GButton Lift_DOWN; 
GButton HVbutton1; 
GButton HVOFFbutton1; 
GLabel Injectionlabel1; 
GDropList InjectiondropList1; 
GButton Injectbutton1; 
GLabel Lifterlabel1; 
GLabel Voltagelabel1; 
GLabel Carousellabel1; 
GButton VialClockbutton1; 
GButton VialCounterbutton2; 
GButton AdjustClockbutton1; 
GButton AdjustCounterbutton2; 
GButton StartAnalysisbutton1; 
GButton StopAnalysisbutton2; 
GLabel InjectMethodlabel1; 
GButton ElectrokineticInjbutton1; 
GButton Hydrodynamicbutton2; 
GLabel Pump_label1; 
GButton PONbutton1; 
GButton POFFbutton1; 
GLabel Valveslabel1; 
GButton VONbutton1; 
GButton VOFFbutton1; 
GWindow window4;
GButton button9Hz; 
GButton button26Hz; 
GButton button91Hz; 
GButton button11Hz; 
GButton button13Hz; 
GButton button16Hz; 
GButton button50Hz; 
GButton button84Hz; 
GButton button16kHz; 
GButton button32kHz; 
GButton buttonVdd8; 
GButton buttonVdd4; 
GButton buttonVddx38; 
GButton buttonVdd2; 
GButton buttonCapChop; 
GButton buttonNormal; 
GButton buttonVT_ON; 
GButton buttonVT_OFF; 
GWindow window5;
GButton REDbutton1; 
GButton GREENbutton2; 
GButton BLUEbutton3; 
GButton ZERObutton1; 
GButton Clearbutton1; 

public float getCap(JSONObject json) {
  if (json.isNull("c") == true) {
    return 0.0f;
  } else
    return json.getFloat("c");
}

public float getTemp(JSONObject json) {
  if (json.isNull("t") == true) {
    return 20.0f;
  } else
    return json.getFloat("t");
}

public float getCurrent(JSONObject json) {
  if (json.isNull("i") == true) {
    return 0.0f;
  } else
    return json.getFloat("i");
}

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "CECUBE2_GUI_JSON_processing" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
