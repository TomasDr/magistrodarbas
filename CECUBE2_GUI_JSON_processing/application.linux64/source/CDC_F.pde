void Hz9() {
  myPort.write("9Hz*");
}

void Hz11() {
  myPort.write("11Hz*");
}

void Hz13() {
  myPort.write("13Hz*");
}

void Hz16() {
  myPort.write("16Hz*");
}

void Hz26() {
  myPort.write("26Hz*");
}

void Hz50() {
  myPort.write("50Hz*");
}

void Hz84() {
  myPort.write("84Hz*");
}

void Hz91() {
  myPort.write("91Hz*");
}

void Excitation16kHz(){
  myPort.write("16kHz*");
}

void Excitation32kHz(){
  myPort.write("32kHz*");
}

void Vdd8(){
  myPort.write("Vdd/8*");
}

void Vdd4(){
  myPort.write("Vdd/4*");
}

void Vddx38(){
  myPort.write("Vddx3/8*");
}
void Vdd2(){
  myPort.write("Vdd/2*");
}

void CapChop(){
    myPort.write("CAPCHOP1*");
    myPort.clear();
}

void NormalOperation(){
    myPort.write("NormalOper*");
    myPort.clear();
}

void VT_on(){
    myPort.write("TemperON*");
    myPort.clear();
    delay(200);
}

void VT_off(){
    myPort.write("TemperOFF*");
    myPort.clear();
//    delay(100);
}

void Auto_Zero(){
      myPort.write("Auto Zero*");
      myPort.clear();
}

void Auto_Zero_Delete(){
      myPort.write("Zero Delete*");
      myPort.clear();
}
