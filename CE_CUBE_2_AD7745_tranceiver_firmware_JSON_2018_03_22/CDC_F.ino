void setupCDC(){
    //C4D detector settings
  Wire.begin();

  Configuration();

  Excitation();

  CapInput();

  CapDacARegister();
  CapOffsetHighAdjust();
  CapOffsetLowAdjust();
  // addressRead();

  continuous();
  VT_on();
  CapGainHighAdjust();
  CapGainLowAdjust();
}

void Excitation16kHz(){
  Wire.beginTransmission(Address);
  Wire.write(ExcSetup);
  Wire.write(B11100011);
  Wire.endTransmission();
}

void Excitation32kHz(){
  Wire.beginTransmission(Address);
  Wire.write(ExcSetup);
  Wire.write(B01100011);
  Wire.endTransmission();
}

void Vdd8(){
  Wire.beginTransmission(Address);
  Wire.write(ExcSetup);
  Wire.write(B01100000);
  Wire.endTransmission();
}

void Vdd4(){
  Wire.beginTransmission(Address);
  Wire.write(ExcSetup);
  Wire.write(B01100001);
  Wire.endTransmission();
}

void Vddx38(){
  Wire.beginTransmission(Address);
  Wire.write(ExcSetup);
  Wire.write(B01100010);
  Wire.endTransmission();
}

void Vdd2(){
  Wire.beginTransmission(Address);
  Wire.write(ExcSetup);
  Wire.write(B01100011);
  Wire.endTransmission();
}

void CapChop(){
  Wire.beginTransmission(Address);
  Wire.write(CapSetup);
  Wire.write(B10100001);
  Wire.endTransmission();
}

void NormalOperation(){
  Wire.beginTransmission(Address);
  Wire.write(CapSetup);
  Wire.write(B10100000);
  Wire.endTransmission();
}

void VT_on(){
  Wire.beginTransmission(Address);
  Wire.write(VTSetup);
  Wire.write(B10000001);
  Wire.endTransmission(); 
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B11111001);
  Wire.endTransmission();
  
  temperatureCompensation = true;
}

void VT_off(){
  Wire.beginTransmission(Address);
  Wire.write(VTSetup);
  Wire.write(B00000000);
  Wire.endTransmission(); 
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00111001);
  Wire.endTransmission();
  
  temperatureCompensation = false;
}

void Auto_ZeroF(){
  zeroCompensation = capacitance;
}

void Auto_Zero_Del(){
  zeroCompensation = 0;
}
