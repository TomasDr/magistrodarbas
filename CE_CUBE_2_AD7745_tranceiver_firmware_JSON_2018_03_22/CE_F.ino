void CEfunctions() {
  //  LiftUp();
  //  LiftDown();
  //  HVON();
  //  HVOFF();
  //  Electro();
  //  Siphoning();
  //  carouselClock();
  //  carouselCounter();
  //  adjustClock();
  //  adjustCounter();
}

void LiftUp() {
  //  if (LiftPlatform == true){
  LiftPlatform = true;
  servoCurrentTime = millis();
  myservo.attach(servoPin);
  myservo.write(5);

}

void LiftDown() {
  //  if (LiftPlatformDown == true){
  //    Serial.println("liftingggg");
  LiftPlatform = true;
  servoCurrentTime = millis();
  myservo.attach(servoPin);
  myservo.write(85);

}

void servoOff() {
  if (LiftPlatform == true && millis() > servoCurrentTime + servoWaitingTime) {
    myservo.detach();
    LiftPlatform = false;
    cycleTask++;
  }
}

void HVON() {
  //  if (SwitchHVon == true) {
  digitalWrite(HVpin, LOW);
  //    cycleTask++;
  //    SwitchHVon = false;
  //  }
}

void HVOFF() {
  //  if (SwitchHVoff == true) {
  digitalWrite(HVpin, HIGH);
  //    cycleTask++;
  //    SwitchHVoff = false;
  //  }
}

void setPumpON() {
  digitalWrite(pumpPin, LOW);
  //pressureInicator = false;
  //pressureActionStatus = true;
}

void setPumpOFF() {
  setValvesON();
  digitalWrite(pumpPin, HIGH);
  delay(100);
  setValvesOFF();
  delay(100);
  setValvesON();
  delay(100);
  setValvesOFF();
  delay(100);
  setValvesON();
  delay(100);
  setValvesOFF();
}

void setValvesON() {
  digitalWrite(valvesPin, LOW);
}

void setValvesOFF() {
  digitalWrite(valvesPin, HIGH);
}

void carouselClock() {
  //  if (carouselClockFlag == true){
  carouselFlag = true;
  stepFlag = vialResolution;
  Direction = false;
  //    carouselClockFlag = false;
  //  }
}

void carouselCounter() {
  //  if (carouselCounterFlag == true){
  carouselFlag = true;
  stepFlag = vialResolution;
  Direction = true;
  //    carouselCounterFlag = false;
  //  }
}

void adjustClock() {
  //  if (adjustClockFlag == true){
  carouselFlag = true;
  stepFlag = vialResolution / 5;
  Direction = false;
  //    adjustClockFlag = false;
  //  }
}

void adjustCounter() {
  //  if (adjustCounterFlag == true){
  carouselFlag = true;
  stepFlag = vialResolution / 5;
  Direction = true;
  //    adjustCounterFlag = false;
  //  }
}

void setHydrodynamicInjection() {
  injectionMethod = true;
}

void setElectrokineticInjection() {
  injectionMethod = false;
}

void setInj5() {
  injectionTime = 5 * 1000;
}

void setInj10() {
  injectionTime = 10 * 1000;
}

void setInj20() {
  injectionTime = 20 * 1000;
}

void setInj40() {
  injectionTime = 40 * 1000;
}

void setInj80() {
  injectionTime = 80 * 1000;
}

void setInj160() {
  injectionTime = 160 * 1000;
}

void setInj320() {
  injectionTime = 320 * 1000;
}

void setInj640() {
  injectionTime = 640 * 1000;
}
