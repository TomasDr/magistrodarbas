void PIDinit() {
  windowStartTimePre = millis();
  InputPre = bmp.readPressure();
  SetpointPre = 80000.0;
  //turn the PID on
  myPIDPre.SetOutputLimits(0, WindowSizePre);
  myPIDPre.SetMode(AUTOMATIC);
}

void regulatePressure(){
  InputPre = bmp.readPressure();
//  Serial.println(InputPre);
  myPIDPre.Compute();
  /************************************************
   * turn the output pin on/off based on pid output
   ************************************************/
  if(millis() - windowStartTimePre>WindowSizePre) //Temperature PID
  { //time to shift the Relay Window
    windowStartTimePre += WindowSizePre;
//    Serial.println("PIDas1");
  }
  if(OutputPre < millis() - windowStartTimePre){
  digitalWrite(pumpPin,HIGH);
  }
  else {
  digitalWrite(pumpPin,LOW);
  }
}
