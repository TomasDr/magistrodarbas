void setupNRF() {
  printf_begin();
  radio.begin();

  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(64);

  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setCRCLength(RF24_CRC_16);

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);

  radio.startListening();
  radio.printDetails();

//  Serial.println();
//  Serial.println("RF Chat V0.90");
  delay(500);
}

void sendvalues(void) {
  char SendPayload[32] = "";
  strcat(SendPayload, valString);
  // swap TX & Rx addr for writing
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(0, pipes[0]);
  radio.stopListening();
  bool ok = radio.write(&SendPayload, strlen(SendPayload));

  //delay(100);
  // restore TX & Rx addr for reading
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);
  radio.startListening();
  SendPayload[0] = 0;
  dataBufferIndex = 0;
} // end serial_receive()

void serialEvent() {
  while (Serial.available() > 0 ) {
    char incomingByte = Serial.read();

    if (stringOverflow) {
      serialBuffer[dataBufferIndex++] = charOverflow;  // Place saved overflow byte into buffer
      serialBuffer[dataBufferIndex++] = incomingByte;  // saved next byte into next buffer
      stringOverflow = false;                          // turn overflow flag off
    } else if (dataBufferIndex > 8) {
      stringComplete = true;        // Send this buffer out to radio
      stringOverflow = true;        // trigger the overflow flag
      charOverflow = incomingByte;  // Saved the overflow byte for next loop
      dataBufferIndex = 0;          // reset the bufferindex
      break;
    }
    else if (incomingByte == '\n') {
      serialBuffer[dataBufferIndex] = 0;
      stringComplete = true;
    } else {
      serialBuffer[dataBufferIndex++] = incomingByte;
      serialBuffer[dataBufferIndex] = 0;
    }
  } // end while()
} // end serialEvent()

void nRF_receive(void) {
  int len = 0;
  if ( radio.available() ) {
    bool done = false;
    while ( !done ) {
      len = radio.getDynamicPayloadSize();
      done = radio.read(&RecvPayload, len);
      delay(5);
    }

    RecvPayload[len] = 0; // null terminate string

    //    Serial.print("R:");
    incomingString = RecvPayload;
    CheckSerial();
    //    Serial.println(RecvPayload);
    RecvPayload[0] = 0;  // Clear the buffers
  }
} // end nRF_receive()

//void serial_receive(void) {
//
//  if (stringComplete) {
//    strcat(SendPayload, serialBuffer);
//    // swap TX & Rx addr for writing
//    radio.openWritingPipe(pipes[1]);
//    radio.openReadingPipe(0, pipes[0]);
//    radio.stopListening();
//    bool ok = radio.write(&SendPayload, strlen(SendPayload));
//
//    //        Serial.print("S:");
//
//    //        Serial.print(SendPayload);
//    //        Serial.println();
//    stringComplete = false;
//
//    // restore TX & Rx addr for reading
//    radio.openWritingPipe(pipes[0]);
//    radio.openReadingPipe(1, pipes[1]);
//    radio.startListening();
//    SendPayload[0] = 0;
//    dataBufferIndex = 0;
//  } // endif
//} // end serial_receive()
