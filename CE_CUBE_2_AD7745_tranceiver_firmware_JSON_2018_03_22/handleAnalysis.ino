void handleAnalysis() {
  if (analysisStartedFlag == true) {
    if (analysisInitFlag == true) {
      analysisInit();
      analysisInitFlag = false;
    }
    for (int i = currentAnalysis; i < analysisNumber; i++) { //analysis number
      for (int j = 1; j < repetitions; j++) { //repetitions of the analysis
        LiftDown();
        getBGE1();
        LiftUp();
        capillaryConditioning();
        LiftDown();
        getBGE2();
        LiftUp();
        capillaryConditioning();
        injectSample();
      }
    }
  }
}

void handleAnalysisCycle() {
  if (analysisStartedFlag == true) {
    if (analysisInitFlag == true) {
      analysisInit();
      analysisInitFlag = false;
    }
    switch (cycleTask) {
      case 0:
        if (lifterActionFlag == true) {
          LiftDown();
          lifterActionFlag = false;
        }
        if (carouselActionFlag == false) {
          carouselActionFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 1:
        if (carouselActionFlag == true) {
          getBGE1();
          carouselActionFlag = false;
        }
        if (lifterActionFlag == false) {
          lifterActionFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 2:
        if (lifterActionFlag == true) {
          LiftUp();
          lifterActionFlag = false;
          //          Serial.println(cycleTask);
        }
        if (flushingFlag == false) {
          flushingFlag = true;
        }
        break;
      case 3:
        capillaryFlushing();
        if (conditioningFlag == false) {
          conditioningFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 4:
        capillaryConditioning();
        if (lifterActionFlag == false) {
          lifterActionFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 5:
        if (lifterActionFlag == true) {
          LiftDown();
          lifterActionFlag = false;
        }
        if (carouselActionFlag == false) {
          carouselActionFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 6:
        if (carouselActionFlag == true) {
          getSample();
          carouselActionFlag = false;
        }
        if (lifterActionFlag == false) {
          lifterActionFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 7:
        if (lifterActionFlag == true) {
          LiftUp();
          lifterActionFlag = false;
        }
        if (injectionFlag == false) {
          injectionFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 8:
        injectSample();
        if (lifterActionFlag == false) {
          lifterActionFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 9:
        if (lifterActionFlag == true) {
          LiftDown();
          lifterActionFlag = false;
        }
        if (carouselActionFlag == false) {
          carouselActionFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 10:
        if (carouselActionFlag == true) {
          getBGE2();
          carouselActionFlag = false;
        }
        if (lifterActionFlag == false) {
          lifterActionFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 11:
        if (lifterActionFlag == true) {
          LiftUp();
          lifterActionFlag = false;
        }
        if (analysisWaitingFlag == false) {
          analysisWaitingFlag = true;
          //          Serial.println(cycleTask);
        }
        break;
      case 12:
        startCEanalysis();
        break;
      case 13:
        finishCEanalysis();
        break;
      default:
        break;
    }
    if (cycleTask >= 14) {
      cycleTask = 0;
      Serial.println(cycleTask);
      Serial.println(analysisStartedFlag);
    }
  }
}

void analysisInit() {
  cycleTask = 0;
  vialsNumber = 12;
  vialPosition = 0;
  analysisNumber = 9;
  repetitions = 3;
  analysisTimeMillis = analysisTimemin * 60 * 1000;
  currentRepetition = 1;
  currentAnalysis = 1;
}

void getBGE1() {
  turnPositions = vialPosition - vialBGE1;
  Serial.print("turnPositions:");
  Serial.println(turnPositions);
  if (turnPositions > 0) {
    carouselFlag = true;
    stepFlag = int(vialResolution * float(turnPositions));
    Direction = false;
    vialPosition = vialPosition - turnPositions;
  }
  else if (turnPositions < 0) {
    carouselFlag = true;
    stepFlag = -1 * int(vialResolution * float(turnPositions));
    Serial.print("stepFlag:");
    Serial.println(stepFlag);
    Direction = true;
    vialPosition = vialPosition - turnPositions;
  }
  else if (turnPositions == 0) {
    carouselFlag = true;
    stepFlag = vialResolution * float(turnPositions);
    Direction = true;
  }
}

void getBGE2() {
  turnPositions = vialPosition - vialBGE2;
  if (turnPositions > 0) {
    carouselFlag = true;
    stepFlag = int(vialResolution * float(turnPositions));
    Direction = false;
    vialPosition = vialPosition - turnPositions;
  }
  else if (turnPositions < 0) {
    carouselFlag = true;
    stepFlag = -1 * int(vialResolution * float(turnPositions));
    Direction = true;
    vialPosition = vialPosition - turnPositions;
  }
  else if (turnPositions == 0) {
    carouselFlag = true;
    stepFlag = int(vialResolution * float(turnPositions));
    Direction = true;
  }
}

void capillaryConditioning() {
  if (conditioningFlag == true) {
    currentConditioningTime = millis();
    HVON();
    conditioningFlag = false;
  }
  if (millis() >= currentConditioningTime + conditioningTime) {
    HVOFF();
    cycleTask++;
  }
}

void capillaryFlushing() {
  if (flushingFlag == true) {
    currentFlushTime = millis();
    setValvesON();
    setPumpON();
    flushingFlag = false;
  }
  if (millis() >= currentFlushTime + flushTime) {
    setPumpOFF();
    cycleTask++;
  }
}

void injectSample() {
  if (injectionFlag == true) {
    PIDinit();
    currentInjectionTime = millis();
    if (injectionMethod == false) {
      HVON();
    }
    else if (injectionMethod == true) {
      pressureActionStatus = true;
      pressureIndicator = false;
    }
    injectionFlag = false;
  }
  if (millis() >= currentInjectionTime + injectionTime) {
    HVOFF();
    pressureActionStatus = false;
    setPumpOFF();
    cycleTask++;
  }
}

void getSample() {
  turnPositions = vialPosition - currentAnalysis + 1 - 2;
  Serial.print("currentAnalysis:");
  Serial.println(currentAnalysis);
  Serial.print("turnPositions:");
  Serial.println(turnPositions);
  if (turnPositions > 0) {
    carouselFlag = true;
    stepFlag = int(vialResolution * float(turnPositions));
    Direction = false;
    vialPosition = vialPosition - turnPositions;
  }
  else if (turnPositions < 0) {
    carouselFlag = true;
    stepFlag = -1 * int(vialResolution * float(turnPositions));
    Serial.print("stepFlag:");
    Serial.println(stepFlag);
    Direction = true;
    vialPosition = vialPosition - turnPositions;
  }
  else if (turnPositions == 0) {
    carouselFlag = true;
    stepFlag = int(vialResolution * float(turnPositions));
    Direction = true;
  }
}

void startCEanalysis() {
  if (analysisWaitingFlag == true) {
    Auto_ZeroF();
    currentAnalysisTime = millis();
    HVON();
    //    Serial.println("START");
    sendStartSignal();
    analysisWaitingFlag = false;
    analysisStateFlag = true;
    cycleTask++;
  }
}

void finishCEanalysis() {
  if (millis() >= analysisTimeMillis + currentAnalysisTime && analysisStateFlag == true) {
    HVOFF();
    Serial.println("STOP");

    analysisStateFlag = false;
    cycleTask++;
    //    Serial.println(cycleTask);
    currentRepetition++;
    if (currentRepetition > repetitions) {
      currentRepetition = 1;
      currentAnalysis++;
    }
    if (currentAnalysis > analysisNumber) {
      analysisStartedFlag = false;
      HVOFF();
      Serial.println("finished");
    }
    Serial.print("currentRepetition:");
    Serial.println(currentRepetition);
    Serial.print("currentAnalysis:");
    Serial.println(currentAnalysis);
    lifterActionFlag = true;
  }
}

void sendStartSignal() {
  char SendPayloadStart[32] = "";
  strcat(SendPayloadStart, startString);
  //  Serial.println(startString);
  //  Serial.println(SendPayload);
  // swap TX & Rx addr for writing
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(0, pipes[0]);
  radio.stopListening();
  bool ok = radio.write(&SendPayloadStart, strlen(SendPayloadStart));

  //delay(100);
  // restore TX & Rx addr for reading
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);
  radio.startListening();
  SendPayloadStart[0] = 0;
  dataBufferIndex = 0;
}


