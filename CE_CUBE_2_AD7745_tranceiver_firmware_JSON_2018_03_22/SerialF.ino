void CheckSerial() {

  if (incomingString.equals("9Hz") == true) {
    Update9Hz();
    incomingString = "zero";
  }
  else if (incomingString.equals("11Hz") == true) {
    Update11Hz();
    incomingString = "zero";
  }
  else if (incomingString.equals("13Hz") == true) {
    Update13Hz();
    incomingString = "zero";
  }
  else if (incomingString.equals("16Hz") == true) {
    Update16Hz();
    incomingString = "zero";
  }
  else if (incomingString.equals("50Hz") == true) {
    Update50Hz();
    incomingString = "zero";
  }
  else if (incomingString.equals("84Hz") == true) {
    Update84Hz();
    incomingString = "zero";
  }
  else if (incomingString.equals("91Hz") == true) {
    Update91Hz();
    incomingString = "zero";
  }
  else if (incomingString.equals("26Hz") == true) {
    Update26Hz();
    incomingString = "zero";
  }
  else if (incomingString.equals("16kHz") == true) {
    Excitation16kHz();
    incomingString = "zero";
  }
  else if (incomingString.equals("32kHz") == true) {
    Excitation32kHz();
    incomingString = "zero";
  }
  else if (incomingString.equals("Vdd/8") == true) {
    Vdd8();
    incomingString = "zero";
  }
  else if (incomingString.equals("Vdd/4") == true) {
    Vdd4();
    incomingString = "zero";
  }
  else if (incomingString.equals("Vddx3/8") == true) {
    Vddx38();
    incomingString = "zero";
  }
  else if (incomingString.equals("Vdd/2") == true) {
    Vdd2();
    incomingString = "zero";
  }
  else if (incomingString.equals("CAPCHOP1") == true) {
    CapChop();
    incomingString = "zero";
  }
  else if (incomingString.equals("NormalOper") == true) {
    NormalOperation();
    incomingString = "zero";
  }
  else if (incomingString.equals("TemperON") == true && temperatureCompensation == false) {
    delay(100);
    VT_on();
    incomingString = "zero";
    delay(100);
  }
  else if (incomingString.equals("TemperOFF") == true && temperatureCompensation == true) {
    VT_off();
    incomingString = "zero";
  }
  else if (incomingString.equals("Auto Zero") == true) {
    Auto_ZeroF();
    incomingString = "zero";
  }
  else if (incomingString.equals("Zero Delete") == true) {
    Auto_Zero_Del();
    incomingString = "zero";
  }
  else if (incomingString.equals("CCLOCK") == true) {
    carouselClock();
    incomingString = "zero";
  }
  else if (incomingString.equals("CCOUNT") == true) {
    carouselCounter();
    incomingString = "zero";
  }
  else if (incomingString.equals("ACLOCK") == true) {
    adjustClock();
    incomingString = "zero";
  }
  else if (incomingString.equals("ACOUNT") == true) {
    adjustCounter();
    incomingString = "zero";
  }
  else if (incomingString.equals("LIFTUP") == true) {
    LiftUp();
    incomingString = "zero";
  }
  else if (incomingString.equals("LIFTDOWN") == true) {
    LiftDown();
    incomingString = "zero";
  }
  else if (incomingString.equals("HVONN") == true) {
    HVON();
    incomingString = "zero";
  }
  else if (incomingString.equals("PUMPON") == true) {
    setPumpON();
    incomingString = "zero";
  }
  else if (incomingString.equals("PUMPOFF") == true) {
    setPumpOFF();
    incomingString = "zero";
  }
  else if (incomingString.equals("VALVESON") == true) {
    setValvesON();
    incomingString = "zero";
  }
  else if (incomingString.equals("VALVESOFF") == true) {
    setValvesOFF();
    incomingString = "zero";
  }
  else if (incomingString.equals("HVOFF") == true) {
    HVOFF();
    incomingString = "zero";
  }
  else if (incomingString.equals("STARTA") == true) {
    analysisStartedFlag = true;
    analysisInitFlag = true;
    lifterActionFlag = true;
    incomingString = "zero";
  }
  else if (incomingString.equals("STOPA") == true) {
    analysisStartedFlag = false;
    HVOFF();
    setPumpOFF();
    pressureActionStatus = false;
    incomingString = "zero";
  }
  else if (incomingString.equals("SETHYDRO") == true) {
    setHydrodynamicInjection();
    incomingString = "zero";
  }
  else if (incomingString.equals("SETELECTRO") == true) {
    setElectrokineticInjection();
    incomingString = "zero";
  }
  else if (incomingString.equals("INJ5") == true) {
    setInj5();
    incomingString = "zero";
  }
  else if (incomingString.equals("INJ10") == true) {
    setInj10();
    incomingString = "zero";
  }
  else if (incomingString.equals("INJ20") == true) {
    setInj20();
    incomingString = "zero";
  }
  else if (incomingString.equals("INJ40") == true) {
    setInj40();
    incomingString = "zero";
  }
  else if (incomingString.equals("INJ80") == true) {
    setInj80();
    incomingString = "zero";
  }
  else if (incomingString.equals("INJ160") == true) {
    setInj160();
    incomingString = "zero";
  }
  else if (incomingString.equals("INJ320") == true) {
    setInj320();
    incomingString = "zero";
  }
  else if (incomingString.equals("INJ640") == true) {
    setInj640();
    incomingString = "zero";
  }
}

void Update91Hz() {
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00000000);
  Wire.endTransmission();
  delay(100);
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00000001);
  Wire.endTransmission();
}

void Update9Hz() {
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00000000);
  Wire.endTransmission();
  delay(100);
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00111001);
  Wire.endTransmission();
}

void Update11Hz() {
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00000000);
  Wire.endTransmission();
  delay(100);
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00110001);
  Wire.endTransmission();
}

void Update13Hz() {
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00000000);
  Wire.endTransmission();
  delay(100);
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00101001);
  Wire.endTransmission();
}

void Update16Hz() {
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00000000);
  Wire.endTransmission();
  delay(100);
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00100001);
  Wire.endTransmission();
}

void Update26Hz() {
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00000000);
  Wire.endTransmission();
  delay(100);
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00011001);
  Wire.endTransmission();
}

void Update50Hz() {
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00000000);
  Wire.endTransmission();
  delay(100);
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00010001);
  Wire.endTransmission();
}

void Update84Hz() {
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00000000);
  Wire.endTransmission();
  delay(100);
  Wire.beginTransmission(Address);
  Wire.write(ConfigSetup);
  Wire.write(B00001001);
  Wire.endTransmission();
}
