void handleHardware() {
  turnCarousel();
  turnOffMotor();
  servoOff();
  performPressureAction();
}

void performPressureAction(){
  if (pressureIndicator == false && pressureActionStatus == true){
    regulatePressure();
  }
}

void turnCarousel() {
  currentMillis = millis();
  if (stepFlag > 0 && currentMillis - last_timeStep >= carouselSpeed && carouselFlag == true) {
    //    Direction = true;
    stepper(1);
    timeStep = timeStep + millis() - last_timeStep;
    last_timeStep = millis();
    //    steps_left--;
    stepFlag--;
  }
}


void turnOffMotor() {
  if (stepFlag == 0 && carouselFlag == true) {
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, LOW);
    carouselFlag = false;
    cycleTask++;
  }
}

void setupHardware() {
  pinMode(HVpin, OUTPUT);
  digitalWrite(HVpin, HIGH);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  pinMode(pumpPin, OUTPUT);
  digitalWrite(pumpPin, HIGH);
  pinMode(valvesPin, OUTPUT);
  digitalWrite(valvesPin, HIGH);
  bmp.begin();
//  if (!bme.begin()) {  
////    Serial.println("Could not find a valid BMP280 sensor, check wiring!");
//    while (1);
//  }
}

