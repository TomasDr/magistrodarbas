void Electro() {
  if (InjectElectro1 == true){
  LiftPlatform = true;
  LiftUp();
  SwitchHVon = true;
  HVON();
  delay(1000);
  SwitchHVoff = true;
  HVOFF();
  LiftPlatformDown = true;
  LiftDown();
  InjectElectro1 = false;
  }
  else if (InjectElectro2 == true){
  LiftPlatform = true;
  LiftUp();
  SwitchHVon = true;
  HVON();
  delay(2000);
  SwitchHVoff = true;
  HVOFF();
  LiftPlatformDown = true;
  LiftDown();
  InjectElectro2 = false;
  }
  else if (InjectElectro5 == true){
  LiftPlatform = true;
  LiftUp();
  SwitchHVon = true;
  HVON();
  delay(5000);
  SwitchHVoff = true;
  HVOFF();
  LiftPlatformDown = true;
  LiftDown();
  InjectElectro5 = false;
  }
  else if (InjectElectro10 == true){
  LiftPlatform = true;
  LiftUp();
  SwitchHVon = true;
  HVON();
  delay(10000);
  SwitchHVoff = true;
  HVOFF();
  LiftPlatformDown = true;
  LiftDown();
  InjectElectro10 = false;
  }
}

void Siphoning() {
if (InjectSiphon10 == true){
//  InjectTime = millis();
  LiftPlatform = true;
  LiftUp();

//  while (InjectTime + 10000 >= millis()){
//  alternativeloop();    
//  }
  delay(10000);
  
  LiftPlatformDown = true;
  LiftDown();
  InjectSiphon10 = false;
//  InjectionEvent = false;
}
else if (InjectSiphon20 == true){
  LiftPlatform = true;
  LiftUp();
  delay(20000);
  LiftPlatformDown = true;
  LiftDown();
  InjectSiphon20 = false;
}
if (InjectSiphon40 == true){
  LiftPlatform = true;
  LiftUp();
  delay(40000);
  LiftPlatformDown = true;
  LiftDown();
  InjectSiphon40 = false;
}
if (InjectSiphon80 == true){
  LiftPlatform = true;
  LiftUp();
  delay(80000);
  LiftPlatformDown = true;
  LiftDown();
  InjectSiphon80 = false;
}
if (InjectSiphon160 == true){
  LiftPlatform = true;
  LiftUp();
  delay(160000);
  LiftPlatformDown = true;
  LiftDown();
  InjectSiphon160 = false;
}
if (InjectSiphon320 == true){
  LiftPlatform = true;
  LiftUp();
  delay(320000);
  LiftPlatformDown = true;
  LiftDown();
  InjectSiphon320 = false;
}
}

void injectionControl(){
    if (incomingString.equals("SIPHON10") == true){
    InjectSiphon10 = true;
    InjectionEvent = true;
    InjectionTiming = millis();
    InjWaitTime = 10;
    incomingString = "zero";
    }
        if (incomingString.equals("SIPHON20") == true){
    InjectSiphon20 = true;
    InjectionEvent = true;
    InjectionTiming = millis();
    InjWaitTime = 20;
    incomingString = "zero";
    }
        if (incomingString.equals("SIPHON40") == true){
    InjectSiphon40 = true;
    InjectionEvent = true;
    InjectionTiming = millis();
    InjWaitTime = 40;
    incomingString = "zero";
    }
        if (incomingString.equals("SIPHON80") == true){
    InjectSiphon80 = true;
    InjectionEvent = true;
        InjectionTiming = millis();
    InjWaitTime = 10;
    incomingString = "zero";
    }
        if (incomingString.equals("SIPHON160") == true){
    InjectSiphon160 = true;
    InjectionEvent = true;
        InjectionTiming = millis();
    InjWaitTime = 160;
    incomingString = "zero";
    }    
        if (incomingString.equals("SIPHON320") == true){
    InjectSiphon320 = true;
    InjectionEvent = true;
        InjectionTiming = millis();
    InjWaitTime = 320;
    incomingString = "zero";
    }    
    
    else if (incomingString.equals("ELECTRO1") == true){
    InjectElectro1 = true;
    incomingString = "zero";
    }
        else if (incomingString.equals("ELECTRO2") == true){
    InjectElectro2 = true;
    incomingString = "zero";
    }
        else if (incomingString.equals("ELECTRO5") == true){
    InjectElectro5 = true;
    incomingString = "zero";
    }
        else if (incomingString.equals("ELECTRO10") == true){
    InjectElectro10 = true;
    incomingString = "zero";
    }
}

void CalculateInjTiming(){
  if (InjWaitTime * 1000 + InjectionTiming <= millis()){
   InjectNowFlag = true;
     LiftPlatformDown = true;
  LiftDown();
  InjectSiphon10 = false;
  }
}
