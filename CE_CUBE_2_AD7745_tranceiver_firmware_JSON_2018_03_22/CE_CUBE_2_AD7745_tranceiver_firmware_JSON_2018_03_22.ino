#include <ArduinoJson.h>

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include <Wire.h>
#include <Servo.h>
#include <Adafruit_BMP085.h>
#include <PID_v1.h>

String incomingString;

unsigned int InjectionTiming = 0;
unsigned int InjWaitTime = 0;
unsigned int InjectTime = 0;

//Carousel variables
#define IN1  3
#define IN2  4
#define IN3  5
#define IN4  6

Adafruit_BMP085 bmp; // I2C BMP280 pressure sensor

#define pumpPin 0 //0
#define valvesPin 1 //1

#define servoPin 8 //8

Servo myservo;
int HVpin = 7; //7

#define currentSensePin A0
int currentValue = 0;
const int avgNum = 100;
const int numReadings = 5;

int readings[numReadings];      // the readings from the analog input

int readIndex = 0;              // the index of the current reading
float total = 0.0;                  // the running total
float average = 0.0;                // the average

double SetpointPre, OutputPre;
double InputPre = 100000.0;
int WindowSizePre = 300;
unsigned long windowStartTimePre;
PID myPIDPre(&InputPre, &OutputPre, &SetpointPre, 3, 5, 2, REVERSE);

int currentTestPause = 0;
float vialResolution = 4076.0 / 12.0;
int Steps = 0;
int stepFlag = 0;
boolean Direction = true;// gre
boolean StepError = false;
boolean carouselFlag = false;
unsigned long last_timeStep;
unsigned long currentMillis ;
int carouselSpeed = 4;
long timeStep;

//Servo variables
int servoWaitingTime = 400;
long servoCurrentTime = 0;

double capacitance = 0;
double zeroCompensation = 0, temperature = 25.0, current = 0.0;

bool temperatureCompensation = false;

//CE booleans
boolean carouselClockFlag = false, carouselCounterFlag = false, adjustClockFlag = false, adjustCounterFlag = false;
boolean InjectionEvent = false;
boolean InjectNowFlag = false;
boolean LiftPlatform = false;
boolean LiftPlatformDown = false;
boolean SwitchHVon = false;
boolean SwitchHVoff = false;
boolean InjectElectro1 = false;
boolean InjectElectro2 = false;
boolean InjectElectro5 = false;
boolean InjectElectro10 = false;

boolean InjectSiphon10 = false;
boolean InjectSiphon20 = false;
boolean InjectSiphon40 = false;
boolean InjectSiphon80 = false;
boolean InjectSiphon160 = false;
boolean InjectSiphon320 = false;

int DataReady = B0000;

int StatusAddress = 0x00;
int CapDataH = 0x01;
int CapDataM = 0x02;
int CapDataL = 0x03;
int Address = B1001000;

int CapSetup = 0x07;        //capacitive channel setup address
int VTSetup = 0x08;
int ExcSetup = 0x09;        //excitation setup address
int ConfigSetup = 0x0A;    //configuration setup addrss
int CapDacAReg = 0x0B;//Capacitive DAC setup address
int CapGainRegH = 0x0F; //Cap gain high adress
int CapGainRegL = 0x10; //Cap gain high adress
int CapOffsetH = 0x0D; //Cap Offset high adress
int CapOffsetL = 0x0E; //Cap gain Offset adress

int CapChanProp = B10100001;    //capacitive channel properties
int ExcProp = B01100011;      //excitation properties Default B01100011
int ConfigProp = B00111001;   //configuration properties
int CapDacProp = B0;    //Capacitive DAC setup properties
int CapGainPropH = B01011101;  //cap gain properties high
int CapGainPropL = B10111101;  //cap gain properties low
int CapOffsetPropH = B01110111;  //cap offset properties high
int CapOffsetPropL = B00011010;  //cap Offset properties low

char valString[50];

//NRF settings
RF24 radio(9, 10);
boolean NRFReceiveFlag = false;
const uint64_t pipes[2] = { 0xDEDEDEDEE7LL, 0xDEDEDEDEE9LL };

boolean stringComplete = false;  // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;


char RecvPayload[50] = "";
char serialBuffer[50] = "";

//analysis variables
boolean analysisStartedFlag = false; // if analysis start button pressed, the analysis process is started
boolean analysisStateFlag = false; // checks if analysis is being performed
boolean analysisWaitingFlag = false; //checks if system is waiting for the start of analysis
boolean analysisInitFlag = false, taskDone = true, conditioningFlag = false, flushingFlag = false, injectionFlag = false, lifterActionFlag = false, carouselActionFlag = false;
boolean injectionMethod = true, pressureIndicator = false, pressureActionStatus = false;
int vialsNumber = 12, vialPosition = 0, analysisNumber = 9, currentAnalysis = 1, repetitions = 3, currentRepetition = 0, turnPositions = 0, cycleTask = 0;
float analysisTimemin = 9; //min
long analysisTimeMillis, currentAnalysisTime, conditioningTime = 180000, flushTime = 30000, currentConditioningTime, currentFlushTime, injectionTime = 20000, currentInjectionTime; //seconds
const int vialBGE1 = 0, vialBGE2 = 1;
char startString[50] = "{\"c\":\"1.234567890\",\"t\":\"20.000\"}"; //{"c":"0.011","t":"0.011"}


void setup(void) {
  delay(500);
//    Serial.begin(115200);
  //  Serial.println(startString);
  setupHardware();
  setupNRF();
  setupCDC();
//  PIDinit();
  //analysis setup
  delay(15);

}

void loop(void) {

  //  serialEvent();
  nRF_receive();
//  regulatePressure();
  //  serial_receive();

  statusRead();

  //Serial.println("test");
  if (statusRead() & 0 == DataReady) {
    //  addressRead();
    //      dataRead();
    //  Serial.println(capacitance, DEC);
    //      Serial.println(dataRead(), DEC);
    //  dtostrf(dataRead(),10,8,valString);
    if (Wire.available() >= 0 && carouselFlag == false && LiftPlatform == false) {
      //  if (millis() > currentTestPause + 500 && carouselFlag == false && LiftPlatform == false) {
      getJson();
      sendvalues();

//      if (pressureActionStatus == false) {
//            Serial.println(bmp.readPressure());
//      }
      //      currentTestPause = millis();
    }
  }
  handleHardware();
  //  handleAnalysis();
  handleAnalysisCycle();
} // end loop()
