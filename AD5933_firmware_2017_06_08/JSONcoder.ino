//void jsonEncode() {
//
//  // Memory pool for JSON object tree.
//  //
//  // Inside the brackets, 200 is the size of the pool in bytes.
//  // If the JSON object is more complex, you need to increase that value.
//  //StaticJsonBuffer<20000> jsonBuffer;
//
//  // StaticJsonBuffer allocates memory on the stack, it can be
//  // replaced by DynamicJsonBuffer which allocates in the heap.
//  // It's simpler but less efficient.
//  //
//   DynamicJsonBuffer  jsonBuffer;
//
//  // Create the root of the object tree.
//  //
//  // It's a reference to the JsonObject, the actual bytes are inside the
//  // JsonBuffer with all the other nodes of the object tree.
//  // Memory is freed when jsonBuffer goes out of scope.
//  JsonObject& root = jsonBuffer.createObject();
//
//  // Add values in the object
//  //
//  // Most of the time, you can rely on the implicit casts.
//  // In other case, you can do root.set<long>("time", 1351824120);
//  root["detector"] = "IDC";
//  root["time"] = float(millis())/1000.0;
//  
//  JsonArray& frequency = root.createNestedArray("frequency");
//  frequency.add(double_with_n_digits(MeasuredFrequency, 2));
//  // Add a nested array.
//  //
//  // It's also possible to create the array separately and add it to the
//  // JsonObject but it's less efficient.
//  JsonArray& realPart = root.createNestedArray("realPart");
//  realPart.add(double_with_n_digits(real, 1));
//
//  JsonArray& imagPart = root.createNestedArray("imagPart");
//  imagPart.add(double_with_n_digits(imag, 1));
//
////  root.printTo(Serial);
//  // This prints:
//  // {"sensor":"gps","time":1351824120,"data":[48.756080,2.302038]}
//
////  Serial.println();
//
//  root.prettyPrintTo(Serial);
//  // This prints:
//  // {
//  //   "sensor": "gps",
//  //   "time": 1351824120,
//  //   "data": [
//  //     48.756080,
//  //     2.302038
//  //   ]
//  // }
//  
//}
