void measureImpedance() {
  //  StaticJsonBuffer<200> jsonBuffer;
  DynamicJsonBuffer  jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  JsonArray& frequency = root.createNestedArray("frequency");
  JsonArray& realPart = root.createNestedArray("realPart");
  JsonArray& imagPart = root.createNestedArray("imagPart");

  float MeasuredFrequency = StartFrequency;
  int addedJson = 0, jsonSent = 0;
  // 1. place AD5933 in standby mode
  setControlRegister(STANDBY);

  // 2. initialize with start frequency
  setControlRegister(INITIALIZE);

  // 3. start frequency sweep
  setControlRegister(START_SWEEP);

  // start array:
  //  Serial.println("data = [ ... ");

//   while (checkStatus() < frequencySweepComplete) { // if status is 4 or higher, the sweep is complete
  while (checkStatus() != 7 || checkStatus() < 4) {
//  while (checkStatus() != 7 || checkStatus() < 4) {
    //      if (addedJson == 0)
    //      {
    //        JsonObject& root = jsonBuffer.createObject();
    //        JsonArray& frequency = root.createNestedArray("frequency");
    //        JsonArray& realPart = root.createNestedArray("realPart");
    //        JsonArray& imagPart = root.createNestedArray("imagPart");

    // 4. poll status register until complete
    // while (checkStatus() < validImpedanceData) {
    //}

    //delay(2+(1*(800000/(MeasuredFrequency*(SettlingTimeNumber/2))))); // delay seems to determine the minimum frequency that can be used for an accurate measurement
    // examples (for settling times = 1000)
    //  delay(100);
    //   Serial.println(checkStatus(), BIN);
    //   Serial.println(checkStatus()&1, BIN);
    //   Serial.println(checkStatus()&2, BIN);
    //   Serial.println(checkStatus() &frequencySweepComplete, DEC);
    //   Serial.println(frequencySweepComplete, DEC);
    //   Serial.println(checkStatus(), DEC);

    // 5. read values

    //   Serial.println(",");

    //Serial.print("Magnitude: ");
    //   Serial.print(magnitude);
    //   Serial.print(" ");

    //Serial.print("Impedance: ");
    //   Serial.print(impedance);
    //   Serial.print(" ");

    //Serial.print("Phase: ");
    //   Serial.print(phase);
    //   Serial.println("; ... ");

    // 7. increment frequency
    //  if (checkStatus() &2 == validImpedanceData){
    if (checkStatus() == 2 || checkStatus() == 3) {

      int real = getByte(REAL_DATA_REGISTER[0]) << 8;
      real |= getByte(REAL_DATA_REGISTER[1]);

      if (real > 0x7FFF) { // negative value
        real &= 0x7FFF;
        real -= 0x10000;
      }

      int imag = getByte(IMAG_DATA_REGISTER[0]) << 8;
      imag |= getByte(IMAG_DATA_REGISTER[1]);

      if (imag > 0x7FFF) { // negative value
        imag &= 0x7FFF;
        imag -= 0x10000;
      }

      double magnitude = sqrt(pow(double(real), 2) + pow(double(imag), 2));
      double gain = 8.5 * pow(10, -11); //calibrated with 220kOhm resistor on 1/3/12

      double impedance = 1 / (gain * magnitude);

      double phase = atan(double(imag) / double(real));

      frequency.add(double_with_n_digits(MeasuredFrequency, 8));
      //   Serial.print(MeasuredFrequency);
      //   Serial.print(",");

      realPart.add(real);
      //Serial.print("Real: ");
      //   Serial.print(real);
      //   Serial.print(",");

      imagPart.add(imag);
      //Serial.print("Imaginary: ");
      //   Serial.println(imag);
      
//      if (addedJson == jsonCapacity && jsonSent == 0)
//      {
//        root["status"] = "begin";
//        root["detector"] = "IDC";
//        root["time"] = float(millis() - analysisTime) / 1000.0;
//        root["temperature"] = measureTemperature();
//        root.printTo(Serial);
//        jsonSent++;
//        addedJson = 0;
////        jsonBuffer = DynamicJsonBuffer();
//      }
//      else if (addedJson == jsonCapacity && jsonSent > 0 && jsonSent < (NumberOfIncrementsM / jsonCapacity) - 1)
//      {
//        root["status"] = "part";
//        root["detector"] = "IDC";
//        root["time"] = float(millis() - analysisTime) / 1000.0;
//        root["temperature"] = measureTemperature();
//        root.printTo(Serial);
//        jsonSent++;
//        addedJson = 0;
////        jsonBuffer = DynamicJsonBuffer();
//      }
//      else if (addedJson == jsonCapacity && jsonSent == (NumberOfIncrementsM / jsonCapacity) - 1)
//      {
//        root["status"] = "end";
//        root["detector"] = "IDC";
//        root["time"] = float(millis() - analysisTime) / 1000.0;
// //       root["temperature"] = measureTemperature();
//        root.printTo(Serial);
//        jsonSent++;
//        addedJson = 0;
//      }
//      addedJson++;
      setControlRegister(INCREMENT);
      MeasuredFrequency = MeasuredFrequency + FrequencyResolution;
    }
//Serial.println(checkStatus(), BIN);
  }
//Serial.println("pointLine");
  // 8. power-down mode
  setControlRegister(POWER_DOWN);
  root.printTo(Serial);
  // Serial.println("finito");
  //Serial.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeendndnd");
  //root.prettyPrintTo(Serial);
}


void configureAD5933(int settlingTimes, float startFreq, float freqIncr, int numIncr)
{
  setPGAGain();
  setNumberOfSettlingTimes(settlingTimes);
  setStartFrequency(startFreq);
  setFrequencyIncrement(freqIncr);
  setNumberOfIncrements(numIncr);
}


