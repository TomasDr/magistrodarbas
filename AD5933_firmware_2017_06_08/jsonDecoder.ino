void getJSONinfo(){
  StaticJsonBuffer<200> jsonBuffer2;

  String json = incommingString;
  
  JsonObject& root = jsonBuffer2.parseObject(json);
    // Test if parsing succeeds.
  if (!root.success()) {
  //  Serial.println("parseObject() failed");
    return;
  }
//  Serial.println(json);
  
  String command = root["Command"]; //command
  if (command.equals("Params")){
  StartFrequency = root["StrtFr"]; //start frequency
  FrequencyResolution = root["Incr"]; //increment size
  NumberOfIncrementsM = root["NIncrs"]; //number of increments

    configureAD5933(SettlingTimeNumber, // number of settling times
    StartFrequency, // start frequency (Hz)
    FrequencyResolution, // frequency increment (Hz)
    NumberOfIncrementsM); // number of increments
  
  }

  else if (command.equals("Conf")){
    configureAD5933(SettlingTimeNumber, // number of settling times
    StartFrequency, // start frequency (Hz)
    FrequencyResolution, // frequency increment (Hz)
    NumberOfIncrementsM); // number of increments 
  }

   else if (command.equals("Meas")){
    measureImpedance();
    Serial.println("End");
  }
   else if (command.equals("Clbr")){
      measureImpedance();
 //     Serial.println("Calibrate");
  }
   else if (command.equals("Start")){
        analysisTime = millis();
        measureImpedance();
    Serial.println("End");
  }


//  Serial.println(command);
//  Serial.println(startFr);
//  Serial.println(incrementFr);
//  Serial.println(noOfInc);
}

