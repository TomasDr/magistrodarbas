#include <ArduinoJson.h>
// Testing the AD5933

#include <Wire.h>

// The user must calibrate the AD5933 system for a known impedance range to determine the gain factor

#define AD5933_ADDR 0x0D
#define AD5245_ADDR 0x2D //B0101101; // << assumes AD0 tied to VCC

const float CLOCK_SPEED = 16.776*pow(10,6); // AD5933 has internal clock of 16.776 MHz

const int CONTROL_REGISTER[2] =                    { 0x80, 0x81       }; // see mapping below
const int START_FREQUENCY_REGISTER[3] =            { 0x82, 0x83, 0x84 }; // 24 bits for start frequency
const int FREQ_INCREMENT_REGISTER[3] =             { 0x85, 0x86, 0x87 }; // 24 bits for frequency increment
const int NUM_INCREMENTS_REGISTER[2] =             { 0x88, 0x89       }; //  9 bits for # of increments
const int NUM_SETTLING_CYCLES_REGISTER[2] =        { 0x8A, 0x8B       }; //  9 bits + 1 modifier for # of settling times
const int STATUS_REGISTER[1] =                     { 0x8F             }; // see mapping below
const int TEMPERATURE_DATA_REGISTER[2] =	   { 0x92, 0x93       }; // 16-bit, twos complement format
const int REAL_DATA_REGISTER[2] =                  { 0x94, 0x95       }; // 16-bit, twos complement format
const int IMAG_DATA_REGISTER[2] =                  { 0x96, 0x97       }; // 16-bit, twos complement format

// control register map (D15 to D12)
const int DEFAULT_VALUE =                 B0000; // initial setting
const int INITIALIZE =                    B0001; // excite the unknown impedance initially
const int START_SWEEP = 	          B0010; // begin the frequency sweep
const int INCREMENT =                     B0011; // step to the next frequency point
const int REPEAT =                        B0100; // repeat the current frequency point measurement
const int MEASURE_TEMP =                  B1001; // initiates a temperature reading
const int POWER_DOWN =                    B1010; // VOUT and VIN are connected internally to ground
const int STANDBY =                       B1011; // VOUT and VIN are connected internally to ground
const int PGA_GAIN =                      B00000000; // D8 = PGA gain (0 = x5, 1 = x1) // amplifies the response signal into the ADC B00000000 gain 5; B00000001 gain 1

const int BLOCK_WRITE = B1010000;
const int BLOCK_READ =  B1010001;
const int ADDRESS_PTR = 0xB0;// B1011000;

const int validTemperatureMeasurement =  B00000001;
const int validImpedanceData =           B00000010;
const int frequencySweepComplete =       B00000100;

const int ledPin = 12;

String incommingString;
char incommingStringBuffer[100];
char terminationChar = '*';

int SettlingTimeNumber = 4;
//float StartFrequency = 1.9*pow(10,5);
//float FrequencyResolution = 0.1*pow(1,1);

float StartFrequency = 500, analysisTime = 0;
float FrequencyResolution = 500;

int NumberOfIncrementsM = 20, jsonCapacity = 20;
//int PGAGain = 5;

int reading = 0;
byte rxByte;



void setup() 
{
  Wire.begin();
  Serial.begin(115200);

  pinMode(ledPin, OUTPUT);  

    setPGAGain();
    configureAD5933(SettlingTimeNumber, // number of settling times
    StartFrequency, // start frequency (Hz)
    FrequencyResolution, // frequency increment (Hz)
    NumberOfIncrementsM); // number of increments 
  
}

void loop() 
{  
  if (Serial.available() > 0) {
    
//    int incomingByte = Serial.read();
    Serial.readBytesUntil('*', incommingStringBuffer, 100);
    digitalWrite(ledPin, HIGH);
    incommingString = incommingStringBuffer;
    memset(incommingStringBuffer, 0, sizeof incommingStringBuffer);
    //Serial.println(incommingString);

    getJSONinfo();
    
//    if (incommingString.equals("Temp")){
//    StaticJsonBuffer<100> jsonBuffer;
//    JsonObject& root = jsonBuffer.createObject();
//    root["detector"] = "IDC";
//    root["time"] = float(millis())/1000.0;
//    root["temperature"] = measureTemperature();
//    JsonArray& frequency = root.createNestedArray("frequency");
//    JsonArray& realPart = root.createNestedArray("realPart");
//    JsonArray& imagPart = root.createNestedArray("imagPart");
//    root.printTo(Serial);
//    }
//      delay(1);
//    else if (incommingString.equals("Conf")){
//    configureAD5933(SettlingTimeNumber, // number of settling times
//    StartFrequency, // start frequency (Hz)
//    FrequencyResolution, // frequency increment (Hz)
//    NumberOfIncrementsM); // number of increments  
//  }
//    else if (incommingString.equals("Meas")){
//      measureImpedance();
//      Serial.println("End");
//  }
//    else if (incommingString.equals("ChkS")){
//    Serial.println(checkStatus(), BIN);
//  }
//      else if (incommingString.equals("Clbr")){
//      measureImpedance();
//      Serial.println("Calibrate");
//  }
//      else if (incommingString.equals("Start")){
//        analysisTime = millis();
//      }

      digitalWrite(ledPin, LOW);   
  }
}





